import {Router} from '@angular/router';
import {AuthenticationServicesService} from './../../api-module/services/authentication/authentication-services.service';
import {Component, DoCheck, OnInit} from '@angular/core';
import {UtilitiesService} from "../../operations/utilities.service";

@Component({
  selector: 'app-main-header-component',
  templateUrl: './main-header-component.component.html',
  styleUrls: ['./main-header-component.component.css']
})
export class MainHeaderComponentComponent implements OnInit, DoCheck {
  isLoggedin: boolean = false;
  CurentUser: any;
  languages: any[];
  selectedLang: string;

  constructor(private authenticationService: AuthenticationServicesService, private router: Router, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.isLoggedin = this.authenticationService.isLoggedIn();
    this.languages = this.utilities.languages;
    if (window.localStorage.getItem('lang')) {
      console.log(window.localStorage.getItem('lang'));
      this.selectedLang = window.localStorage.getItem('lang');
      // this.utilities.chooseLang(this.selectedLang);
    } else {
      this.selectedLang = this.utilities.languages[1].symbol;
    }
    // this.chooseLang(this.selectedLang);
    this.utilities.chooseLang(this.selectedLang);
  }

  ngDoCheck() {
    this.CurentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  LogoutAll() {
    this.router.navigate(['/login']);
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginRight = "0";
  }

  chooseLang(lang) {
    this.utilities.chooseLang(lang);
    // setTimeout(() => {
    location.reload()
    // }, 500)
  }

}
