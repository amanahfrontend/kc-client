import {CustomerCrudService} from './../api-module/services/customer-crud/customer-crud.service';
import {FormsModule} from '@angular/forms';
import {AlertServiceService} from './../api-module/services/alertservice/alert-service.service';
import {AlertComponentComponent} from './shared/alert-component/alert-component.component';
import {FooterComponentComponent} from './footer-component/footer-component.component';
import {MainHeaderComponentComponent} from './main-header-component/main-header-component.component';
import {ApiModuleModule} from './../api-module/api-module.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModuleWithProviders, Type} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {DragulaModule, DragulaService} from "ng2-dragula/ng2-dragula"
import {OrderConfirmationModalComponent} from "../customer/order-confirmation-modal/order-confirmation-modal.component";
import {CalendarModule} from 'primeng/components/calendar/calendar';
import {InputMaskModule} from 'primeng/components/inputmask/inputmask';


import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from "@angular/common/http";
import { InputSwitchModule } from 'primeng/primeng';
import { VehicleTypeComponent } from '../admin/admin-main-page/vehicle/vehicle-type.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    CommonModule,

    ApiModuleModule,
    FormsModule,
    NgxDatatableModule,
    DragulaModule,
    CalendarModule,
    InputSwitchModule,
    InputMaskModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    MainHeaderComponentComponent,
    FooterComponentComponent,
    AlertComponentComponent,
    OrderConfirmationModalComponent,
    VehicleTypeComponent
  ],
  exports: [
    MainHeaderComponentComponent,
    FooterComponentComponent,
    AlertComponentComponent,
    InputSwitchModule,
    VehicleTypeComponent,
    NgxDatatableModule,
    ApiModuleModule,
    DragulaModule,
    OrderConfirmationModalComponent,
    CalendarModule,
    InputMaskModule,
    TranslateModule
  ]
})

export class SharedModuleModule {

  static forRoot(entryComponents?: Array<Type<any> | any[]>): ModuleWithProviders {
    return {
      ngModule: SharedModuleModule,
      providers: [
        AlertServiceService,
        CustomerCrudService
      ]
    };
  }
}

export const rootShared: ModuleWithProviders = SharedModuleModule.forRoot();

