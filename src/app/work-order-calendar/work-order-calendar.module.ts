import {AuthGuardGuard} from './../api-module/guards/auth-guard.guard';
import {RouterModule, Routes} from '@angular/router';
import {SharedModuleModule} from './../shared-module/shared-module.module';
import {FormsModule} from '@angular/forms';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {WorkOrderCalendarComponent} from './work-order-calendar-management/work-order-calendar-management.component';
// import {CalendarModule} from "ap-angular2-fullcalendar";
import {CalendarModule} from 'angular-calendar';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
// import {CalendarComponent} from "ap-angular2-fullcalendar";


const routes: Routes = [
  {
    path: '',
    component: WorkOrderCalendarComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['MovementController', 'Dispatcher', 'Admin']}
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModalModule.forRoot(),
    CalendarModule.forRoot(),
    SharedModuleModule,
    RouterModule.forChild(routes)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [WorkOrderCalendarComponent]
})
export class workOrderBoardCalendarModule {
}
