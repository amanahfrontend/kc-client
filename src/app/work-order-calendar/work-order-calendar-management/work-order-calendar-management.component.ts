import {Subject} from 'rxjs/Subject';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute, RouterStateSnapshot} from '@angular/router';
import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef, OnInit, ViewEncapsulation
} from '@angular/core';
import {LookupService} from './../../api-module/services/lookup-services/lookup.service';
import {AlertServiceService} from './../../api-module/services/alertservice/alert-service.service';

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';

import {CustomerCrudService} from './../../api-module/services/customer-crud/customer-crud.service';

const colors: any = {
  Rejected: {
    primary: '#af3f44',
    secondary: '#FAE3E3'
  },
  Approved: {
    primary: '#8ae98a',
    secondary: '#D1E8FF'
  },
  Pending: {
    primary: '#fafa7b',
    secondary: '#FDF1BA'
  },
  Completed: {
    primary: '#ffffff',
    secondary: '#FDF1BA'
  },
  'In Progress': {
    primary: '#f1c77a',
    secondary: '#FDF1BA'
  },
  Cancelled: {
    primary: '#424141',
    secondary: '#FDF1BA'
  },
  Dispatched: {
    primary: '#d08d52',
    secondary: '#FDF1BA'
  },
  'In Confirmation': {
    primary: '#424190',
    secondary: '#FDF1BA'
  }
};

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-work-order-calendar-management',
  templateUrl: './work-order-calendar-management.component.html',
  styleUrls: ['./work-order-calendar-management.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class WorkOrderCalendarComponent implements OnInit {

  public Periorities: any[];
  public vehicles;
  public AllOrders;
  public Factories;
  public allStatus;
   refresh: Subject<any> = new Subject();

  @ViewChild('modalContent') modalContent: TemplateRef<any>;


  view: string = 'month';

  viewDate: Date = new Date();
  orders: any;
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  currentOrder: any;


  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({event}: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({event}: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];


  ngOnInit() {
    this.callNeededLookups();
    let UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
    let UserID = JSON.parse(localStorage.getItem('currentUser')).id;
    //------------------- if Dispatcher ------------------------------
    if ((UserAllRoles.indexOf('Dispatcher') > -1)) {
      this.getAllworkOrders('Dispatcher');
    }
    //------------------- if MovementController ------------------------------
    else if ((UserAllRoles.indexOf('MovementController') > -1)) {
      this.getAllworkOrders('MovementController');
    }
    else if ((UserAllRoles.indexOf('Admin') > -1)) {
      this.getAllworkOrders('Admin');
    }
  }

  setEvents(orders) {
    this.events = orders.map((item) => {
      console.log(item.status.name);
      console.log(colors[item.status.name]);
      return {
        start: new Date(item['startDate']),
        end: new Date(item['startDate']),
        title: 'Work Order Code: ' + item.code,
        color: colors[item.status.name],
        obj: item
      };
    });
    console.log(this.events);
    this.refresh.next();
  }

  getAllworkOrders(roleName) {
    if (roleName == 'Dispatcher') {
      this._customerService.GetCallendarWorkOrders().subscribe(WorkOrder => {
          console.log(WorkOrder);
          this.setEvents(WorkOrder);
        },
        err => {
          // this.alertService.error('your session has timed out please login again !')
        });
    }
    else if (roleName == 'MovementController') {
      this._customerService.GetCallendarWorkOrders().subscribe(WorkOrder => {
          console.log(WorkOrder);
          this.setEvents(WorkOrder);
        },
        err => {
          // this.alertService.error('your session has timed out please login again !')
        });
    }
    else if (roleName == 'Admin') {
      this._customerService.GetCallendarWorkOrders().subscribe(WorkOrder => {
          console.log(WorkOrder);
          this.setEvents(WorkOrder);
        },
        err => {
          // this.alertService.error('your session has timed out please login again !')
        });
    }
  }

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  constructor(private modal: NgbModal, private _customerService: CustomerCrudService,
              private lookupService: LookupService, private alertService: AlertServiceService) {
  }

  dayClicked({date, events}: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventTimesChanged({
                      event,
                      newStart,
                      newEnd
                    }: CalendarEventTimesChangedEvent): void {
    event['start'] = newStart;
    event['end'] = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = {event, action};
    this.currentOrder = event['obj'];
    this.modal.open(this.modalContent, {size: 'lg'});
  }

  callNeededLookups() {

    this.lookupService.GetallPeriorities().subscribe(Periorities => {
        this.Periorities = Periorities;
      },
      err => {
      });
    this.lookupService.GetAllVehicles().subscribe(vehicle => {
        this.vehicles = vehicle;
      },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });
    this._customerService.GetAllWorkOrder().subscribe(orders => {
        this.AllOrders = orders;
      },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetallFactories().subscribe(Factories => {
        this.Factories = Factories;
      },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });


    this.lookupService.GetallStatus().subscribe(allStatus => {
        this.allStatus = allStatus;
      },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });

  }

  SetStatus(t) {
    var statusName = '';

    if (t == 1) {
      statusName = 'Approved';
    }
    else if (t == 2) {
      statusName = 'Pending';
    }
    else if (t == 5) {
      statusName = 'Completed';
    }
    else if (t == 6) {
      statusName = 'InProgress';
    }
    else if (t == 10) {
      statusName = 'Rejected';
    }
    else if (t == 31) {
      statusName = 'Cancelled';
    }

    return statusName;
  }

  keyPressNegative(e: any) {
    if (!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58)
      || e.keyCode == 8)) {
      return false;
    }
  }
  
}
