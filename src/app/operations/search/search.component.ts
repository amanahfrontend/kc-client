import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {
  @Input() placeholder: string;
  @Input() back: boolean;
  @Output() searchValue = new EventEmitter();
  @Output() viewAll = new EventEmitter();
  @Output() exportToCsv = new EventEmitter();

  constructor(private location: Location) {
  }

  search(value) {
    this.searchValue.emit(value.searchText);
  }

  applyViewAll() {
    this.viewAll.emit()
  }

  exportCsv() {
    if (this.back) {
      this.location.back();
    } else {
      this.exportToCsv.emit();
    }
  }
}
