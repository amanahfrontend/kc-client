import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class UtilitiesService {
  customerSearch: string;
  showLatestCustomer: boolean;
  languages = [
    {id: 1, name: 'english', symbol: 'en'},
    {id: 2, name: 'arabic', symbol: 'ar'}
  ];

  constructor(private translate: TranslateService) {
  }

  convertDatetoNormal(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${day}/${month}/${year}`;
    //console.log(formatedDate);
    return formatedDate;
  }

    convertDate(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
      }
      let formatedDate = `${year}-${month}-${day}`;
    //console.log(formatedDate);
    return formatedDate;
  }


  chooseLang(lang) {
    this.translate.use(lang);
    window.localStorage.setItem('lang', lang);
  }

}
