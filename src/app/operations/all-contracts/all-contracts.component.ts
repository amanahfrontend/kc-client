import {Component, OnInit} from '@angular/core';
import {LookupService} from '../../api-module/services/lookup-services/lookup.service';
import {UtilitiesService} from '../utilities.service';
import {AlertServiceService} from '../../api-module/services/alertservice/alert-service.service';
import {Angular2Csv} from 'angular2-csv/Angular2-csv';
import {customer} from '../../api-module/models/customer-model';

@Component({
  selector: 'app-all-contracts',
  templateUrl: './all-contracts.component.html',
  styleUrls: ['./all-contracts.component.css']
})

export class AllContractsComponent implements OnInit {
  allContracts: any[];

  constructor(private lookupService: LookupService, private utilities: UtilitiesService, private alertService: AlertServiceService) {
  }

  ngOnInit() {
    this.getAllContracts();
  }

  exportCsv(contracts) {
    let exportData = [];
    exportData.push({
      'Contract Number': 'Contract Number',
      'Contract Type': 'Contract Type',
      'Amount': 'Amount',
      'Start Date': 'Start Date',
      'End Date': 'End Date'
    });
    contracts.map((contract) => {
      exportData.push({
        'Contract Number': contract.contractNumber,
        'Contract Type': contract.contractType.name,
        'Amount': contract.amount,
        'Start Date': contract.startDateView,
        'End Date': contract.endDateView
      });
    });
    return new Angular2Csv(exportData, 'Contracts', {
      showLabels: true
      // showTitle: true
    });
  }

  getAllContracts() {
    this.lookupService.getAllContracts().subscribe((contracts) => {
        this.allContracts = contracts;
        console.log(this.allContracts);
        this.allContracts.map((contract) => {
          contract.startDateView = this.utilities.convertDatetoNormal(contract.startDate);
          contract.endDateView = this.utilities.convertDatetoNormal(contract.endDate);
          if (contract.customer.phones) {
            contract.customer.phone = contract.customer.phones[0];
          }
        });
        // this.exportCsv(this.allContracts);
      },
      err => {
        this.alertService.error('Server Error');
        console.log('error');
      });
  }

  search(searchText) {
    console.log(searchText);
    this.allContracts = this.allContracts.filter((contract) => {
      return contract.contractNumber.toLowerCase().includes(searchText.toLowerCase());
    });
  }

}
