import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LookupService} from '../../api-module/services/lookup-services/lookup.service';
import {AlertServiceService} from '../../api-module/services/alertservice/alert-service.service';
import {UtilitiesService} from '../utilities.service';

@Component({
  selector: 'app-all-jobs',
  templateUrl: './all-jobs.component.html',
  styleUrls: ['./all-jobs.component.css']
})
export class AllJobsComponent implements OnInit {
  jobs: any[];
  private saveNoteObj: any =
    {
      'id': 0,
      'deliveryNote': ''
    };

  constructor(private activatedRoute: ActivatedRoute, private lookup: LookupService, private alertService: AlertServiceService, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.getAllJobs();
  }

  getAllJobs() {
    let orderId = this.activatedRoute.snapshot.params['OrderId'];
    this.lookup.getJobsByOrder(orderId).subscribe((orders) => {
        this.jobs = orders;
        console.log(this.jobs);
        this.jobs.map((order) => {
          order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
          order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
        });
      },
      err => {
        this.alertService.error('Server Error');
        console.log('failed');
      });
  }

  search(searchText) {
    console.log(searchText);
    this.jobs = this.jobs.filter((order) => {
      return order.vehicleNumber.toLowerCase().includes(searchText.toLowerCase());
    });
  }

  saveNote(job) {
    console.log(job);
    this.saveNoteObj.id = job.id;
    this.saveNoteObj.deliveryNote = job.deliveryNote;
    this.lookup.postJob(this.saveNoteObj).subscribe(() => {
        console.log('success');
        this.alertService.success('Delivery note added successfully!');
      },
      err => {
        this.alertService.error('Server Error');
        console.log('failed');
      });
  }
}
