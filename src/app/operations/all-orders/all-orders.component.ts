import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {LookupService} from '../../api-module/services/lookup-services/lookup.service';
import {AlertServiceService} from '../../api-module/services/alertservice/alert-service.service';
import {UtilitiesService} from '../utilities.service';

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css']
})
export class AllOrdersComponent implements OnInit {
  orders: any[];
  contractId: number;

  constructor(private activatedRoute: ActivatedRoute, private lookup: LookupService, private alertService: AlertServiceService, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.getAllOrders();
  }

  getAllOrders() {
    let contractId = this.activatedRoute.snapshot.params['contractsId'];
    this.lookup.getOrdersByContract(contractId).subscribe((orders) => {
        this.orders = orders;
        console.log(this.orders);
        this.orders.map((order) => {
          order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
          var orderEndDate = new Date(order.endDate);
          if (order.endDate && orderEndDate.getFullYear() > 1) {
            order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
          } else {
            order.endDateView = '--';
          }
        });
      },
      err => {
        this.alertService.error('Server Error');
        console.log('failed');
      });

  }

  search(searchText) {
    console.log(searchText);
    this.orders = this.orders.filter((order) => {
      return order.code.toLowerCase().includes(searchText.toLowerCase());
    });
  }

}
