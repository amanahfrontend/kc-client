import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AllContractsComponent} from './all-contracts/all-contracts.component';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuardGuard} from '../api-module/guards/auth-guard.guard';
import {AllOrdersComponent} from './all-orders/all-orders.component';
import {AllJobsComponent} from './all-jobs/all-jobs.component';
import {SharedModuleModule} from '../shared-module/shared-module.module';
import {UtilitiesService} from './utilities.service';
import {SearchComponent} from './search/search.component';
import {FormsModule} from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: AllContractsComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['Operator', 'Admin']}
  },
  {
    path: ':contractsId',
    component: AllOrdersComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['Operator', 'Admin']}
  },
  {
    path: ':contarctId/:OrderId',
    component: AllJobsComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['Operator', 'Admin']}
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModuleModule,
    FormsModule
  ],
  declarations: [AllContractsComponent, AllOrdersComponent, AllJobsComponent, SearchComponent],
  providers: [
    UtilitiesService
  ]
})
export class OperationsModule {
}
