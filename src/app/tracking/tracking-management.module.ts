import {AuthGuardGuard} from './../api-module/guards/auth-guard.guard';
import {RouterModule, Routes} from '@angular/router';
import {SharedModuleModule} from './../shared-module/shared-module.module';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TrackingManagementComponent} from './tracking-management/tracking-management.component';
// import {AgmCoreModule} from 'angular2-google-maps/core'
import * as config from '../api-module/services/globalPath';
import {AgmCoreModule} from '@agm/core';


const routes: Routes = [
  {
    path: '',
    component: TrackingManagementComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['MovementController', 'Dispatcher', 'CallCenter', 'Admin']}
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModuleModule,
    AgmCoreModule.forRoot(
      {
        apiKey: config.googlMapsAPiKey
      }
    ),
    RouterModule.forChild(routes)
  ],
  declarations: [TrackingManagementComponent]
})
export class TrackingModule {
}
