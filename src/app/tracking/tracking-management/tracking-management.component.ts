import {LookupService} from './../../api-module/services/lookup-services/lookup.service';
import {CustomerCrudService} from './../../api-module/services/customer-crud/customer-crud.service';
import {Component, ElementRef, OnInit, Renderer, OnDestroy, ViewEncapsulation} from '@angular/core';
import * as Config from '../../api-module/services/globalPath';
import { HubConnection } from '@aspnet/signalr-client/dist/browser/signalr-clientES5-1.0.0-alpha2-final.min.js'

import {AuthenticationServicesService} from '../../api-module/services/authentication/authentication-services.service';

@Component({
  selector: 'app-tracking-management',
  templateUrl: './tracking-management.component.html',
  styleUrls: ['./tracking-management.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class TrackingManagementComponent implements OnInit {
  // initial center position for the map
  private _hubConnection: HubConnection;

  public lat: number = 29.378586;
  public lng: number = 47.990341;
  zoom: number = 8;
  factories: any[];
  vehicle: any[];
  drivers: any[];
  status: any[];
  orders;
  ordersOnMap: any[];
  allOrdersOnMap: any[];
  toggleOrders: boolean;
  mainURL: string = '';
  isHideFromLoggedUser: boolean = false;

  public filterCritieria = {driverName: '', vehicleNumber: '', orderName: '', factoryName: '', status: ''};
  public vehicles = [];
  public originalVehicles = [];

  constructor(private _lookupService: LookupService, private _customerService: CustomerCrudService, private auth: AuthenticationServicesService) {
  }

  ngOnInit() {
    this.mainURL = this._lookupService.clientPath();
    this.toggleOrders = true;
    this.setLookups();
    this.getData()
      .then(() => {
        this.getVehiclesLocation();
      });
    this.isHideFromLoggedUser = this.auth.CurrentUser().roles.includes('CallCenter');
    if (!this.isHideFromLoggedUser) {
      this.getAllOrders();
    }
    // this.vehicles = this.originalVehicles;

  }

  getAllOrders() {
    this._lookupService.getAllOrdersLive().subscribe((orders) => {
        console.log(orders);
        this.ordersOnMap = orders;
        this.ordersOnMap.map((order) => {
          order.statusIcon =  'assets/Images/' + order.status.id + '.png';
          this.allOrdersOnMap = this.ordersOnMap.slice();
        });
      },
      err => {

      });
  }

  applyToggle() {
    if (this.toggleOrders) {
      this.ordersOnMap = this.allOrdersOnMap.slice();
    } else {
      this.ordersOnMap = [];
    }
  }

  merge_array(array1, array2) {
    var result_array = [];
    var arr = array1.concat(array2);
    var len = arr.length;
    var assoc = {};

    while (len--) {
      var item = arr[len];

      if (!assoc[item]) {
        result_array.unshift(item);
        assoc[item] = true;
      }
    }

    return result_array;
  }

  applyFilter() {
    debugger;
    let orderNoVehicle = [];
    if (this.filterCritieria.orderName && this.filterCritieria.orderName != '') {
      orderNoVehicle = this.originalVehicles.filter((item) => {
        if (this.filterCritieria.orderName == item.orderNumber) {
          debugger;
          return item;
        }
      });
      console.log('orderNoVehicle vehicles');
      console.log(orderNoVehicle);
    }


    let driverNameVehicle = [];

    if (this.filterCritieria.driverName) {
      driverNameVehicle = this.originalVehicles.filter((item) => {
        if (this.filterCritieria.driverName == item.driverId) {
          debugger;
          return item;
        }
      });
      console.log('driverNameVehicle vehicles');
      console.log(driverNameVehicle);
    }

    let factoryNameVehicle = [];
    if (this.filterCritieria.factoryName) {
      factoryNameVehicle = this.originalVehicles.filter((item) => {
        if (this.filterCritieria.factoryName == item.factoryId) {
          debugger;
          return item;
        }
      });
      console.log('factoryNameVehicle vehicles');
      console.log(factoryNameVehicle);
    }

    let vehicleNumberVehicle = [];

    if (this.filterCritieria.vehicleNumber) {
      vehicleNumberVehicle = this.originalVehicles.filter((item) => {
        if (this.filterCritieria.vehicleNumber == item.vehicleNumber) {
          debugger;
          return item;
        }
      });

      console.log('vehicleNumberVehicle vehicles');
      console.log(vehicleNumberVehicle);
    }

    let firstVehicles = this.merge_array(orderNoVehicle, driverNameVehicle);

    console.log('firstVehicles vehicles');
    console.log(firstVehicles);

    let secondVehicles = this.merge_array(factoryNameVehicle, vehicleNumberVehicle);

    console.log('secondVehicles vehicles');
    console.log(secondVehicles);

    this.vehicles = this.merge_array(firstVehicles, secondVehicles);
    // this.vehicle = this.originalVehicles.filter((item) => {
    //   if (this.filterCritieria.orderName == item.orderNumber) {
    //     debugger;
    //     return item;
    //   }
    // });
    console.log('out vehicles');
    console.log(this.vehicles);

    //this.vehicles = [];

    console.log('out vehicles');
    console.log(this.vehicles);
    // console.log(this.allOrdersOnMap);


    let orderStatusOrders = [];
    if (this.filterCritieria.status) {
      orderStatusOrders = this.allOrdersOnMap.filter((order) => {
        return order.status.id == this.filterCritieria.status;
      });
    }

    let orderNameOrders = [];
    if (this.filterCritieria.orderName) {
      orderNameOrders = this.allOrdersOnMap.filter((order) => {
        return order.code.includes(this.filterCritieria.orderName);
      });
    }
    this.ordersOnMap = this.merge_array(orderNameOrders, orderStatusOrders);

  }

  resetFilter() {
    this.vehicles = this.originalVehicles;
    this.filterCritieria = {driverName: '', vehicleNumber: '', orderName: '', factoryName: '', status: ''};
    this.ordersOnMap = this.allOrdersOnMap.slice();
  }

  getVehiclesLocation() {
    this._lookupService.GetallVehiclesLocation()
      .subscribe((vehicles) => {
          console.log('vehicles');
          this.originalVehicles = vehicles;
          this.vehicles = vehicles;
          console.log(this.vehicles);
        },
        err => {
          console.log(err);
        });
  }

  iconForItem(status) {
    if (status) {
      return this.mainURL + 'assets/Images/' + status + '.jpg';
    } else {
      return this.mainURL + 'assets/Images/empty.png';
    }
  }

  setLookups() {
    this._lookupService.GetallFactories().subscribe(
      items =>
        this.factories = items
    );
    this._lookupService.GetAllVehicleTypes().subscribe(
      items =>
        this.vehicle = items
    );
    this._lookupService.GetAllDrivers().subscribe(
      items =>
        this.drivers = items
    );
    this._lookupService.GetallStatus().subscribe(
      items =>
        this.status = items
    );
    let UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
    let UserID = JSON.parse(localStorage.getItem('currentUser')).id;
    //------------------- if Dispatcher ------------------------------
    //if ((UserAllRoles.indexOf('Dispatcher') > -1)) {
    //  this._customerService.GetAllWorkOrdersDispature().subscribe(WorkOrder => {
    //      this.orders = WorkOrder;
    //    },
    //    err => {
    //      // this.alertService.error('your session has timed out please login again !')
    //    });
    //}
    ////------------------- if MovementController ------------------------------
    //else if ((UserAllRoles.indexOf('MovementController') > -1)) {
    //  this._customerService.GetAllWorkOrdersMovementController().subscribe(WorkOrder => {
    //      this.orders = WorkOrder;
    //    },
    //    err => {
    //      // this.alertService.error('your session has timed out please login again !')
    //    });
    //}
    ////------------------- if Admin ------------------------------
    //else if ((UserAllRoles.indexOf('Admin') > -1)) {
    //  this._customerService.GetAllWorkOrdersMovementController().subscribe(WorkOrder => {
    //      this.orders = WorkOrder;
    //    },
    //    err => {
    //      // this.alertService.error('your session has timed out please login again !')
    //    });
    //}
  }

  getData() {
    return new Promise((resolve, reject) => {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.originalVehicles = [];
      if (currentUser && currentUser.token) {
        this._hubConnection = new HubConnection(Config.hub + currentUser.token);
        console.log('Token ==> ' + currentUser.token);
        this._hubConnection.start()
          .then(() => {
            //////// listen to notification event////////////////
            this._hubConnection.on(Config.liveLocation, (data: any) => {
              let vehicleIndex = this.originalVehicles.findIndex(item => item.vehicleNumber == data.vehicleNumber);
              console.log(data);
              if (vehicleIndex > -1) {
                this.originalVehicles[vehicleIndex] = data;
              }
              else {
                this.originalVehicles.push(data);
              }
              this.vehicles = this.originalVehicles;
            });
            console.log('Hub connection started');
            resolve();
          })
          .catch(err => {
            console.log('Error while establishing connection');
          });
      }
    });
  }

}
