
import {Component, OnDestroy} from '@angular/core';
import {UtilitiesService} from '../../operations/utilities.service';

@Component({
  selector: 'app-customer-search-page',
  templateUrl: './customer-search-page.component.html',
  styleUrls: ['./customer-search-page.component.css']
})
export class CustomerSearchPageComponent implements  OnDestroy {

  public UserExistance = '';
  public SearchInput = '';
  public allSearchCustomers: any[] = [];
  public latestCustomers: any[] = [];
  loading = false;

  constructor( private utilities: UtilitiesService) {
  }



  ngOnDestroy() {
    this.utilities.customerSearch = '';
  }


  

}
