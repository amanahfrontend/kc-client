import { contract, CustomerHistory } from './../../api-module/models/customer-model';
import { Subscription } from 'rxjs/Subscription';
import { AlertServiceService } from './../../api-module/services/alertservice/alert-service.service';
import { CustomerCrudService } from './../../api-module/services/customer-crud/customer-crud.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Component,ChangeDetectorRef } from '@angular/core';
import { ExistingCustomerService } from './existing-customer.service';

@Component({
  selector: 'app-existing-customer',
  templateUrl: './existing-customer.component.html',
  styleUrls: ['./existing-customer.component.css'],
  providers: [ExistingCustomerService]
})
export class ExistingCustomerComponent {
  public selectedTab = 'tab1';
  public existCust = false;
  private id: number;
  private sub: any;
  public ExistingCustomer: Subscription;
  customerCredit: number;
  customerBalance: number;
  public CustomerInfo: any;
  public ContractInfo: Array<contract>;
  public ComplainInfo: CustomerHistory;
  public WorkOrderInfo: CustomerHistory

  loadingDone: boolean = true;
  routerSub: Subscription;
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;


  constructor(private router: Router,
    public cdRef: ChangeDetectorRef,
    private route: ActivatedRoute,
    private customerService: CustomerCrudService,
    private alertService: AlertServiceService) {

    //this.existingCustomerService.updateContractInfo.subscribe(
    //  res => {
    //    console.log('we are here');
    //    this.GetCustDetails()
    //  }
    //);
    this.routerSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {

        if (event.url.includes("/search/customer/")) {

          this.routerSub = this.route.params.subscribe((params) => {
            if (params['CurrentCustomer']) {
              this.id = +params['CurrentCustomer'];
              this.GetCustDetails();
              this.selectedTab = 'tab1';

            }
          });

        }
      }
    });


  }

 

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  GetCustDetails() {
    this.loading = true;
    this.customerService.GetCustomerDetailsByIDAsync(this.id).subscribe(CustomerData => {
      if (CustomerData != null) {
        this.existCust = true;
        this.CustomerInfo = CustomerData;
        this.customerCredit = CustomerData.creditLimit;
        this.customerBalance = CustomerData.balance;
        this.loading = false;
      }
      else {
        this.router.navigate(['search']);

      }
    },
      err => {
        this.alertService.error('your session has timed out please login again !')
      });

  }


}
