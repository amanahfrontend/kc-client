import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { CustomerCrudService } from './../../../api-module/services/customer-crud/customer-crud.service';
import { workOrderData } from './../../../api-module/models/customer-model';
import { Component, OnInit ,ViewChild} from '@angular/core';
import { AuthenticationServicesService } from '../../../api-module/services/authentication/authentication-services.service';
import * as moment from 'moment';
import { Page } from '../../../shared-module/Page';
import { UtilitiesService } from '../../../operations/utilities.service';

@Component({
  selector: 'app-word-order',
  templateUrl: './word-order.component.html',
  styleUrls: ['./word-order.component.css', './../existing-customer.component.css']
})
export class WordOrderComponent implements OnInit {
  //--------------------  All Contracts Data Table Data ------------------
  WorkItem: any;
  price: any;
  availableAmount;
  public assignedFactory = '';
  public ListOfContracts: Array<any> = [];

  contractColumns: any[] = [
    { name: 'code' },
    { name: 'amount' },
    { name: 'startDate' },
    { name: 'endDate' },
    { name: 'fK_Contract_Id' },
    { name: 'fK_Priority_Id' },
    { name: 'fK_Status_Id' },
    { name: 'fK_Customer_Id' },
    { name: 'fK_Location_Id' },
    { name: 'pendingAmount' },
    { name: 'items' },
    { name: 'hasSuborders' },
  ];

  selectedWork = [];
  customerData: any;
  private id: number;
  private sub: any;
  public selectedState = 'assign';
  public workOrderInfo: any;
  public EditedWorkOrder: any = {};
  public AdddWorkOrder: any = {};
  public newOrder: workOrderData = {};
  public newOrderStringfied: any;
  public contractMixTypes: any[] = [];
  public newLocation: any = {};
  public AddedNewLocation: any = {};

  isSuperCallCenter: boolean;
  isCallCenter: boolean;
  isAdmin: boolean;
  isSales: boolean;
  public orderCount: number = 0;
  role: any = JSON.parse(localStorage.getItem('currentUser')).roles;
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;


  //@Output() Reset: EventEmitter<any> = new EventEmitter<any>();
  item: any;
  isOn: boolean;
  disableAfterSave: boolean;
  isDisabled: boolean;
  contractIds: any[] = [];
  ListOfLocations: any[] = [];
  contractMininmumDate: any;
  contractMaxdate: any;
  orderStartDate: any;
  OrderAltDate: any;
  contractCode: any;
  currentDate: any;
  selctedAmount: any;
  selectedItemId: any;
  checkAmountValidation: boolean;
  checkItemValidation: boolean;
  selectedStartDate: Date;
  selectedEndDate: any;
  workOrderObjectToEdit: any;
  movementRole: any;
  page: Page = new Page();
    confirmDelete: boolean=false;

  //@Input() set data(data) {
  //  this.workOrderInfo = data.workOrders;
  //  //  this.ListOfContracts = data.contracts;

  //  console.log('--------- workOrderInfo --------');
  //  console.log(this.workOrderInfo);
  //}

  @ViewChild('myTable') table: any;
  expanded: any = {};

  public rowHeight: number = 50;
  public headerHeight: number = 50;
  public editContract: boolean = false;
  public notEditOrderAllowed: boolean = false;

  public workOrderItems: Subscription;
  public Locations: any[] = [];
  public Periorities: Subscription;
  public Factories: Subscription;
  public allStatus: any[];
  public contractDetails: any = {};
  public allItemsToPoured: any[] = [];

  public minimumDate: any;
  public maximumDate: any;
  fDate: any;
  private isHideFromLoggedUser: boolean = false;
    selectedLang: string;
  // validAmount: boolean;
  // validAmount: boolean;

  constructor(private route: ActivatedRoute, private lookupService: LookupService, private utilities: UtilitiesService,
    private alertService: AlertServiceService, private customerService: CustomerCrudService,
    private auth: AuthenticationServicesService) {
    this.page.pageNumber = 1;
    this.page.pageSize = 5;
    if (window.localStorage.getItem('lang')) {
      console.log(window.localStorage.getItem('lang'));
      this.selectedLang = window.localStorage.getItem('lang');
    } else {
      this.selectedLang = this.utilities.languages[1].symbol;
    }

  }

  ngOnInit() {

    
    this.sub = this.route.params.subscribe(params => {
      this.id = params['CurrentCustomer'];
    });
    this.GetNonExpiredContracts();
    //this.GetAllItemPoured();
    this.CallNeededLookups();

    this.movementRole = this.auth.CurrentUser().roles.includes('MovementController');
    this.currentDate = new Date();
    //this.currentDate.setDate(this.currentDate.getDate() + 1);

    if (this.movementRole) {
      this.currentDate = new Date();

    }
    this.GetOrdersDetails({ offset: 0 });
    this.isSuperCallCenter = this.auth.CurrentUser().roles.includes('SuperCallCenter');
    this.isAdmin = this.auth.CurrentUser().roles.includes('Admin');
    this.isCallCenter = this.auth.CurrentUser().roles.includes('CallCenter');
    this.isHideFromLoggedUser = this.auth.CurrentUser().roles.includes('CallCenter') || this.auth.CurrentUser().roles.includes('superCallCenter');
    this.isSales = this.auth.CurrentUser().roles.includes('Sales');
  }



  GetOrdersDetails(pageNumber: any) {
    this.loading = true;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let pagingData = Object.assign({}, this.page);
    pagingData.pageNumber = this.page.pageNumber + 1;
    pagingData.id = this.id;
    this.customerService.GetDetailedWorkOrdersByCustomerIdAsync(pagingData).subscribe(contratcs => {
      this.workOrderInfo = contratcs.result;
      if (this.workOrderInfo != null && this.workOrderInfo != undefined) {
        this.workOrderInfo.map((workOrder) => {
          workOrder.startDate = new Date(workOrder.startDate);
          workOrder.startDate.setHours(workOrder.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          if (workOrder.endDate != null && new Date(workOrder.endDate).getFullYear() != 1 && new Date(workOrder.endDate).getFullYear() != 1970) {
            workOrder.endDate = new Date(workOrder.endDate);
            workOrder.endDate.setHours(workOrder.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());

          }
          else {
            workOrder.endDate = null;
          }

        });
      }


      this.page.totalElements = contratcs.totalCount;

      this.loading = false;

    },
      err => {
        this.alertService.error('your session has timed out please login again !')
      });
  }







  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.GetOrdersDetails(this.page.pageNumber);

  }



  checkIsValidToEdit(amount) {
    if (amount != this.selctedAmount)
      this.checkAmountValidation = true;
  }



  GetLocationByContractIds(): any {
    this.customerService.GetLocationByContractIds(this.contractIds).subscribe(locations => {
      this.ListOfLocations = locations;

      for (var i = 0; i < this.ListOfContracts.length; i++) {
        for (var j = 0; j < this.ListOfLocations.length; j++) {
          if (this.ListOfContracts[i].id == this.ListOfLocations[j].fk_Contract_Id) {
            if (this.ListOfContracts[i].lstLocationName == undefined)
              this.ListOfContracts[i].lstLocationName = [];
            this.ListOfContracts[i].lstLocationName.push({ name: this.ListOfLocations[j].title, id: this.ListOfLocations[j].id, contractId: this.ListOfContracts[i].id });
          }

        }

      }

      this.loading = false;

    });
  }






  GetNonExpiredContracts() {
 
    this.customerService.GetNonExpiredContracts(this.id).subscribe(contracts => {
      this.ListOfContracts = contracts;
      this.ListOfContracts.forEach(e => { this.contractIds.push(e.id) });
      console.log(this.contractIds);
      if (contracts.length > 0)
        this.customerData = contracts[0].customer;

      this.GetLocationByContractIds();

    });
  }




  stringfy(order) {
    this.newOrderStringfied = JSON.stringify(order);
    return this.newOrderStringfied;
  }



  //ngOnDestroy() {
  //  this.sub.unsubscribe();
  //}

  checkOrderPendingStatus(order) {
    let editOrderPendingState = order.workOrder.status.name.toLowerCase() == 'pending';
    let editOrderCancelledState = order.workOrder.status.name.toLowerCase() == 'cancelled';
    if (this.isSuperCallCenter || editOrderCancelledState) {
      this.notEditOrderAllowed = true;
    } else {
      if (editOrderPendingState || editOrderCancelledState) {
        this.notEditOrderAllowed = true;
      }
    }
  }

  // -------------- work order ------------

  public ListOfDividesFactories: Array<{ id: string, percentage: number }> = [];
  public TotalAssignedFactories = 0;
  public oneFactory = '';


  //Edit Mode For INFO details
  public editWorkOrder: boolean = false;
  public AddNewLocation: boolean = false;
  public addWorkOrder: boolean = false;
  public AddNewLocation2: boolean = false;


  //-------------- Create Factories ------------

  AddToListofFactories() {
    if (this.oneFactory.length != 0 && !this.checkExistanceOfFactory(this.oneFactory)) {
      this.ListOfDividesFactories.push({ 'id': this.oneFactory, 'percentage': 0 });
      this.oneFactory = '';
      console.log(this.ListOfDividesFactories);
    }
  }

  checkExistanceOfFactory(factory): boolean {
    for (let i of this.ListOfDividesFactories) {
      if (i['id'] == factory) {
        return true;
      }
    }
    return false;
  }

  getTotalPecentages() {

    let total = 0;
    for (let i of this.ListOfDividesFactories) {
      total = total + (i['percentage']);
    }
    this.TotalAssignedFactories = total;

  }

  DeleteFactoryPer(facto) {
    for (var i = 0; i < this.ListOfDividesFactories.length; i++) {

      if (this.ListOfDividesFactories[i]['id'] == facto) {

        this.ListOfDividesFactories.splice(i, 1);
      }
    }
    this.getTotalPecentages();
    console.log(this.ListOfDividesFactories);

  }

  // -------------------- Expandable -------------------

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  getRowDetailHeight(row: any, index: number): number {
    const myTable = this;
    if (!row) {
      return myTable['rows'][index].height;
    }
    return row.height;
  }

  getInnerTableHeight(row: any) {
    return (this.headerHeight + (this.rowHeight * row.subOrdersFactoryPercent.length)).toString() + 'px';
  }

  //----------------------------------------------------------


  getLocations(contractId) {
    this.loading = true;
    this.customerService.getLocationByContract(contractId).subscribe(locations => {
      this.Locations = locations;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  CallNeededLookups() {

    this.lookupService.GetAllItemPoured().
      subscribe(ItemPoured => {
        this.allItemsToPoured = ItemPoured;
      },
        err => {
          this.loading = false;
          this.alertService.error('error')
        });



    this.lookupService.GetallPeriorities().subscribe(Periorities => {
      console.log(Periorities);
      this.Periorities = Periorities;
      this.loading = false;
      console.log('per');
      this.newOrder.fK_Priority_Id = Periorities.find(x => x.name.toLowerCase() === 'normal').id;
      console.log(this.newOrder.fK_Priority_Id);
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetallStatus().subscribe(allStatus => {
      this.allStatus = allStatus;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });


    //this.lookupService.GetallWorkItems().subscribe(workOrderItems => {
    //  this.workOrderItems = workOrderItems;
    //  this.loading = false;
    //},
    //  err => {
    //    this.loading = false;
    //    this.alertService.error('your session has timed out please login again !');
    //  });

    //this.lookupService.GetallFactories().subscribe(Factories => {
    //  this.Factories = Factories;
    //},
    //  err => {
    //    this.loading = false;
    //    this.alertService.error('your session has timed out please login again !');
    //  });

    //this.lookupService.GetallGovernorates().subscribe(Governorates => {
    //  this.Governorates = Governorates;
    //  this.showG = false;
    //  this.loading = false;
    //},
    //  err => {
    //    this.loading = false;
    //    this.alertService.error('your session has timed out please login again !');
    //  });

   

  }

  GetContractMiXTypes(contractId) {
    return new Promise((resolve, reject) => {
      this.lookupService.GetContractMiXTypes(contractId).subscribe((contractMixTypes) => {
        this.contractMixTypes = contractMixTypes;
        this.contractMixTypes = this.contractMixTypes.filter(mix => mix.isDisabled == false);
        console.log(this.contractMixTypes);
        resolve();
      },
        err => {

        });
    });


  }
  GetContractMiXTypesNew(contractId) {
    this.contractMixTypes = [];
    console.log(this.id);
    this.ListOfContracts.forEach(x => {
      if (x.id == contractId)
        this.contractMixTypes = x.items;
    });
    this.contractMixTypes = this.contractMixTypes.filter(mix => mix.isDisabled == false);

  }
  GetContractById(contractId) {
    this.contractDetails = [];
    console.log(this.id);
    this.ListOfContracts.forEach(x => {
      if (x.id == contractId)
        this.contractDetails.push(x);
    });


    this.contractDetails[0].startDate = new Date(this.contractDetails[0].startDate);
    this.contractDetails[0].startDate.setHours(this.contractDetails[0].startDate.getHours() + this.customerService.getTimeOffsetTimeZone());

    if (this.contractDetails[0].endDate == null)
      this.contractDetails[0].endDate = null;
    else {
      this.contractDetails[0].endDate = new Date(this.contractDetails[0].endDate);
      this.contractDetails[0].endDate.setHours(this.contractDetails[0].endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
    }
  }
  getContractDetails(contractId) {
    this.reloadLoc(contractId);
    this.GetContractMiXTypesNew(contractId);
    this.GetContractById(contractId);
  }



  checkPriceValidaity(itemAmount) {

    itemAmount = parseInt(itemAmount);
    if (itemAmount <=0) {
      this.alertService.error("Quantity must be greater than zero");
      return false;
    }
    if (this.customerData.creditLimit == 1) {
      return true;
    }
    else if (this.item != undefined) {
      let total = itemAmount * this.item.price;
      if (this.customerData.debit == 0 && (this.customerData.creditLimit - this.customerData.balance) >= total) {
        return true;

      }
      else if (this.customerData.debit > 0 && this.customerData.debit >= total) {
        return true;

      }
      else if (this.customerData.debit > 0 && this.customerData.debit < total) {
        var diff = total - this.customerData.debit;
        if ((this.customerData.creditLimit - this.customerData.balance) >= diff) {
          return true;

        }
        else {
          this.alertService.error("You have exceeds your credit limit");

          return false;

        }


      }
      else {
        this.alertService.error("You have exceeds your credit limit");

        return false;

      }
    }

  }




  SaveEdits() {
    this.loading = true;

    if (this.checkAmountValidation == true || this.checkItemValidation == true) {
      if (this.checkPriceValidaity(this.selectedWork[0].workOrder.amount) == false)
        return false;
    }
    if (this.checkAddDateValidaty(this.selectedWork[0].workOrder) == false) {
      this.isDisabled = false;
      this.isOn = true;
      this.loading = false;

      return false;

    };
    this.EditedWorkOrder.workOrder = {};
    this.EditedWorkOrder.workOrder.id = this.selectedWork[0].workOrder.id;
    this.EditedWorkOrder.workOrder.code = this.selectedWork[0].workOrder.code;
    this.EditedWorkOrder.workOrder.amount = this.selectedWork[0].workOrder.amount;
    this.EditedWorkOrder.workOrder.description = this.selectedWork[0].workOrder.description;
    this.EditedWorkOrder.workOrder.fK_ElementToBePoured_Id = this.selectedWork[0].workOrder.fK_ElementToBePoured_Id;

    this.EditedWorkOrder.workOrder.fK_Location_Id = this.selectedWork[0].workOrder.fK_Location_Id;
    this.EditedWorkOrder.workOrder.fK_Customer_Id = this.selectedWork[0].workOrder.fK_Customer_Id;
    this.EditedWorkOrder.workOrder.fK_Contract_Id = this.selectedWork[0].workOrder.fK_Contract_Id;
    this.EditedWorkOrder.workOrder.fK_Priority_Id = this.selectedWork[0].workOrder.fK_Priority_Id;
    this.EditedWorkOrder.workOrder.fk_Status_Id = this.selectedWork[0].workOrder.fK_Status_Id;

    this.EditedWorkOrder.workOrder.pendingAmount = this.selectedWork[0].workOrder.pendingAmount;

    if (this.date) {
      let date = new Date(this.date);
      this.EditedWorkOrder.workOrder.startDate = date.toISOString();
    }
    else {
      let date = new Date(this.selectedWork[0].workOrder.startDate);
      this.EditedWorkOrder.workOrder.startDate = date.toISOString();
    }

    if (this.selectedWork[0].workOrder.endDate != undefined) {
      let endDate = new Date(this.selectedWork[0].workOrder.endDate);
      this.EditedWorkOrder.workOrder.endDate = endDate.toISOString();;
    }



    this.EditedWorkOrder.workOrder.fK_Item = this.selectedWork[0].workOrder.fK_Item;

    console.log('EditedWorkOrder');
    console.log(this.EditedWorkOrder);
    console.log(this.selectedWork[0]);
    delete this.EditedWorkOrder.priority;
    delete this.EditedWorkOrder.status;

    if (this.selectedWork[0].workOrder.fK_Status_Id == 4 || this.selectedWork[0].workOrder.fK_Status_Id == 10) {
      this.customerService.EditWorkOrderCancelled(this.selectedWork[0].workOrder.id, 4).subscribe(state => {
        console.log('EditedWorkOrder In Service ');
        console.log(state);
        this.alertService.success('order Updated Successfully');

        this.Resetall();
        this.loading = false;

      },
        err => {
          this.alertService.error('Server Error !!');
          this.loading = false;

        });
    }
    else {
      if (this.EditedWorkOrder.workOrder.amount != undefined) {
        if (!this.selectedWork[0].workOrder.fK_Location_Id) {
          this.alertService.error('you should select location first');
          this.loading = false;

        }
        else {

          this.customerService.EditWorkOrderServ(this.EditedWorkOrder).subscribe(state => {
            this.alertService.success('order Updated Successfully');
            this.Resetall();
            this.loading = false;


          },
            err => {
              this.alertService.error('Server Error !!');
              this.loading = false;

            });
        }
      }
    }
  }

  getAvailableAmount(itemId, show?: any) {

    //this.checkPriceValidaity(itemId, itemPrice);
    this.item = this.contractMixTypes.find(x => x.fk_Item_Id == itemId);

    console.log('in change');
    console.log(itemId);
    console.log(this.contractMixTypes);
    this.contractMixTypes.map((type) => {
      console.log(type.fk_Item_Id);
      console.log(itemId);
      if (type.fk_Item_Id == itemId) {
        console.log(type.availableAmount);
        this.availableAmount = type.availableAmount;
      }
    });
    if (show == "close")
      return;


    if (itemId == this.selectedItemId) {
      this.checkItemValidation = true;
      return false;
    }
    //if (this.newOrder.amount != undefined)
    //  this.checkPriceValidaity(this.newOrder.amount);
    //else if (this.selectedWork[0] != undefined) {
    //  if (this.selectedWork[0].workOrder.amount != undefined)
    //    this.checkPriceValidaity(this.selectedWork[0].workOrder.amount);
    //}

  }

  validateDate(date: any) {
    let selctedDate = new Date(date);
    let minimumDate = new Date();
    minimumDate.setDate(minimumDate.getDate() - 1);
    minimumDate = minimumDate;
    if (selctedDate.getTime() < minimumDate.getTime()) {
      this.alertService.error("The start date must be greater than today or equal");
    }
  }
  EditWorkOrder() {
    if (this.selectedWork[0].fK_Status_Id == 2 || this.selectedWork[0].fK_Status_Id == 4) {
      this.workOrderObjectToEdit = this.selectedWork[0];
      this.editWorkOrder = true;
    }
    else {
      this.alertService.info('this order not editable')
    }

     this.selectedWork.push({});
      this.selectedWork[0].workOrder = this.workOrderObjectToEdit;

    if (this.selectedWork[0].workOrder.fK_Status_Id == 2 || this.selectedWork[0].workOrder.fK_Status_Id == 4) {
      this.checkOrderPendingStatus(this.selectedWork[0]);

      this.getContractDetails(this.selectedWork[0].workOrder.fK_Contract_Id);
             this.getAvailableAmount(this.selectedWork[0].workOrder.fK_Item, "close");

      this.reloadLoc(this.selectedWork[0].workOrder.fK_Contract_Id);


      this.selctedAmount = this.selectedWork[0].workOrder.amount;
      this.selectedItemId = this.selectedWork[0].workOrder.fK_Item;

      this.selectedWork[0].workOrder.oldAmount = this.selectedWork[0].workOrder.amount;

      this.selectedWork[0].workOrder.fK_ElementToBePoured_Id = this.selectedWork[0].workOrder.fK_ElementToBePoured_Id;

      this.selectedStartDate = new Date(this.selectedWork[0].workOrder.startDate);
      this.selectedWork[0].workOrder.startDate = new Date(this.selectedWork[0].workOrder.startDate);
      //this.selectedWork[0].workOrder.startDate.setHours(this.selectedWork[0].workOrder.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());

      if (new Date(this.selectedWork[0].workOrder.endDate) && new Date(this.selectedWork[0].workOrder.endDate).getFullYear() != 1 && new Date(this.selectedWork[0].workOrder.endDate).getFullYear() != 1970) {
        this.selectedWork[0].workOrder.endDate = new Date(this.selectedWork[0].workOrder.endDate);
        this.selectedEndDate = new Date(this.selectedWork[0].workOrder.endDate);
        //if (this.workOrderObjectToEdit.source != "Order Confirmation")
        //    this.selectedWork[0].workOrder.endDate.setHours(this.selectedWork[0].workOrder.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());

      }
      else {
        this.selectedEndDate = new Date(this.selectedWork[0].workOrder.endDate);;
        this.selectedWork[0].workOrder.endDate = null;
        this.selectedWork[0].workOrder.endDate = null;
      }

      this.WorkItem = this.selectedWork[0].workOrder.fK_Item;


    }
    else {
      this.alertService.info('this order not editable')
    }
    console.log(this.selectedState);
  }

  private date: any = '';


  AddWorkOrder() {
  
    this.loading = true;
    let endDate;
    let startDate = moment(this.newOrder.startDate).local();
    if (this.newOrder.endDate != undefined)
      endDate = moment(this.newOrder.endDate).local();
    let currentDate = moment(this.currentDate).local();


    if (startDate.isBefore(currentDate)) {
      this.alertService.error("Start date can't be less than current day ");
      this.loading = false;
      return false;

    }
    if (endDate != undefined && endDate.isBefore(currentDate)) {
      this.alertService.error("End date can't be less than current day ");
      this.loading = false;
      return false;
    }
    this.isDisabled = true;
    this.isOn = false;
    if (this.checkPriceValidaity(this.newOrder.amount) == false) {
      this.isDisabled = false;
      this.isOn = true;
      this.loading = false;

      return false;

    }
   
    this.AdddWorkOrder.workOrder = {};
    this.AdddWorkOrder.workOrder.amount = this.newOrder.amount;
    this.AdddWorkOrder.workOrder.description = this.newOrder.description;
    this.AdddWorkOrder.workOrder.fK_ElementToBePoured_Id = this.newOrder.fK_ElementToBePoured_Id;

    this.AdddWorkOrder.workOrder.fK_Customer_Id = this.id + '';
    this.AdddWorkOrder.workOrder.fK_Contract_Id = this.newOrder.fK_Contract_Id;
    if (this.newOrder.fK_Contract_Id != undefined) {
      let selctedContract = this.ListOfContracts.find(contract => contract.id == this.newOrder.fK_Contract_Id);

      if (selctedContract.lstLocationName.length > 0) {
        for (var j = 0; j < selctedContract.lstLocationName.length; j++) {
          if (selctedContract.lstLocationName[j].contractId == this.newOrder.fK_Contract_Id) {
            this.AdddWorkOrder.workOrder.fK_Location_Id = selctedContract.lstLocationName[j].id;
          }
        }

      }

    }

    if (this.AdddWorkOrder.workOrder.fK_Location_Id > 0) {
      this.newOrder.fK_Location_Id = this.AdddWorkOrder.workOrder.fK_Location_Id;
    }
    this.AdddWorkOrder.workOrder.fK_Priority_Id = this.newOrder.fK_Priority_Id;
    this.AdddWorkOrder.workOrder.fk_Status_Id = this.newOrder.fk_Status_Id;
    // this.AdddWorkOrder.workOrder.pendingAmount = this.newOrder.pendingAmount;
    this.AdddWorkOrder.workOrder.startDate = new Date(this.newOrder.startDate).toISOString();
    if (this.newOrder.endDate) {
      this.AdddWorkOrder.workOrder.endDate = new Date(this.newOrder.endDate).toISOString();
    }
    else {
      this.AdddWorkOrder.workOrder.endDate = null;
    }



    this.AdddWorkOrder.workOrder.fK_Item = this.WorkItem;

    this.AdddWorkOrder.workOrder.items = {};
    if (this.WorkItem != '') {
      //this.AdddWorkOrder.workOrder.items.push({ 'id': this.WorkItem });
      this.AdddWorkOrder.workOrder.items.id = this.WorkItem;

    }

    console.log(this.availableAmount);
    console.log(this.AdddWorkOrder);


    if (this.checkAddDateValidaty(this.newOrder) == false) {
      this.isDisabled = false;
      this.isOn = true;
      this.loading = false;

      return false;

    };


    if (this.AdddWorkOrder.workOrder.amount > 0) {
      console.log('AdddWorkOrder');
      console.log(this.AdddWorkOrder);
      if (!this.newOrder.fK_Location_Id) {
        this.alertService.error('you should select location first');
        this.isDisabled = false;
        this.isOn = true;
        this.loading = false;
      }
      //else if (this.checkPriceValidaity(this.WorkItem) == false) {
      //  this.isDisableSaveAdd = false;
      //  this.loading = false;
      //  return false;

      //}
      else {
        this.AdddWorkOrder.creatingOrderFirstTime = true;
        this.customerService.AddNewWorkOrder(this.AdddWorkOrder).subscribe(state => {
          console.log(state);
          this.loading = false;
          this.alertService.success('Work Order Added Successfully');
          this.Resetall();
        },
          err => {
            this.loading = false;
            this.isDisabled = false;
            this.isOn = true;
            // this.isDisableSaveAdd = true;
            this.alertService.error('Server Error !!');
          });
      }

    }

  }


  checkAddDateValidaty(currentOrder): any {
    let order = currentOrder;
    let contractMaxdate: any;
    let contractMininmumDate = moment(this.contractDetails[0].startDate);
    let OrderAltDate = moment(order.endDate);
    let orderStartDate = moment(order.startDate);
    let currentdate = moment(new Date());



    if (order.endDate != null && new Date(order.endDate).getFullYear() != 1 && new Date(order.endDate).getFullYear() != 1970) {

      if (OrderAltDate.isBefore(currentdate)) {
        this.alertService.error("The AL date must be greater than today");
        return false;
      }

      if (OrderAltDate.isBefore(contractMininmumDate, 'day')) {
        this.alertService.error("The order ALT date must be greater than contract start date : " + contractMininmumDate.format("YYYY-MM-DD"));
        return false;
      }
    }
    else {
      OrderAltDate = null;
    }

    if (orderStartDate.isBefore(currentdate)) {
      this.alertService.error("The start date must be greater than today");
      return false;
    }


    if (orderStartDate.isBefore(contractMininmumDate, 'day')) {
      this.alertService.error("The order start date must be greater than contract start date : " + contractMininmumDate.format("YYYY-MM-DD"));
      return false;
    }

    if (this.contractDetails[0].endDate != null && new Date(this.contractDetails[0].endDate).getFullYear() != 1 && new Date(this.contractDetails[0].endDate).getFullYear() != 1970) {

      contractMaxdate = moment(this.contractDetails[0].endDate);
      if (OrderAltDate != null) {
        if (OrderAltDate.isAfter(contractMaxdate, 'day')) {
          this.alertService.error("The order alt date can't be exceed the contract end date: " + contractMaxdate.format("YYYY-MM-DD"))
          return false;

        }

      }
     
      if (orderStartDate.isAfter(contractMaxdate, 'day')) {
        this.alertService.error("The order start date can't be exceed the contract end date: " + contractMaxdate.format("YYYY-MM-DD"))
        return false;

      }

    
    }


  }

  getSelectedName(list, id) {
    if (list != null) {
      for (var i = 0; i < list.length; i++) {
        if (list[i].id == id) {
          return list[i].name;
        }
      }
    }

  }

  Resetall() {
    this.GetOrdersDetails({ offset: 0 });
    this.ListOfDividesFactories = [];
    this.WorkItem = '';
    this.selectedState = '';
    this.ListOfDividesFactories = [];
    this.assignedFactory = '';
    this.selectedWork = [];
    this.EditedWorkOrder = {};
    this.AdddWorkOrder = {};
    this.newOrder = {
      fK_Priority_Id: this.newOrder.fK_Priority_Id
    };
    this.editWorkOrder = false;
    this.AddNewLocation = false;
    this.AddNewLocation2 = false;
    this.Locations = [];
    this.addWorkOrder = false;
    // this.workOrderInfo = [];
    // this.ListOfContracts = [];
  }
  enableButton() {
    this.addWorkOrder = true;
    this.isOn = true;
    this.isDisabled = false;

  }



  reloadLoc(contractId) {
    this.Locations = [];
    console.log(this.id);
    this.ListOfLocations.forEach(x => {
      if (x.fk_Contract_Id == contractId)
        this.Locations.push(x)

    });
   
  }

  DeleteItem() {
    this.confirmDelete = true;
  }
  cancelDelete() {
    this.confirmDelete = false;
  }
  ConfirmDelete() {
    this.customerService.DeleteWorkOrder(this.selectedWork[0].id).subscribe(state => {
      console.log(state);
      if (state) {
        this.Resetall();
        this.confirmDelete = false;

        this.alertService.success(' Deleted Successfully');
      }
      else {
        this.alertService.error('Failed To delete , this order in use');
        this.confirmDelete = false;

      }
    },
      err => {
        this.alertService.error('Failed To delete , this order in use');
        this.confirmDelete = false;

      });
  }



  keyPressNegative(e: any) {
    if (!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58)
      || e.keyCode == 8)) {
      return false;
    }
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

}


