import { complain,customer } from './../../../api-module/models/customer-model';
import { ActivatedRoute } from '@angular/router';
import { CustomerCrudService } from './../../../api-module/services/customer-crud/customer-crud.service';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { AuthenticationServicesService } from '../../../api-module/services/authentication/authentication-services.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-complains',
  templateUrl: './complains.component.html',
  styleUrls: ['./complains.component.css','./../existing-customer.component.css']
})
export class ComplainsComponent implements OnInit {
  public firstSubmitTry = false;
  public firstSubmitTryadd=false;
  public isDirty: boolean = false;
  
  public ComplainInfo: Array<any>;
  public newComplain:complain={};
  public Customer:customer;
  private id: number;
  private sub: any;
  isSales: boolean;
  public editComplains: boolean = false;
  public AddComplains:boolean=false;

  role:any = JSON.parse(localStorage.getItem('currentUser')).roles;

  complainsColumns: any[] = [
    { name: 'fK_Customer_Id' },
    { name: 'note' }

  ];
  selectedComplain = [];
  

    public loadingImage: string = "assets/Images/loading.gif"
    loading:boolean = false;

  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private customerService: CustomerCrudService,
    private auth: AuthenticationServicesService) { }
  
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['CurrentCustomer'];
      if (this.id>0) {
        this.getCustomerByID();
      }
    });
    this.getAllComplains();


    this.isSales = this.auth.CurrentUser().roles.includes('Sales');
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }



  ResetCustInfo() {
    this.getAllComplains();
    this.editComplains = false;
    this.AddComplains=false;
    this.selectedComplain = [];
    this.newComplain={};
    this.firstSubmitTryadd=false;
    this.firstSubmitTry=false;

  }
  getCustomerByID() {


    this.customerService.GetSelctedCustomer(this.id).
      subscribe(customer => {
        this.Customer = customer;
      },
        err => {
          this.loading = false;
          this.alertService.error('error')
        });
  }
  getAllComplains() {


    this.customerService.getAllComplains(this.id).
      subscribe(ComplainInfo => {
        this.ComplainInfo = ComplainInfo;
      },
        err => {
          this.loading = false;
          this.alertService.error('error')
        });
  }


  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editComplains = false;
      }
      else {
        this.Editcomplain();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }
  onAddContract(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddComplains = false;
      }
      else {
        this.Addcomplain();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  Editcomplain() {
    this.loading = true;
    this.customerService.EditComplain(this.selectedComplain[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetCustInfo();
          this.loading = false;
          this.alertService.success(' Updated Successfully');
          this.editComplains=false
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }

  Addcomplain() {
    this.loading = true;
    this.newComplain.fK_Customer_Id = this.id+"";

    console.log(this.newComplain);
    this.customerService.AddComplain(this.newComplain).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetCustInfo();
          this.loading = false;
          this.alertService.success(' Updated Successfully')
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }
    

    //----------------- Complains Functions -----------------
    
    DeleteItem(){
      this.customerService.DeleteComplain(this.selectedComplain[0].id).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetCustInfo();
          this.alertService.success(' Deleted Successfully')
        }
        else {
          this.alertService.error('failed To Delete')
        }
      },
      err => {
        this.alertService.error('we have Error !!')
      });
    }


}
