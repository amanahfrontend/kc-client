import { CustomerCrudService } from './../../../api-module/services/customer-crud/customer-crud.service';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { AuthenticationServicesService } from '../../../api-module/services/authentication/authentication-services.service';

@Component({
  selector: 'app-customerinfo',
  templateUrl: './customerinfo.component.html',
  styleUrls: ['./customerinfo.component.css', './../existing-customer.component.css']
})

export class CustomerinfoComponent implements OnInit, OnChanges {


  numberCount: number = 8;


  public editMode = false;

  public firstSubmitTry = false;
  editLocation = false;
  kcrmIsExist: boolean;
  isOn: boolean;
  isDisabled: boolean;
  public isHideFromLoggedUser: boolean = false;

  @Output() Reset: EventEmitter<any> = new EventEmitter<any>();

  // --------- Lookups ---------------
  public CustomerType: Subscription;
  public CustomerCategory: Subscription;
  public Locations: Subscription;
  @Input() CustomInfo: any;

  public loadingImage: string = "assets/Images/loading.gif"
  loading: boolean = false;
  currentCustomerCreditLimit: any;
  currentCustomerBalance: any;
  kcrmId: any;
  totalPhones: any = [1];
  totalPhonesData: any = [];
  phoneState: boolean;
  messageError: string;
  noBlocks: boolean;
  isBlocksExist: boolean =true;
  isSales: boolean = false;
  // @Input() set data(data) {
  //   this.CustomInfo = data;
  //   console.log("--------- Customer --------");
  //   console.log(this.CustomInfo);
  // }

  constructor(private customerService: CustomerCrudService, private lookupService: LookupService, private alertService: AlertServiceService, private auth: AuthenticationServicesService) {
  }

  ngOnInit() {
    if (this.CustomInfo.kcrM_Customer_Id != undefined)
      this.kcrmId = this.CustomInfo.kcrM_Customer_Id;

    if (this.CustomInfo.phones.length == 0) {
      this.totalPhones = (JSON.parse(JSON.stringify(this.CustomInfo.phones))); 
      this.totalPhones = [1];

    }
    else
      this.totalPhones = (JSON.parse(JSON.stringify(this.CustomInfo.phones))); 

    this.CallNeededLookups();

    this.isHideFromLoggedUser = this.auth.CurrentUser().roles.includes('CallCenter') || this.auth.CurrentUser().roles.includes('superCallCenter');

    this.isSales = this.auth.CurrentUser().roles.includes('Sales');


 
  }


    checkIsValidtoSaveCustomer() {

        if (this.CustomInfo.creditLimit == 1) {
            return true;
        }
    else if (this.CustomInfo.creditLimit < this.currentCustomerCreditLimit) {
      let diff = this.currentCustomerCreditLimit - this.CustomInfo.creditLimit;
      if (this.CustomInfo.balance < diff) {
        this.firstSubmitTry = true;
        this.alertService.error("Your balance is less than your credit limit !")
        return false;
      }
    };

  }

  KcrmIsExist(kcrmId: any): any {
    this.customerService.KCRMIdExist(kcrmId).
      subscribe(response => {
        this.kcrmIsExist = response;
        if (this.kcrmIsExist) {
          this.isDisabled = true;
          this.isOn = false;
          this.alertService.error("KCRM is already exist ");
          return true;
        }
        else if (this.kcrmIsExist == false) {
          this.EditInfo();
        }
      },
        err => {
        })

  }
  ngOnChanges() {
    if (this.CustomInfo.id) {
      console.log(this.CustomInfo.id);
      //  this.getLocationByCustomer();
    }
  }
  onSubmit(form) {
    if (this.checkIsValidtoSaveCustomer() == false) return false;

    if (this.CustomInfo != undefined) {

      console.log('Valid Form');
      console.log(form);
      if (this.kcrmId != this.CustomInfo.kcrM_Customer_Id)
        this.KcrmIsExist(this.CustomInfo.kcrM_Customer_Id);
      else
        this.EditInfo();

    }
    else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');
    }
  }


  checkPhone(phoneValue) {
 
    let index = phoneValue.charAt(0);
    if (index == 1) {
      if (phoneValue.length != 7) {
        this.alertService.error("Hotline should be only 7 numbers ");
        this.messageError="Hotline should be only 7 numbers ";
        return false;
      }
      else {
        return true;
      }
    }
    else {
      if (phoneValue.length != 8) {
        this.alertService.error("Mobile should be only 8 numbers ");
        this.messageError="Mobile should be only 8 numbers";

        return false;
      }
      else {
        return true;
      }
    }

  }


  AddPhoneItem() {

    this.totalPhones.push(1);
    this.totalPhonesData = this.CustomInfo.phones;
    this.CustomInfo.phones = [];
    this.CustomInfo.phones = this.totalPhonesData;
  }

  DeletePhoneItem(index) {

    this.totalPhones.splice(index, 1);
    this.CustomInfo.phones.splice(index, 1);
    this.totalPhonesData = this.CustomInfo.phones;
    this.CustomInfo.phones = [];
    this.CustomInfo.phones = this.totalPhonesData;
  }

  eventHandler(event, i) {

    if (this.CustomInfo.phones[i] != undefined) {
      let index = this.CustomInfo.phones[i].charAt(0);
      if (index == 1)
        this.numberCount = 7;
      else
        this.numberCount = 8;
    }

    console.log(event, event.keyCode, event.keyIdentifier);
    if (event.which < 48 || event.which > 57) {
      event.preventDefault();
    }
  }
  private checkAllPhones() {
    for (let i = 0; i <  this.CustomInfo.phones.length; i++) {
      let result = this.checkPhone(this.CustomInfo.phones[i]);
      if (result == false) {
        this.phoneState = false;
        break;
      }
      else {
        this.phoneState = true;
      }
    }
  }



  EditInfo() {
    this.loading = true;
    console.log(this.CustomInfo);
   this.checkAllPhones();
    if (this.phoneState == false) {
      this.loading = false;
      return false;
    }

    this.customerService.EditCustomerInfo(this.CustomInfo).subscribe(users => {
      console.log(users);
      if (users) {
        this.Reset.emit();
        this.editMode = false;
        this.loading = false;
        this.alertService.success('user Updated Successfully');
      }
      else {
        this.loading = false;
        this.alertService.error('failed To update');
      }
    },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!');
      });
  }

  ResetCustInfo() {
    this.Reset.emit();

    console.log('--------------------');
    console.log(this.CustomInfo);
    this.currentCustomerCreditLimit = this.CustomInfo.creditLimit;
    this.currentCustomerBalance = this.CustomInfo.balance;

    this.editMode = false;
  }

  CallNeededLookups() {
    this.loading = true;
    this.lookupService.GetAllCustomerTypes().subscribe(types => {
      this.CustomerType = types;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetAllCustomerCategories().subscribe(categories => {
      this.CustomerCategory = categories;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

  

  }


  /******** location dialog *********/

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

}
