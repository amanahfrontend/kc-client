import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from "@angular/core";
import { contract, WorkOrderDetails, workOrderData } from "../../../api-module/models/customer-model";
import { Subscription } from "rxjs";
import { UtilitiesService } from "../../../operations/utilities.service";
import { ActivatedRoute, Router } from "@angular/router";
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { AlertServiceService } from "../../../api-module/services/alertservice/alert-service.service";
import { CustomerCrudService } from "../../../api-module/services/customer-crud/customer-crud.service";
import { AuthenticationServicesService } from "../../../api-module/services/authentication/authentication-services.service";

import * as moment from 'moment';


@Component({
  selector: 'app-word-order-edit',
  templateUrl: './word-order-edit.component.html',
  styleUrls: ['./word-order-edit.component.css', './../existing-customer.component.css']
})
export class WordOrderEditComponent implements OnInit {
  //--------------------  All Contracts Data Table Data ------------------
  WorkItem: any;
  price: any;
  availableAmount;
  public assignedFactory = '';
  public ListOfContracts: Array<contract> = [];

  contractColumns: any[] = [
    { name: 'code' },
    { name: 'amount' },
    { name: 'startDate' },
    { name: 'endDate' },
    { name: 'fK_Contract_Id' },
    { name: 'fK_Priority_Id' },
    { name: 'fK_Status_Id' },
    { name: 'fK_Customer_Id' },
    { name: 'fK_Location_Id' },
    { name: 'pendingAmount' },
    { name: 'items' },
    { name: 'hasSuborders' },
  ];

  selectedWork = [];
  customerData: any;
  private id: number;
  private isDisableSaveAdd: boolean = false;
  private sub: any;
  public selectedState = 'assign';
  public workOrderInfo: Array<WorkOrderDetails>;
  public EditedWorkOrder: WorkOrderDetails = {};
  public AdddWorkOrder: any = {};
  public newOrder: workOrderData = {};
  public newOrderStringfied: any;
  public contractMixTypes: any[] = [];
  public newLocation: any = {};
  public AddedNewLocation: any = {};
  public PACI = '';
  public showG: boolean = true;
  public showB: boolean = true;
  public showA: boolean = true;
  public showS: boolean = true;
  isSuperCallCenter: boolean;
  isCallCenter: boolean;
  isAdmin: boolean;
  isSales: boolean;
  public orderCount: number = 0;
  role: any = JSON.parse(localStorage.getItem('currentUser')).roles;
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;


  @Output() Reset: EventEmitter<any> = new EventEmitter<any>();
  item: any;
  isOn: boolean;
  disableAfterSave: boolean;
  isDisabled: boolean;
  contractIds: any[] = [];
  ListOfLocations: any[] = [];
  contractMininmumDate: any;
  contractMaxdate: any;
  orderStartDate: any;
  OrderAltDate: any;
  contractCode: any;
  currentDate: any;
  selctedAmount: any;
  selectedItemId: any;
  checkAmountValidation: boolean;
  checkItemValidation: boolean;
  selectedStartDate: Date;
  selectedEndDate: any;
  showSave: boolean = true;
  selectedPouringTime: Date;

  @Input() set data(data) {
    this.workOrderInfo = data.workOrders;
    //  this.ListOfContracts = data.contracts;

    console.log('--------- workOrderInfo --------');
    console.log(this.workOrderInfo);
  }

  @ViewChild('myTable') table: any;
  expanded: any = {};
  @Input() editWorkOrder: boolean = false;
  @Input() workOrderObjectToEdit: any;
  public rowHeight: number = 50;
  public headerHeight: number = 50;
  public editContract: boolean = false;
  public notEditOrderAllowed: boolean = false;

  public workOrderItems: Subscription;
  public Locations: any[] = [];
  public Periorities: Subscription;
  public Factories: Subscription;
  public Governorates: Subscription;
  public areas: any[];
  public blocks: any[];
  public streets: any[];
  public allStatus: any[];
  public contractDetails: any = {};
  public allItemsToPoured: any[] = [];

  public minimumDate: any;
  public maximumDate: any;
  fDate: any;
  private isHideFromLoggedUser: boolean = false;
  // validAmount: boolean;
  // validAmount: boolean;

  constructor(private utilitiesService: UtilitiesService, private route: ActivatedRoute, private lookupService: LookupService, private alertService: AlertServiceService, private customerService: CustomerCrudService, private auth: AuthenticationServicesService, private router: Router) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      if (params['CurrentCustomer'] != undefined) {
        this.id = params['CurrentCustomer'];

      }
    });
    this.currentDate = new Date();
    this.EditWorkOrder();
    this.GetAllItemPoured();
    this.GetNonExpiredContracts();

    this.CallNeededLookups();
    // this.reloadLocation();
    this.isSuperCallCenter = this.auth.CurrentUser().roles.includes('SuperCallCenter');
    this.isAdmin = this.auth.CurrentUser().roles.includes('Admin');
    this.isCallCenter = this.auth.CurrentUser().roles.includes('CallCenter');

    this.isHideFromLoggedUser = this.auth.CurrentUser().roles.includes('CallCenter') || this.auth.CurrentUser().roles.includes('superCallCenter');

    this.isSales = this.auth.CurrentUser().roles.includes('Sales');
    console.log(this.isSales)


    if (this.workOrderObjectToEdit.isDeclined == 1) {
      this.notEditOrderAllowed = false;
      this.showSave = false;
    }

  }

  GetAllItemPoured() {
    this.loading = true;
    this.lookupService.GetAllItemPoured().
      subscribe(ItemPoured => {


        this.allItemsToPoured = ItemPoured;
        this.loading = false;
      },
        err => {
          this.loading = false;
          this.alertService.error('error')
        });
  }

  checkIsValidToEdit(amount) {
    if (amount != this.selctedAmount)
      this.checkAmountValidation = true;
  }

  GetLocationByContractIds(): any {
    this.customerService.GetLocationByContractIds(this.contractIds).subscribe(locations => {
      this.ListOfLocations = locations;

      for (var i = 0; i < this.ListOfContracts.length; i++) {
        for (var j = 0; j < this.ListOfLocations.length; j++) {
          if (this.ListOfContracts[i].id == this.ListOfLocations[j].fk_Contract_Id) {
            if (this.ListOfContracts[i].lstLocationName == undefined)
              this.ListOfContracts[i].lstLocationName = [];
            this.ListOfContracts[i].lstLocationName.push({ name: this.ListOfLocations[j].title, id: this.ListOfLocations[j].id, contractId: this.ListOfContracts[i].id });
          }

        }

      }

      this.loading = false;

      console.log(this.ListOfContracts)
    });
  }
  GetNonExpiredContracts(id?: any) {
    if (id != undefined) {
      this.id = id;
    }
    this.customerService.GetNonExpiredContracts(this.id).subscribe(contracts => {
      this.loading = true;
      this.ListOfContracts = contracts;
      this.ListOfContracts.forEach(e => { this.contractIds.push(e.id) });
      console.log(this.contractIds);
      if (contracts.length > 0)
        this.customerData = contracts[0].customer;

      this.GetLocationByContractIds();
      console.log(this.ListOfContracts)
    });
  }


  stringfy(order) {
    this.newOrderStringfied = JSON.stringify(order);
    return this.newOrderStringfied;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  checkOrderPendingStatus(order) {
    let editOrderPendingState = order.workOrder.status.name.toLowerCase() == 'pending';
    let editOrderCancelledState = order.workOrder.status.name.toLowerCase() == 'cancelled';
    if (this.isSuperCallCenter || editOrderCancelledState) {
      this.notEditOrderAllowed = true;
    } else {
      if (editOrderPendingState || editOrderCancelledState) {
        this.notEditOrderAllowed = true;
      }
    }
  }

  // -------------- work order ------------

  public ListOfDividesFactories: Array<{ id: string, percentage: number }> = [];
  public TotalAssignedFactories = 0;
  public oneFactory = '';


  //Edit Mode For INFO details
  public AddNewLocation: boolean = false;
  public addWorkOrder: boolean = false;
  public AddNewLocation2: boolean = false;


  //-------------- Create Factories ------------

  AddToListofFactories() {
    if (this.oneFactory.length != 0 && !this.checkExistanceOfFactory(this.oneFactory)) {
      this.ListOfDividesFactories.push({ 'id': this.oneFactory, 'percentage': 0 });
      this.oneFactory = '';
      console.log(this.ListOfDividesFactories);
    }
  }

  checkExistanceOfFactory(factory): boolean {
    for (let i of this.ListOfDividesFactories) {
      if (i['id'] == factory) {
        return true;
      }
    }
    return false;
  }

  getTotalPecentages() {

    let total = 0;
    for (let i of this.ListOfDividesFactories) {
      total = total + (i['percentage']);
    }
    this.TotalAssignedFactories = total;

  }

  DeleteFactoryPer(facto) {
    for (var i = 0; i < this.ListOfDividesFactories.length; i++) {

      if (this.ListOfDividesFactories[i]['id'] == facto) {

        this.ListOfDividesFactories.splice(i, 1);
      }
    }
    this.getTotalPecentages();
    console.log(this.ListOfDividesFactories);

  }

  // -------------------- Expandable -------------------

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  getRowDetailHeight(row: any, index: number): number {
    const myTable = this;
    if (!row) {
      return myTable['rows'][index].height;
    }
    return row.height;
  }

  getInnerTableHeight(row: any) {
    return (this.headerHeight + (this.rowHeight * row.subOrdersFactoryPercent.length)).toString() + 'px';
  }

  //----------------------------------------------------------


  getLocations(contractId) {
    this.loading = true;
    this.customerService.getLocationByContract(contractId).subscribe(locations => {
      this.Locations = locations;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  CallNeededLookups() {
    console.log(this.id);
    this.loading = true

    this.lookupService.GetallWorkItems().subscribe(workOrderItems => {
      this.workOrderItems = workOrderItems;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetallPeriorities().subscribe(Periorities => {
      console.log(Periorities);
      this.Periorities = Periorities;
      this.loading = false;
      console.log('per');
      this.newOrder.fK_Priority_Id = Periorities.find(x => x.name.toLowerCase() === 'normal').id;
      console.log(this.newOrder.fK_Priority_Id);
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetallFactories().subscribe(Factories => {
      this.Factories = Factories;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetallGovernorates().subscribe(Governorates => {
      this.Governorates = Governorates;
      this.showG = false;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetallStatus().subscribe(allStatus => {
      this.allStatus = allStatus;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

  }

  GetContractMiXTypes(contractId) {
    return new Promise((resolve, reject) => {
      this.lookupService.GetContractMiXTypes(contractId).subscribe((contractMixTypes) => {
        this.contractMixTypes = contractMixTypes;
        this.contractMixTypes = this.contractMixTypes.filter(mix => mix.isDisabled == false);
        console.log(this.contractMixTypes);
        resolve();
      },
        err => {

        });
    });


  }
  GetContractMiXTypesNew(contractId) {
    this.contractMixTypes = [];
    console.log(this.id);
    this.ListOfContracts.forEach(x => {
      if (x.id == contractId)
        this.contractMixTypes = x.items;
    });
    this.contractMixTypes = this.contractMixTypes.filter(mix => mix.isDisabled == false);

  }

  GetContractById(contractId) {
    this.contractDetails = [];
    console.log(this.id);
    this.ListOfContracts.forEach(x => {
      if (x.id == contractId)
        this.contractDetails.push(x);
    });


    this.contractDetails[0].startDate = new Date(this.contractDetails[0].startDate);
    this.contractDetails[0].startDate.setHours(this.contractDetails[0].startDate.getHours() + this.customerService.getTimeOffsetTimeZone());

    if (this.contractDetails[0].endDate == null)
      this.contractDetails[0].endDate = null;
    else {
      this.contractDetails[0].endDate = new Date(this.contractDetails[0].endDate);
      this.contractDetails[0].endDate.setHours(this.contractDetails[0].endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
    }
  }

  getContractDetails(contractId) {
    this.reloadLoc(contractId);
    this.GetContractMiXTypesNew(contractId);
    this.GetContractById(contractId);
  }
  checkPriceValidaity(itemAmount) {


    itemAmount = parseInt(itemAmount);
    if (itemAmount <= 0) {
      this.alertService.error("Quantity must be greater than zero");
      return false;
    }
    if (this.customerData != undefined ) {

      if (this.customerData.creditLimit == 1) {
        return true;
      }
      else if (this.item != undefined) {
        let total = itemAmount * this.item.price;
        if (this.customerData.debit == 0 && (this.customerData.creditLimit - this.customerData.balance) >= total) {
          return true;

        }
        else if (this.customerData.debit > 0 && this.customerData.debit >= total) {
          return true;

        }
        else if (this.customerData.debit > 0 && this.customerData.debit < total) {
          var diff = total - this.customerData.debit;
          if ((this.customerData.creditLimit - this.customerData.balance) >= diff) {
            return true;

          }
          else {
            this.alertService.error("You have exceeds your credit limit");

            return false;

          }


        }
        else {
          this.alertService.error("You have exceeds your credit limit");

          return true;

        }
      }

    }

  }
  checkStartDateIfValid() {
    let contractMininmumDate = new Date(this.minimumDate);
    let orderStartDate = new Date((this.selectedWork[0].workOrder.startDate));
    let currentdate = new Date();
    if (orderStartDate.getTime() < currentdate.getTime()) {
      this.alertService.error("The start date must be greater than today");
      return false;
    }
    if (orderStartDate.getTime() < contractMininmumDate.getTime()) {
      this.alertService.error("The order start date must be greater than contract start date : " + this.utilitiesService.convertDatetoNormal(contractMininmumDate));
      return false;

    }
  }

  checkEndDateIfValid() {
    let contractMaxdate = new Date(new Date(this.maximumDate).toDateString());
    let OrderAltDate = new Date(new Date(this.selectedWork[0].workOrder.endDate).toDateString());
    let currentdate = new Date(new Date().toDateString());

    if (this.selectedWork[0].workOrder.endDate != undefined && OrderAltDate.getFullYear() != 1) {
      if (OrderAltDate.getTime() < currentdate.getTime()) {
        this.alertService.error("The AL date must be greater than today");
        return false;
      }
    }
    else {
      this.selectedWork[0].workOrder.endDate = null;
    }
    if (this.selectedWork[0].workOrder.endDate != null && contractMaxdate != undefined) {
      if (OrderAltDate.getTime() > contractMaxdate.getTime()) {
        this.alertService.error("The alt  date can't be exceed the contract end date : " + this.utilitiesService.convertDatetoNormal(contractMaxdate));
        return false;
      }
    }
  }



  SaveEdits() {
    this.loading = true;

    if (this.checkAmountValidation == true || this.checkItemValidation == true) {
      if (this.checkPriceValidaity(this.selectedWork[0].workOrder.amount) == false)
        return false;
    }
    if (this.checkAddDateValidaty() == false) {
      this.isDisabled = false;
      this.isOn = true;
      this.loading = false;

      return false;

    };
    this.EditedWorkOrder.workOrder = {};
    this.EditedWorkOrder.workOrder.id = this.selectedWork[0].workOrder.id;
    this.EditedWorkOrder.workOrder.code = this.selectedWork[0].workOrder.code;
    this.EditedWorkOrder.workOrder.amount = this.selectedWork[0].workOrder.amount;
    this.EditedWorkOrder.workOrder.description = this.selectedWork[0].workOrder.description;
    this.EditedWorkOrder.workOrder.fK_ElementToBePoured_Id = this.selectedWork[0].workOrder.fK_ElementToBePoured_Id;

    this.EditedWorkOrder.workOrder.fK_Location_Id = this.selectedWork[0].workOrder.fK_Location_Id;
    this.EditedWorkOrder.workOrder.fK_Customer_Id = this.selectedWork[0].workOrder.fK_Customer_Id;
    this.EditedWorkOrder.workOrder.fK_Contract_Id = this.selectedWork[0].workOrder.fK_Contract_Id;
    this.EditedWorkOrder.workOrder.fK_Priority_Id = this.selectedWork[0].workOrder.fK_Priority_Id;
    this.EditedWorkOrder.workOrder.fk_Status_Id = this.selectedWork[0].workOrder.fK_Status_Id;

    this.EditedWorkOrder.workOrder.pendingAmount = this.selectedWork[0].workOrder.pendingAmount;

    if (this.date) {
      let date = new Date(this.date);
      this.EditedWorkOrder.workOrder.startDate = date.toISOString();
    }
    else {
      let date = new Date(this.selectedWork[0].workOrder.startDate);
      this.EditedWorkOrder.workOrder.startDate = date.toISOString();
    }

    if (this.selectedWork[0].workOrder.endDate != undefined) {
      let endDate = new Date(this.selectedWork[0].workOrder.endDate);
      this.EditedWorkOrder.workOrder.endDate = endDate.toISOString();;
    }



    this.EditedWorkOrder.workOrder.fK_Item = this.selectedWork[0].workOrder.items.id;


    console.log('EditedWorkOrder');
    console.log(this.EditedWorkOrder);
    console.log(this.selectedWork[0]);
    delete this.EditedWorkOrder.priority;
    delete this.EditedWorkOrder.status;

    if (this.selectedWork[0].workOrder.fK_Status_Id == 4 || this.selectedWork[0].workOrder.fK_Status_Id == 10) {
      this.customerService.EditWorkOrderCancelled(this.selectedWork[0].workOrder.id, 4).subscribe(state => {
        console.log('EditedWorkOrder In Service ');
        console.log(state);
        this.alertService.success('order Updated Successfully');

        this.Resetall();
        this.loading = false;

      },
        err => {
          this.alertService.error('Server Error !!');
          this.loading = false;

        });
    }
    else {
      if (this.EditedWorkOrder.workOrder.amount != undefined) {
        if (!this.selectedWork[0].workOrder.fK_Location_Id) {
          this.alertService.error('you should select location first');
          this.loading = false;

        }
        else {

          this.customerService.EditWorkOrderServ(this.EditedWorkOrder).subscribe(state => {
            this.alertService.success('order Updated Successfully');
            this.Resetall();
            this.loading = false;
            console.log('EditedWorkOrder In Service ');
            this.GetNonExpiredContracts();
            console.log(state);
          

          },
            err => {
              this.alertService.error('Server Error !!');
              this.loading = false;

            });
        }
      }
    }
  }

  getAvailableAmount(itemId, itemPrice, show?: any) {

    //this.checkPriceValidaity(itemId, itemPrice);
    this.item = this.contractMixTypes.find(x => x.fk_Item_Id == itemId);

    console.log('in change');
    console.log(itemId);
    console.log(this.contractMixTypes);
    this.contractMixTypes.map((type) => {
      console.log(type.fk_Item_Id);
      console.log(itemId);
      if (type.fk_Item_Id == itemId) {
        console.log(type.availableAmount);
        this.availableAmount = type.availableAmount;
      }
    });
    if (show == "close")
      return;


    if (itemId == this.selectedItemId) {
      this.checkItemValidation = true;
      return false;
    }
    //if (this.newOrder.amount != undefined)
    //  this.checkPriceValidaity(this.newOrder.amount);
    //else if (this.selectedWork[0] != undefined) {
    //  if (this.selectedWork[0].workOrder.amount != undefined)
    //    this.checkPriceValidaity(this.selectedWork[0].workOrder.amount);
    //}

  }

  validateDate(date: any) {
    let selctedDate = new Date(date);
    let minimumDate = new Date();
    minimumDate.setDate(minimumDate.getDate() - 1);
    minimumDate = minimumDate;
    if (selctedDate.getTime() < minimumDate.getTime()) {
      this.alertService.error("The start date must be greater than today or equal");
    }
  }

  EditWorkOrder() {
    

    if (this.workOrderObjectToEdit.source == "Order Confirmation") {
      this.selectedWork.push({});
      this.selectedWork[0].workOrder = this.workOrderObjectToEdit;
      if (this.selectedWork[0].workOrder.items == undefined || this.selectedWork[0].workOrder.items == null) {
        this.selectedWork[0].workOrder.items = {};
        this.selectedWork[0].workOrder.items.id = 0;
      }
      this.GetNonExpiredContracts(this.workOrderObjectToEdit.customer.id);

    }


    else {
      this.selectedWork.push({});
      this.selectedWork[0].workOrder = this.workOrderObjectToEdit;
    }
    if (this.selectedWork[0].workOrder.fK_Status_Id == 2 || this.selectedWork[0].workOrder.fK_Status_Id == 4) {
      this.checkOrderPendingStatus(this.selectedWork[0]);

      this.GetContractMiXTypes(this.selectedWork[0].workOrder.fK_Contract_Id
      ).then(() => {
        console.log(this.selectedWork[0].workOrder.items.id);
        this.getAvailableAmount(this.selectedWork[0].workOrder.items.id, this.selectedWork[0].workOrder.items.price, "close");
      });

      this.reloadLocation(this.selectedWork[0].workOrder.fK_Contract_Id);
      this.selctedAmount = this.selectedWork[0].workOrder.amount;
      this.selectedItemId = this.selectedWork[0].workOrder.items.id;

      this.selectedWork[0].workOrder.oldAmount = this.selectedWork[0].workOrder.amount;
      this.selectedWork[0].workOrder.items = this.selectedWork[0].workOrder.items;
   
      this.selectedWork[0].workOrder.fK_ElementToBePoured_Id = this.selectedWork[0].workOrder.fK_ElementToBePoured_Id;

      this.selectedStartDate = new Date(this.selectedWork[0].workOrder.startDate);
      this.selectedWork[0].workOrder.startDate = new Date(this.selectedWork[0].workOrder.startDate);
      //this.selectedWork[0].workOrder.startDate.setHours(this.selectedWork[0].workOrder.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());

      if (new Date(this.selectedWork[0].workOrder.endDate) && new Date(this.selectedWork[0].workOrder.endDate).getFullYear() != 1 && new Date(this.selectedWork[0].workOrder.endDate).getFullYear() != 1970) {
        this.selectedWork[0].workOrder.endDate = new Date(this.selectedWork[0].workOrder.endDate);
        this.selectedEndDate = new Date(this.selectedWork[0].workOrder.endDate);
        //if (this.workOrderObjectToEdit.source != "Order Confirmation")
        //    this.selectedWork[0].workOrder.endDate.setHours(this.selectedWork[0].workOrder.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());

      }
      else {
        this.selectedEndDate = new Date(this.selectedWork[0].workOrder.endDate);;
        this.selectedWork[0].workOrder.endDate = null;
        this.selectedWork[0].workOrder.endDate = null;
      }

     


      this.customerService.GetContractById(this.selectedWork[0].workOrder.fK_Contract_Id).subscribe(contract => {

        this.contractCode = contract.id;


        this.minimumDate = new Date(contract.startDate);
        this.minimumDate.setHours(this.minimumDate.getHours() + this.customerService.getTimeOffsetTimeZone());

        if (contract.endDate != null && new Date(contract.endDate).getFullYear() != 1 && new Date(contract.endDate).getFullYear() != 1970) {
          this.maximumDate = new Date(contract.endDate);
          this.maximumDate.setHours(this.maximumDate.getHours() + this.customerService.getTimeOffsetTimeZone());

        }
        else {
          this.maximumDate = null;
        }
      });


      console.log(this.selectedWork[0]);
      console.log(this.selectedWork[0].workOrder.fK_Contract_Id);
      console.log(this.selectedWork[0].workOrder.fK_Location_Id);
      if (this.selectedWork[0].workOrder.items.length > 0) {
        // 1
        this.WorkItem = this.selectedWork[0].workOrder.items.id;
      }
      else {
        this.WorkItem = '';
      }


    }
    else {
      this.alertService.info('this order not editable')

      this.Resetall();
      return;
    }
    console.log(this.selectedState);
  }

  private date: any = '';

  

  

  checkAddDateValidaty(): any {
    let contractMaxdate: any;
    let contractMininmumDate = moment(this.minimumDate);
    let OrderAltDate = moment(this.selectedWork[0].workOrder.endDate);
    let orderStartDate = moment(this.selectedWork[0].workOrder.startDate);
    let currentdate = moment(new Date());



    if (this.selectedWork[0].workOrder.endDate != null && new Date(this.selectedWork[0].workOrder.endDate).getFullYear() != 1 && new Date(this.selectedWork[0].workOrder.endDate).getFullYear() != 1970) {

      if (OrderAltDate.isBefore(currentdate)) {
        this.alertService.error("The AL date must be greater than today");
        return false;
      }

      if (OrderAltDate.isBefore(contractMininmumDate, 'day')) {
        this.alertService.error("The order ALT date must be greater than contract start date : " + contractMininmumDate.format("YYYY-MM-DD"));
        return false;
      }
    }
    else {
      OrderAltDate = null;
    }

    if (orderStartDate.isBefore(currentdate)) {
      this.alertService.error("The start date must be greater than today");
      return false;
    }


    if (orderStartDate.isBefore(contractMininmumDate, 'day')) {
      this.alertService.error("The order start date must be greater than contract start date : " + contractMininmumDate.format("YYYY-MM-DD"));
      return false;
    }

    if (this.maximumDate != null) {

      contractMaxdate = moment(this.maximumDate);
      if (OrderAltDate !=null) {
        if (OrderAltDate.isAfter(contractMaxdate, 'day')) {
          this.alertService.error("The order alt date can't be exceed the contract end date: " + contractMaxdate.format("YYYY-MM-DD"))
          return false;

        }
      }
     

      if (orderStartDate.isAfter(contractMaxdate, 'day')) {
        this.alertService.error("The order start date can't be exceed the contract end date: " + contractMaxdate.format("YYYY-MM-DD"))
        return false;

      }


    }


  }




  // createNewLocation(statev) {
  //   console.log('statev');
  //   console.log(statev);
  //   this.AddedNewLocation.fk_Customer_Id = this.id;
  //   this.AddedNewLocation.title = this.newLocation.title;
  //
  //   this.CallGetStreat();
  //
  //
  //   if (this.AddedNewLocation.PACINumber != '') {
  //     this.AddedNewLocation.PACINumber = this.PACI;
  //   }
  //   if (!this.AddedNewLocation.street) {
  //     this.AddedNewLocation.street = '';
  //   }
  //
  //   console.log('AddedNewLocation');
  //   console.log(this.AddedNewLocation);
  //
  //   this.customerService.AddNewLocation(this.AddedNewLocation).subscribe(state => {
  //       console.log(state);
  //       this.ResetallLocation();
  //       this.AddNewLocation = false;
  //       this.AddNewLocation2 = false;
  //       this.reloadLocation();
  //       if (statev == 'editWorkOrder') {
  //         this.editWorkOrder = true;
  //       }
  //       else {
  //         this.addWorkOrder = true;
  //       }
  //       this.alertService.success('Location Added Successfully');
  //
  //     },
  //     err => {
  //       this.alertService.error('Server Error !!');
  //     });
  // }

  Resetall() {
    this.Reset.emit();



  }
  enableButton() {
    this.addWorkOrder = true;
    this.isOn = true;
    this.isDisabled = false;

  }
  ResetallLocation() {
    this.newLocation = {};
    this.newLocation.Government = {};
    this.AddedNewLocation = {};
    this.PACI = '';
    this.showG = false;
    this.showB = true;
    this.showA = true;
    this.showS = true;
    this.editWorkOrder = false;
    this.addWorkOrder = false;
    this.AddNewLocation = false;
    this.AddNewLocation2 = false;

  }

  reloadLocation(contractId) {
    console.log(this.id);
    this.customerService.getLocationByContract(contractId).subscribe(locations => {
      this.Locations = locations;
      console.log(JSON.stringify(this.Locations))
      console.log('%c we are here', 'color: red; font-weight: bold;');
      console.log(this.Locations);
      if (!this.Locations) {
        this.isDisableSaveAdd = false;
      }
      this.newOrder.fK_Location_Id = this.Locations[0].id;
    },
      err => {
        this.isDisableSaveAdd = true;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  reloadLoc(contractId) {
    this.Locations = [];
    console.log(this.id);
    this.ListOfLocations.forEach(x => {
      if (x.fk_Contract_Id == contractId)
        this.Locations.push(x)

    });
    //this.customerService.getLocationByContract(contractId).subscribe(locations => {
    //  this.Locations = locations;
    //  console.log(JSON.stringify(this.Locations))
    //  console.log('%c we are here', 'color: red; font-weight: bold;');
    //  console.log(this.Locations);
    //  if (!this.Locations) {
    //    this.isDisableSaveAdd = false;
    //  }
    //  this.newOrder.fK_Location_Id = this.Locations[0].id;
    //},
    //  err => {
    //    this.isDisableSaveAdd = true;
    //    this.alertService.error('your session has timed out please login again !');
    //  });
  }



  keyPressNegative(e: any) {
    if (!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58)
      || e.keyCode == 8)) {
      return false;
    }
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

}


