import { ActivatedRoute } from '@angular/router';
import { CustomerCrudService } from './../../../api-module/services/customer-crud/customer-crud.service';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { contract } from './../../../api-module/models/customer-model';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AuthenticationServicesService } from '../../../api-module/services/authentication/authentication-services.service';
import { ExistingCustomerService } from '../existing-customer.service';
import { UtilitiesService } from '../../../operations/utilities.service';
import { ExcelService } from '../../../report/excel-service.service';
import moment = require('moment');
import { Page } from '../../../shared-module/Page';


@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.css', './../existing-customer.component.css']
})

export class ContractComponent implements OnInit, OnDestroy {
  viewContract: boolean;
  public firstSubmitTry = false;
  public firstSubmitTryadd = false;
  page: Page = new Page();

  //--------------------  All Contracts Data Table Data ------------------

  contractColumns: any[] = [
    { name: 'id' },
    { name: 'fK_Customer_Id' },
    { name: 'contractNumber' },
    { name: 'startDate' },
    { name: 'endDate' },
    { name: 'amount' },
    { name: 'fK_ContractType_Id' },
    { name: 'pendingAmount' },
    { name: 'remarks' },
    { name: 'price' },
    { name: 'contractTypeName' }
  ];

  private saveEditLocation: any = {
    'id': 0,
    'selectedLang': '',
    'paciNumber': '',
    'title': '',
    'governorate': '',
    'area': '',
    'block': '',
    'street': '',
    'addressNote': '',
    'latitude': 0,
    'longitude': 0,
    'fk_Contract_Id': 0
  };

  selectedContract = [];
  public contractLocations: any = [];
  public ContractType: Subscription;
  public isDisableSaveAdd: boolean = false;
  public EditLocation: boolean = false;
  public newLocation: any = {};
  public editedLocation: any = {};
  public AddNewLocation: boolean = false;
  public PACI = '';
  public showG: boolean = true;
  public showB: boolean = true;
  public showA: boolean = true;
  public showS: boolean = true;
  public editedContract: any = {};

  public editMode = false;
  AddedNewLocation: any = {};
  editLocation = false;
  currentLocation: any = {};
  public Governorates: any;
  public areas: any[];
  public blocks: any[];
  public streets: any[];
  /** location zone **/

  paciEdit: any;

  public editContract: boolean = false;
  public AddContract: boolean = false;
  public contractAddMixType: boolean = false;
  public contractEditMixType: boolean = false;

  public ContractInfo: Array<contract>;
  public isDirty: boolean = false;
  public newContract: any = {};
  public contractObj: any = {};
  listOfMixTypesEdit: any[] = [];
  listOfMixTypesAdd: any[] = [];
  readyMixTypes: any[];
  private id: number;
  private sub: any;
  areaId: any;

  public isHideFromLoggedUser: boolean = false;
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
  mixTypeData: any;
  priceData: any;
  contractId: any;
  status: any;
  showInfo: boolean;
  typesNotDisabledAndNotUsed: any;
  readyMixTypeId: any;
  allPaciData: any = {};
  streetData: any;
  selctedItem: any;
  itemIndex: number = 0;
  typesNotUsed: any[];
  allReadyMix: any;
  isBlockExist: boolean = false;
  selectedLang: string;
  addLoc: boolean = false;
    confirmDelete: boolean;

  constructor(private excelService: ExcelService,
    private route: ActivatedRoute,
    private lookupService: LookupService,
    private alertService: AlertServiceService,
    private customerService: CustomerCrudService,
    private auth: AuthenticationServicesService,
    private existingCustomerService: ExistingCustomerService,
    private utilities: UtilitiesService) {
    this.page.pageNumber = 1;
    this.page.pageSize = 5;
  }

  //@Output() Reset: EventEmitter<any> = new EventEmitter<any>();

  @Input() customerCredit: number;
  @Input() customerBalance: number;
  //@Output() updateContractInfo = new EventEmitter();

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.id = params['CurrentCustomer'];
    });

    this.GetContractDetails({ offset: 0 });
    if (window.localStorage.getItem('lang')) {
      console.log(window.localStorage.getItem('lang'));
      this.selectedLang = window.localStorage.getItem('lang');
      // this.utilities.chooseLang(this.selectedLang);
    } else {
      this.selectedLang = this.utilities.languages[1].symbol;
    }
    this.loading = true;
    this.currentLocation = {};
    this.callGovs();

    this.CallNeededLookups();
    this.getReadyMix();


    this.isHideFromLoggedUser = this.auth.CurrentUser().roles.includes('CallCenter') ||
      this.auth.CurrentUser().roles.includes('superCallCenter');
  }


  DeleteItem() {
    this.confirmDelete = true;
  }
  cancelDelete() {
    this.confirmDelete = false;
  }
  ConfirmDelete() {
    this.customerService.DeleteContract(this.selectedContract[0].id).subscribe(state => {
      if (state) {
        this.ResetCustInfo();
        this.confirmDelete = false;

        this.alertService.success(' Deleted Successfully');
      }
      else {
        this.confirmDelete = false;

        this.alertService.error('This contract already used');
      }
    },
      err => {
        this.confirmDelete = false;

        this.alertService.error('we have Error !!');
      });
  }

  exportCsv() {
    let exportData = [];
    this.ContractInfo.map((item) => {
      exportData.push({
        'Contract Code': item.id,
        'Customer Name': item.customer.name,
        'Contract No': item.contractNumber,

        'Start Date': moment(item.startDate).format("YYYY-MM-DD"),
        'Expiry Date': moment(item.endDate).format("YYYY-MM-DD "),
        'Contract Type': item.contractType.name,
        'Remark': item.remarks
      })
    });
    this.excelService.exportAsExcelFile(exportData, 'Customer Contract Report');
  }



  callGovs() {

    this.loading = true;

    this.lookupService.GetallGovernorates().subscribe(Governorates => {
      this.Governorates = Governorates;
      if (this.allPaciData != undefined && this.allPaciData.governorate != undefined && this.allPaciData.governorate.id > 0)
        this.loading = false;
      this.loading = false;
      if (Object.keys(this.allPaciData).length > 0)
        this.CallArea();

      this.showG = false;
    },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });
  }
  /*Ragab new logic*/

  /*Sorry If there is Stupid code I was just following the recipe of the initial developer, Sorry Again!*/

  toggleContractAddMixType() {
    this.toggleAddContractModal();
    this.contractAddMixType = !this.contractAddMixType;
  }

  checkNegative(num) {
    if (num < 0) {
      this.alertService.error("you can't enter negative numbers.");
    }
  }

  toggleContractEditMixType() {
    for (var i = 0; i < this.selectedContract[0].items.length; i++) {
      if (this.selectedContract[0].items[i].disabled = true) {
        this.showInfo = true;
        break;
      }
    }
    if (this.selectedContract[0].items.length == 0) {
      this.showInfo = false;
    }
    this.toggleEditContractModal();
    this.contractEditMixType = !this.contractEditMixType;
  }

  toggleAddContractModal() {
    this.isFromAdd = true;
    this.AddContract = !this.AddContract;
  }

  public isFromAdd: boolean = false;

  returnUsedItems() {
    let typesId = this.readyMixTypes.map(
      item => item = item.id
    );
    this.typesNotUsed = this.readyMixTypes;
    for (let i = 0; i < this.selectedContract[0].items.length; i++) {
      let item = this.selectedContract[0].items[i];
      if (typesId.includes(item.fk_Item_Id) == true && item.isDisabled == false) {
        let index = typesId.indexOf(item.fk_Item_Id);
        //this.typesNotUsed.splice(index, 1);
      }
    }

  }

  AddToListOfMixEdit() {
    if (this.selectedContract[0].items.length < this.readyMixTypes.length) {
      this.selectedContract[0].items.push({ fk_Item_Id: null, price: null, disabled: false });

    }
    this.listOfMixTypesEdit = this.selectedContract[0].items.slice();
  }
  CloseAddReadyMixType() {
    this.contractAddMixType = false;
    this.AddContract = true;
    $('#addReady').hide();
  }

  CancelAddReadyMixType() {
    this.contractAddMixType = false;
    this.AddContract = true;
    $('#editReady').hide();
  }

  CloseEditReadyMixType() {
    this.contractAddMixType = false;
    this.editContract = true;
    $('#editReady').hide();
  }

  AddToListOfMixAdd() {
    if (this.listOfMixTypesAdd.length < this.readyMixTypes.length) {
      this.listOfMixTypesAdd.push({ price: null });
    }
  }

  removeReadyMixTypeEdit(readyMixType) {
    this.customerService.IsOrderHaveContractItem(this.contractId, readyMixType.fk_Item_Id).subscribe(state => {

      if (state == true) {
        this.alertService.error("The item already used with an order , you can't delete it");
        return false;
      }
      else if (state == false) {
        let removedFlag: boolean;
        this.listOfMixTypesEdit.map((mixType) => {
          if (JSON.stringify(mixType) == JSON.stringify(readyMixType) && !removedFlag) {
            this.listOfMixTypesEdit.splice(this.listOfMixTypesEdit.indexOf(readyMixType), 1);
            removedFlag = true;
          }
        });

      }
    },
      err => {
        this.alertService.error('server Error !!');
      });

  }

  disableReadyMixType(selectedItem) {
    this.selectedContract[0].items.map(
      item => {
        if (item.id == selectedItem.id) {
          item.isDisabled = true;
        }
      }
    );
    this.lookupService.EditContractToDisableItems(this.selectedContract[0])
      .subscribe(
        res => {
          console.log(res);
        }, err => {
          console.log(err);
        }
      )
  }

  removeReadyMixTypeAdd(readyMixType) {
    let removedFlag: boolean;
    this.listOfMixTypesAdd.map((mixType) => {
      if (JSON.stringify(mixType) == JSON.stringify(readyMixType) && !removedFlag) {
        this.listOfMixTypesAdd.splice(this.listOfMixTypesAdd.indexOf(readyMixType), 1);
        removedFlag = true;
      }
    });
  }

  checkSelectedEdit(readyMixTypeId) {
    if (this.selectedContract[0].items.length) {
      let foundFlag = false;
      for (let i = 0, l = this.selectedContract[0].items.length; i < l; i++) {
        if (this.selectedContract[0].items[i].fk_Item_Id == readyMixTypeId) {
          foundFlag = true;
        }
      }
      return foundFlag;
    } else {
      return false;
    }
  }

  checkSelectedAdd(readyMixTypeId) {
    if (this.listOfMixTypesAdd.length) {
      let foundFlag = false;
      for (let i = 0, l = this.listOfMixTypesAdd.length; i < l; i++) {
        if (this.listOfMixTypesAdd[i].id == readyMixTypeId) {
          foundFlag = true;
        }
      }
      return foundFlag;
    } else {
      return false;
    }
  }

  chooseSelectedType(type) {
    if (type.id != undefined) {
      this.mixTypeData = this.readyMixTypes.find(x => x.id == type.id);
      if (this.mixTypeData != undefined) {
        let itemPriceIndex = this.listOfMixTypesAdd.findIndex(x => x.id == this.mixTypeData.id);
        this.listOfMixTypesAdd[itemPriceIndex].price = this.mixTypeData.price;
      }
    }
    else if (type.fk_Item_Id != undefined) {
      this.mixTypeData = this.readyMixTypes.find(x => x.id == type.fk_Item_Id);
      if (this.mixTypeData != undefined) {
        let itemPriceIndex = this.selectedContract[0].items.findIndex(x => x.fk_Item_Id == this.mixTypeData.id);
        this.selectedContract[0].items[itemPriceIndex].price = this.mixTypeData.price;
      }

    }
    this.readyMixTypes.map((readyMixType) => {
      if (readyMixType.id == type.fK_Item_Id) {
        type.name = readyMixType.name;
      }
    });
  }

  checkAmountValidity(list) {
    return !list.filter((item) => {
      return item.amount < 0.001;
    }).length;
  }


  // ***************************************

  /*api calls*/
  getReadyMix() {
    this.loading = true;
    this.lookupService.GetallWorkItems().subscribe(
      (readyMixTypes) => {
        this.readyMixTypes = readyMixTypes;
        this.readyMixTypes.map(
          item => {
            item.price = null;
            item.label = item.code + ' : ' + item.description;
            item.value = item.id;
          }
        );
        this.readyMixTypeId = this.readyMixTypes[0].id;
        this.loading = false;
      },
      err => {
        this.loading = false;
      });
  }

  showEditContractForm() {
    this.contractId = this.selectedContract[0].id;
    this.selectedContract[0].startDate = new Date(this.selectedContract[0].startDate);
    this.selectedContract[0].startDate.setHours(this.selectedContract[0].startDate.getHours() + this.customerService.getTimeOffsetTimeZone());

    if (this.selectedContract[0].endDate == null)
      this.selectedContract[0].endDate = null;
    else {
      this.selectedContract[0].endDate = new Date(this.selectedContract[0].endDate);
      this.selectedContract[0].endDate.setHours(this.selectedContract[0].endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
    }


    this.editContract = true;
    this.listOfMixTypesEdit = this.selectedContract[0].items;
    for (var i = 0; i < this.selectedContract[0].items.length; i++) {
      this.selectedContract[0].items[i].disabled = true;
    }
    this.editedContract = this.selectedContract[0];
    this.getContractLocation(this.selectedContract[0].id);
  }

  viewContractDetails() {
    this.viewContract = true;
    this.listOfMixTypesEdit = this.selectedContract[0].items;
    this.selectedContract[0].startDate = new Date(this.selectedContract[0].startDate);
    this.selectedContract[0].startDate.setHours(this.selectedContract[0].startDate.getHours() + this.customerService.getTimeOffsetTimeZone());

    if (this.selectedContract[0].endDate == null)
      this.selectedContract[0].endDate = null;
    else {
      this.selectedContract[0].endDate = new Date(this.selectedContract[0].endDate);
      this.selectedContract[0].endDate.setHours(this.selectedContract[0].endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
    }
    this.viewContract = this.selectedContract[0];
    this.getContractLocation(this.selectedContract[0].id);
  }

  ResetCusttInfo() {
    this.GetContractDetails({ offset: 0 });
    this.contractLocations = [];
    this.viewContract = false;
    this.AddContract = false;
    this.isDisableSaveAdd = false;
    this.selectedContract = [];
    this.newContract = {};
    this.firstSubmitTryadd = false;
    this.firstSubmitTry = false;
    this.listOfMixTypesAdd = [];
    this.editContract = false;
    $('.confirm-popup-fade').hide();
    // $('#editContact').remove();
  }
  toggleEditContractModal() {
    this.editContract = !this.editContract;
    if (this.editContract == false) {
      this.selectedContract[0].items = this.listOfMixTypesEdit.slice();

    }
    this.isFromAdd = false;
    if (this.editedContract) {
      this.getContractLocation(this.editedContract.id);
    }
  }
  getContractLocation(contractId) {
    this.contractLocations = [];
    this.customerService.getLocationByContract(contractId).subscribe(locations => {
      locations.map(loc => {
        this.contractLocations.push(loc); //= locations;
      });
    },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });
  }

  showAddNewLocationModal() {
    this.newLocation = {};
    this.AddedNewLocation = {};
    this.toggleEditContractModal();
    this.AddNewLocation = true;
  }

  showEditLocationModal(location) {
    this.toggleEditContractModal();
    this.EditLocation = true;
    this.editedLocation = location;
    if (location.paciNumber) {
      this.paciEdit = location.paciNumber;
    }
    else {
      this.paciEdit = '';
    }

    this.Governorates.map((gov) => {
      if (location.selectedLang == "en") {
        if (gov.name == this.editedLocation.governorate) {

          this.editedLocation.governorate = gov.id;
          this.editedLocation.governorateName = gov.name;
        }

      }
      else {
        if (gov.arabicName == this.editedLocation.governorate) {

          this.editedLocation.governorate = gov.id;
          this.editedLocation.governorateName = gov.arabicName;
        }
      }
    });
    this.GetAreasForEditLocation(this.editedLocation.governorate, location.selectedLang);

  }

  GetAreasForEditLocation(govId, lang = "en") {
    let langEn = lang;
    this.loading = true;
    this.lookupService.GetallAreas(govId).subscribe(areas => {
      if (areas != null) {
        this.areas = areas;
        this.areas.map((area) => {
          if (langEn == "en") {
            if (area.name == this.editedLocation.area) {
              this.editedLocation.area = area.name;
              this.areaId = area.id;
              this.editedLocation.areaName = area.name;
              this.GetBlocksForEditLocation(area.id);
              this.loading = false;
            }
          }
          else {
            if (area.arabicName == this.editedLocation.area) {
              this.editedLocation.area = area.arabicName;
              this.areaId = area.id;
              this.editedLocation.areaName = area.arabicName;
              this.GetBlocksForEditLocation(area.id);
              this.loading = false;
            }
          }
        });
        this.showA = false;
        this.loading = false;
      }
      else {
        this.loading = false;
        this.GetAreasForEditLocation(this.editedLocation.governorate, langEn);
      }
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  GetBlocksForEditLocation(areaId) {
    this.areas.map((area) => {
      if (area.id == areaId) {
        this.editedLocation.areaName = area.name;
      }
    });
    if (this.isBlockExist) {
      this.lookupService.GetallBlocks(areaId).subscribe(blocks => {
        if (blocks != null) {
          this.blocks = blocks;
          this.showB = false;
          this.blocks.map((block) => {
            if (block.name == this.editedLocation.block) {
              this.editedLocation.block = block.name;
              this.editedLocation.blockName = block.name;
              this.GetStreetsForEditLocation(areaId, this.editedLocation.block, this.editedLocation.governorate);
              this.loading = false;
            }
          });
        } else {
          this.loading = false;
          this.GetBlocksForEditLocation(this.editedLocation.area);
        }
      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        });
    }
  }

  GetStreetsForEditLocation(areaId, blockName, govId) {
    this.loading = true;
    this.lookupService.GetallStreets(areaId, blockName, govId).subscribe(streets => {
      if (streets != null) {
        this.streets = streets;
        this.showS = false;
        this.streets.map((street) => {
          if (street.name == this.editedLocation.street) {
            this.editedLocation.street = street.id;
            this.editedLocation.streetName = street.name;
          }
          this.loading = false;
        });
      } else {
        this.loading = false;
        this.GetStreetsForEditLocation(this.editedLocation.area, this.editedLocation.block, this.editedLocation.governorate);
      }
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  showAddLocationWithContractModal(loc) {
    this.newLocation = {};
    this.AddedNewLocation = {};
    this.toggleAddContractModal();
    this.AddNewLocation = true;
    this.editedLocation = loc;
  }

  calculateAmountEdit() {
    this.selectedContract[0].amount = 0;
    this.listOfMixTypesEdit.map((type) => {
      this.selectedContract[0].amount += Number(type.amount);
    });
  }

  calculateAmountAdd() {
    this.newContract.amount = 0;
    this.listOfMixTypesAdd.map((type) => {
      this.newContract.amount += Number(type.amount);
    });
  }

  removeAmountAdd() {
    let amounts = 0;
    this.listOfMixTypesAdd.map((type) => {
      amounts += type.amount;
    });
    if (amounts != this.newContract.amount) {

    } else {
      if (this.newContract.amount != 0) {
        this.newContract.amount = 0;
        this.listOfMixTypesAdd.map((type) => {
          this.newContract.amount -= Number(type.amount);
        });
      }
    }

  }

  /******************************************/

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  ResetCustInfo() {
    this.GetContractDetails({ offset: 0 });
    this.contractLocations = [];
    this.editContract = false;
    this.AddContract = false;
    this.isDisableSaveAdd = false;
    this.selectedContract = [];
    this.newContract = {};
    this.firstSubmitTryadd = false;
    this.firstSubmitTry = false;
    this.listOfMixTypesAdd = [];
    this.contractEditMixType = false;
    $('.confirm-popup-fade').hide();
  }

  onSubmit(form) {
    if (form.valid) {
      let date = new Date(this.selectedContract[0].startDate);
      this.selectedContract[0].startDate = new Date(date.toISOString());

      if (this.selectedContract[0].endDate != undefined) {
        let endDate = new Date(this.selectedContract[0].endDate);
        this.selectedContract[0].endDate = new Date(endDate.toISOString());
      }
      this.Editcontract();
      // }
    } else {
      this.firstSubmitTry = true;
    }
  }





  onAddContract(form) {
    if (form.valid) {

      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddContract = false;
      }
      else {
        this.Addcontract();
      }
    } else {
      this.firstSubmitTryadd = true;
    }
  }


  //----------------- Contract Functions -----------------

  Editcontract() {


    if (this.validateContractdate(this.selectedContract[0].startDate, this.selectedContract[0].endDate)) {
      this.loading = false;
      return;
    }


    for (var i = 0; i < this.selectedContract[0].items.length; i++) {
      this.selectedContract[0].items[i].id = 0;
    }
    this.selectedContract[0].items.map((contract) => {
      delete contract.name;
      delete contract.availableAmount;
      // delete contract.id;
    });
    if (this.listOfMixTypesEdit.slice().length == 0 && this.selectedContract[0].items.length == 0) {
      this.alertService.error('you must add quantity first')
    }
    else {
      if (this.contractLocations.length > 0) {
        this.customerService.EditContract(this.selectedContract[0]).subscribe(state => {
          if (state) {
            this.ResetCustInfo();
            this.alertService.success('user Updated Successfully');
            this.GetContractDetails({ offset: 0 });
          }
          else {
            this.alertService.error('failed To update');
          }
        },
          err => {
            this.alertService.error('we have Error !!');
          });
      }
      else {
        this.alertService.error('you must add location first');
      }
    }
  }


  GetContractDetails(pageNumber: any) {
    this.loading = true;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let pagingData = Object.assign({}, this.page);
    pagingData.pageNumber = this.page.pageNumber + 1;
    pagingData.id = this.id;
    this.customerService.GetAllContractsByCustomerIdAsync(pagingData).subscribe(contratcs => {
      this.ContractInfo = contratcs.result;
      this.page.totalElements = contratcs.totalCount;

      if (this.ContractInfo != null) {
        this.ContractInfo.map((contract) => {
          contract.startDate = new Date(contract.startDate);
          contract.startDate.setHours(contract.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          if (contract.endDate != null) {
            contract.endDate = new Date(contract.endDate);
            contract.endDate.setHours(contract.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());

          }

        });

      }
      this.loading = false;

    },
      err => {
        this.alertService.error('your session has timed out please login again !')
      });
  }







  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.GetContractDetails(this.page.pageNumber);

  }




  // create contract
  validateContractdate(startDate, endData) {
    let sDate = moment(startDate);
    let eDate = moment(endData);
    if (eDate.isBefore(sDate)) {
      this.alertService.error("The contract start date can't be exceed the contract end date ")
      return true;
    }
    else {
      return false;

    }

  }


  Addcontract() {

    this.loading = true;
    if (this.listOfMixTypesAdd.slice().length == 0) {
      this.loading = false;
      this.alertService.error('you must add quantity first')
      return;
    }

 

    this.newContract.startDate = new Date(new Date(this.newContract.startDate).toISOString());
    if (this.newContract.endDate) {
      this.newContract.endDate = new Date(new Date(this.newContract.endDate).toISOString());
    }
    else {
      this.newContract.endDate = null;
    }

    if (this.validateContractdate(this.newContract.startDate, this.newContract.endDate)) {
      this.loading = false;
      return;
    }


    this.newContract.fK_Customer_Id = this.id + '';
  
    if (this.contractLocations.length == 0) {
      this.loading = false;
      this.alertService.error('you must add location first');
      return;
    }
    this.customerService.chechContractNum(this.newContract.contractNumber).subscribe(result => {

      if (result == false) {
        this.newContract.items = this.listOfMixTypesAdd.slice();

        this.newContract.items.map((item) => {
          delete item.name;
          delete item.availableAmount;
          item.fK_Item_Id = item.id;
          delete item.id;
        });
    
        this.contractObj = {
          contract: this.newContract,
          locations: this.contractLocations
        };
        this.customerService.AddContract(this.contractObj).subscribe(state => {
          if (state) {
            this.AddedNewLocation.fK_Contract_Id = state.id;
            this.ResetCustInfo();
            this.alertService.success('contract added successfully');
            this.loading = false;
          }
          else {
            this.loading = false;
            this.alertService.error('failed To update');
          }
        },
          err => {
            this.loading = false;
            this.alertService.error('we have Error !!');
          });
      }
      else {
        this.loading = false;
        this.alertService.error('The contract number already exist ');
        return;
      }
    }, error => {
      this.loading = false;
    })

  }


  getToday() {
    let today = new Date();
    let tomorrow = new Date();
    tomorrow.setDate(today.getDate());
    return tomorrow.toISOString().split('T')[0];
  }

  CallNeededLookups() {
    this.loading = true;
    this.lookupService.GetAllContractTypes().subscribe(contractTypes => {
      this.ContractType = contractTypes;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

  }


  /******** location dialog *********/

  checktoSearch(event) {
    if (this.PACI.length > 7) {
      this.GetLocationByPaci();
    }
    else {
      this.newLocation = {};
      // this.AddedNewLocation={};
    }
  }

  checkEditToSearch() {
    if (this.paciEdit.length > 7) {
      this.GetEditLocationByPaci();
    }
    else {
      this.editedLocation = {};
    }
  }


  GetEditLocationByPaci() {

    this.lookupService.GetLocationByPaci(this.paciEdit).subscribe(loc => {
      if (loc != null && this.isBlockExist) {
        this.editedLocation.governorate = loc.governorate.id;
        this.showG = false;
        this.GetAreasForEditLocation(loc.governorate.id);
        this.editedLocation.area = loc.area.id;
        this.GetBlocksForEditLocation(loc.area.id);
        this.editedLocation.block = loc.block.name;
        this.GetStreetsForEditLocation(loc.area.id, loc.block.name, loc.governorate.id);
        this.editedLocation.street = loc.street.id;
      }
      else {
        this.editedLocation.governorate = loc.governorate.id;
        this.showG = false;
        this.GetAreasForEditLocation(loc.governorate.id);
        this.editedLocation.area = loc.area.id;

        this.editedLocation.block = this.editedLocation.block;
        this.editedLocation.street = this.editedLocation.street;
      }

    },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });
  }

  GetLocationByPaci() {
    this.loading = true;
    this.lookupService.GetLocationByPaci(+this.PACI).subscribe(loc => {
      if (loc != null) {
        this.allPaciData = loc;
        this.newLocation.Government = loc.governorate.id;
        this.callGovs();

      }
      else {
        this.loading = false;
        this.showG = false;
        this.showA = true;
        this.showB = true;
        this.showS = true;
        this.newLocation.area = '';
        this.newLocation.block = '';
        this.newLocation.street = '';
        this.alertService.error('there is no location with this PACI number !');
      }
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  CallArea() {
    this.loading = true;
    this.AddedNewLocation.governorate = this.getSelectedNameGov(this.Governorates, this.newLocation.Government);

    if (Object.keys(this.newLocation).length > 0 && !Number.isInteger(+this.newLocation.Government)) {
      this.lookupService.GetallAreas(this.newLocation.Government.id).subscribe(areas => {
        this.areas = areas;
        if (areas != null) {
          this.showA = false;
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('there are not areas for this Governorate !');
        }
      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        });
    }

    else if (this.allPaciData != undefined && Object.keys(this.allPaciData).length > 0) {
      this.lookupService.GetallAreas(this.allPaciData.governorate.id).subscribe(areas => {
        this.areas = areas;
        if (areas != null) {
          this.newLocation.area = this.allPaciData.area.id;
          this.showA = false;
          // this.CallBlock();

        }
        else {
          this.loading = false;
          this.alertService.error('there are not areas for this Governorate !');
        }
      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        });
    }

    else if (this.newLocation.Government > 0) {
      this.lookupService.GetallAreas(this.newLocation.Government).subscribe(areas => {
        this.areas = areas;
        if (areas != null) {
          this.showA = false;
          this.loading = false;

        }
        else {
          this.loading = false;
          this.alertService.error('there are not areas for this Governorate !');
        }
      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        });
    }
  }

  CallBlock() {
    this.loading = true;


    if (this.isBlockExist == false) {
      this.AddedNewLocation.area = this.getSelectedNameGov(this.areas, this.newLocation.area);
      this.loading = false;

    }
    else {
      this.AddedNewLocation.area = this.getSelectedNameGov(this.areas, this.newLocation.area);

      if (Object.keys(this.newLocation).length > 0 && !Number.isInteger(+this.newLocation.area)) {
        this.lookupService.GetallBlocks(this.newLocation.area.id).subscribe(blocks => {
          this.blocks = blocks;
          if (blocks != null) {
            this.newLocation.block = this.allPaciData.block.name;
            this.showB = false;
            this.isBlockExist = true;
            this.CallStreet();
          }
          this.loading = false;
        },
          err => {
            this.loading = false;
            this.alertService.error('your session has timed out please login again !');
          });
      }

      else if (Object.keys(this.allPaciData).length > 0) {
        this.lookupService.GetallBlocks(this.allPaciData.area.id).subscribe(blocks => {
          this.blocks = blocks;
          if (blocks != null) {
            this.newLocation.block = this.allPaciData.block.id;
            this.showB = false;
            this.CallStreet();
          }
        },
          err => {
            this.loading = false;
            this.alertService.error('your session has timed out please login again !');
          });
      }

      else if (this.newLocation.area > 0) {
        this.lookupService.GetallBlocks(this.newLocation.area).subscribe(blocks => {
          this.blocks = blocks;
          if (blocks != null) {
            this.showB = false;
          }
          this.loading = false;

        },
          err => {
            this.loading = false;
            this.alertService.error('your session has timed out please login again !');
          });
      }
    }
  }

  CallStreet() {
    this.loading = true;
    if (this.blocks != undefined)
      this.streetData = this.blocks.find(x => x.id == this.newLocation.block);

    this.AddedNewLocation.block = this.getSelectedName(this.blocks, this.streetData.name);

    if (Object.keys(this.newLocation).length > 0 && !Number.isInteger(+this.newLocation.block)) {

      this.lookupService.GetallStreets(this.newLocation.area.id, this.newLocation.block.name, this.newLocation.Government.id).subscribe(streets => {
        this.streets = streets;
        if (streets != null) {
          if (this.allPaciData != undefined) {
            this.newLocation.street = this.allPaciData.street.id;
            this.CallAddGetStreat();

          }
          this.showS = false;
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('there are not streets for this block !');
        }

      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        });
    }

    else if (Object.keys(this.allPaciData).length > 0) {

      this.lookupService.GetallStreets(this.allPaciData.area.id, this.allPaciData.block.name, this.allPaciData.governorate.id).subscribe(streets => {
        this.streets = streets;
        if (streets != null) {
          if (this.allPaciData != undefined) {
            this.newLocation.area = this.allPaciData.area.id;
            this.newLocation.block = this.allPaciData.block.id;
            this.newLocation.street = this.allPaciData.street.id;
            this.CallAddGetStreat();

          }
          this.showS = false;
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('there are not streets for this block !');
        }

      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        });
    }

    else if (this.newLocation.area > 0, this.newLocation.Government > 0) {
      let street = this.blocks.find(x => x.id == this.newLocation.block);
      this.lookupService.GetallStreets(this.newLocation.area, street.name, this.newLocation.Government).subscribe(streets => {
        this.streets = streets;
        if (streets != null) {

          this.showS = false;
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('there are not streets for this block !');
        }

      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        });
    }


  }

  CallGetStreat() {
    this.editedLocation.streetName = this.getSelectedName(this.streets, this.newLocation.street);
  }
  CallAddGetStreat() {

    this.AddedNewLocation.street = this.getSelectedName(this.streets, this.newLocation.street.id);
  }

  editLocationForContract() {
    if (this.selectedLang == "en") {
      this.editedLocation.selectedLang ="en"
    }
    else {
      this.editedLocation.selectedLang ="ar"

    }
    this.loading = true;
    this.editedLocation.paciNumber = this.paciEdit;
    this.Governorates.map((gov) => {
      if (gov.id == this.editedLocation.governorate) {
        if (this.selectedLang == "en") {
          this.editedLocation.governorate = gov.name;

        }
        else {
          this.editedLocation.governorate = gov.arabicName;

        }
      }
    });
    this.areas.map((area) => {
      if (area.id == this.areaId) {
        if (this.selectedLang == "en") {
          this.editedLocation.area = area.name;
          this.areaId = area.id;
          this.editedLocation.areaName = area.name;

        }
        else {
          this.editedLocation.area = area.arabicName;
          this.areaId = area.id;
          this.editedLocation.areaName = area.arabicName;
        }
      }
    });
    if (this.isBlockExist) {
      this.blocks.map((block) => {
        if (block.name == this.editedLocation.block) {
          this.editedLocation.block = block.name;
          this.editedLocation.blockName = block.name;
        }
      });
      this.streets.map((street) => {
        if (street.id == this.editedLocation.street) {
          this.editedLocation.street = street.name;
          this.editedLocation.streetName = street.name;
        }
      });
    }
    this.saveEditLocation = this.editedLocation;

    this.saveEditLocation.addressNote = this.editedLocation.addressNote;
    this.saveEditLocation.paciNumber = this.paciEdit;
    this.saveEditLocation.selectedLang = this.editedLocation.selectedLang;

    this.saveEditLocation.area = this.editedLocation.areaName;
    this.saveEditLocation.block = this.editedLocation.block;
    this.saveEditLocation.street = this.editedLocation.street;
    this.saveEditLocation.longitude = this.editedLocation.longitude;
    this.saveEditLocation.latitude = this.editedLocation.latitude;
    this.saveEditLocation.fk_Contract_Id = this.editedLocation.fk_Contract_Id;
    this.saveEditLocation.id = this.editedLocation.id;
    this.saveEditLocation.title = this.editedLocation.title;
    this.customerService.UpdateLocation(this.saveEditLocation).subscribe(state => {
      if (state) {
        this.loading = false;
        // this.ResetCustInfo();
        this.alertService.success('Location Updated Successfully');
      }
      else {
        this.loading = false;
        this.alertService.error('failed To update');
      }
      this.ResetallLocation();
    },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!');
      });
  }

  createNewLocation() {
    this.loading = true;
    if (this.selectedLang == "en") {
      this.AddedNewLocation.selectedLang = "en"
    }
    else {
      this.AddedNewLocation.selectedLang = "ar"

    }
    this.AddedNewLocation.paciNumber = this.PACI;
    this.AddedNewLocation.title = this.newLocation.title;
    this.AddedNewLocation.addressNote = this.newLocation.addressNote;

    if (this.isBlockExist == false)


      if (this.isBlockExist) {
        if (this.newLocation.block != undefined)
          this.AddedNewLocation.block = this.blocks.find(x => x.id == this.newLocation.block).name;
        if (this.newLocation.street != undefined)
          this.AddedNewLocation.street = this.streets.find(x => x.id == this.newLocation.street).name;
      }
      else {
        this.AddedNewLocation.block = this.newLocation.block;
        this.AddedNewLocation.street = this.newLocation.street;
      }

    if (!this.isFromAdd) {
      this.AddedNewLocation.fK_Contract_Id = this.editedContract.id;
      this.customerService.AddNewLocation(this.AddedNewLocation)
        .subscribe(() => {
          this.selectedContract[0] = this.editedContract;
          this.toggleEditContractModal();
          this.AddNewLocation = false;
          this.addLoc = true;
          this.loading = false;
        },
          err => {
            this.loading = false;
          });
    } else {
      this.loading = false;
      this.toggleAddContractModal();
      this.contractLocations.push(this.AddedNewLocation);
      this.AddNewLocation = false;
    }

    //this.existingCustomerService.updateContractInfo.next(true);
  }

  getSelectedNameGov(list, id) {
    if (list != null) {
      for (var i = 0; i < list.length; i++) {
        if (list[i].id == id) {
          if (this.selectedLang == "en") {
            return list[i].name;

          }
          else {

            return list[i].arabicName;

          }
        }
      }
    }
  }


  getSelectedName(list, id) {
    if (list != null) {
      for (var i = 0; i < list.length; i++) {
        if (list[i].id == id) {
          return list[i].name;

        }
      }
    }
  }

  ResetallLocation() {
    this.newLocation = {};
    this.allPaciData = "";
    this.newLocation.Government = {};
    this.AddedNewLocation = {};
    this.PACI = '';
    this.showG = false;
    this.showB = true;
    this.showA = true;
    this.showS = true;
    this.AddNewLocation = false;
    this.EditLocation = false;
    this.editedLocation = {};
    this.saveEditLocation = {
      'id': 0,
      'paciNumber': '',
      'title': '',
      'governorate': '',
      'area': '',
      'block': '',
      'street': '',
      'addressNote': '',
      'latitude': 0,
      'longitude': 0,
      'fk_Contract_Id': 0
    };
    if (this.isFromAdd) {
      this.toggleAddContractModal();
    } else {
      this.toggleEditContractModal();
    }
  }

  keyPressNegative(e: any) {
    if (!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58)
      || e.keyCode == 8)) {
      return false;
    }
  }

  chechContactNum(contNum) {
    this.customerService.chechContractNum(contNum).subscribe(result => {
      if (result == true) {
        this.alertService.error('this contract number alreadt exsit, please change it')
      }
    }, error => {
      console.log('error')
    })
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
  showRepeated(data) {
    let allItems: any[] = JSON.parse(JSON.stringify(this.selectedContract[0].items));
    allItems.reverse();
    allItems.splice(0, 1);
    let selectedItem = allItems.findIndex(item => (item.fk_Item_Id == data.fk_Item_Id && (item.isDisabled == false || item.disabled == false)));
    if (selectedItem >= 0) {
      this.alertService.error("item already exist");
      this.selectedContract[0].items.pop();
    }

  }
  getReadyMixTypes(mixType) {
    if (this.allReadyMix == undefined)
      this.allReadyMix = JSON.parse(JSON.stringify(this.readyMixTypes));

    if (mixType.disabled == true) {
      this.allReadyMix.map(
        item => {
          item.price = null;
          item.label = item.code + ' : ' + item.description;
          item.value = item.id;
        }
      );
      return this.allReadyMix;
    } else {
      let typesId = this.readyMixTypes.map(
        item => item = item.id
      );
      this.typesNotDisabledAndNotUsed = this.readyMixTypes;

      this.typesNotDisabledAndNotUsed.map(
        item => {
          item.label = item.code + ' : ' + item.description;
          item.value = item.id;
        }
      );
      console.log(this.typesNotDisabledAndNotUsed);
      return this.typesNotDisabledAndNotUsed;
    }
  }

}
