import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { AlertServiceService } from '../../api-module/services/alertservice/alert-service.service';
import { AuthenticationServicesService } from '../../api-module/services/authentication/authentication-services.service';
import { CustomerCrudService } from '../../api-module/services/customer-crud/customer-crud.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { equipmentInFactory } from '../../api-module/models/newCustomer';
import { isNullOrUndefined } from '@swimlane/ngx-datatable/release/utils';
import { UtilitiesService } from '../../operations/utilities.service';


@Component({
  selector: 'app-order-confirmation-modal',
  templateUrl: './order-confirmation-modal.component.html',
  styleUrls: ['./order-confirmation-modal.component.css']
})

export class OrderConfirmationModalComponent implements OnInit {
  @Input() orderConfirmation: any = {};
  @Input() currentOrder: any = {};
  @Output() isModalClosed = new EventEmitter();
  @Output() pendingOrders = new EventEmitter();
  confirmationStatus: any[];
  defaultDate: any;
  public AddEquipment: boolean = false;

  isMovement: boolean;
  isCallCenter: boolean;
  isNeedReason: boolean;
  isResecdual: boolean = false;
  public orderCount: number = 0;
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
  public Factories: Subscription;
  public assignedFactory = '';
  disableFirstDate: boolean;
  disableAltDate: boolean;
  public AllVehicle;
  public AllEqui: Array<equipmentInFactory>;
  public itemEqui: Array<equipmentInFactory>;
  public equipmentAll;
  selectedDate: string;
  showSelectedDate: boolean;
    showAllConfirmationData: boolean;
    selectedLang: string;

  constructor(private router: Router, private lookupService: LookupService, private alertService: AlertServiceService,
    private auth: AuthenticationServicesService, private customerService: CustomerCrudService, private utilities: UtilitiesService) {

    if (window.localStorage.getItem('lang')) {
      console.log(window.localStorage.getItem('lang'));
      this.selectedLang = window.localStorage.getItem('lang');
    } else {
      this.selectedLang = this.utilities.languages[1].symbol;
    }
  }

  ngOnInit() {
    this.loading = true;
    this.showSelectedDate = false;
    if (this.orderConfirmation.selectedDate == 1) {
      this.selectedDate = "First Date confirmed by movement controller"
      this.showSelectedDate = true;
    }
    else if (this.orderConfirmation.selectedDate == 2) {
      this.selectedDate = "AlT Date confirmed by movement controller"
      this.showSelectedDate = true
    }
    this.defaultDate = new Date();
    if (this.orderConfirmation.orderAlternativeDate != null && new Date(this.orderConfirmation.orderAlternativeDate).getFullYear() != 1)
      this.orderConfirmation.orderAlternativeDateView = new Date(this.orderConfirmation.orderAlternativeDate);
    else
      this.orderConfirmation.orderAlternativeDateView = null;

    this.orderConfirmation.orderDate = new Date(this.orderConfirmation.orderDate);

    console.log(this.orderConfirmation.orderAlternativeDate);
    this.isMovement = this.auth.CurrentUser().roles.includes('MovementController');
    this.isCallCenter = this.auth.CurrentUser().roles[0].includes('CallCenter');


    this.getAllStatus();
    this.getAllFactories();




  }
  getAllStatus() {

    this.lookupService.getConfirmationStatus()
      .subscribe((confirmationStatus) => {
        this.confirmationStatus = confirmationStatus;
        this.orderConfirmation.fk_ConfirmationStatus_Id = this.orderConfirmation.fk_ConfirmationStatus_Id;

        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }


  getAllFactories() {
    this.loading = true;

    this.lookupService.GetallFactories().subscribe(Factories => {
      this.Factories = Factories;
      this.orderConfirmation.factoryId = this.orderConfirmation.factoryId;

    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

  }

  addEquipment() {
    this.loading = true;
    let factoryId = this.orderConfirmation.factoryId;
    console.log(factoryId);
    this.customerService.getEquipmentByOrder(this.orderConfirmation.id).subscribe((equipments) => {
      console.log(equipments);
      this.itemEqui = equipments;
      this.loading = false;
    },
      err => {
        this.loading = false;
      });

    this.customerService.GetAvailableEquipmentInFactory(factoryId).subscribe(equipment => {
      this.equipmentAll = equipment;
      this.AllEqui = equipment;
      this.AddEquipment = true;

      console.log(equipment);
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      }, () => {
  

      });
    this.currentOrder = this.orderConfirmation;
    // this.body.classList.add('noOverflow');

  }

  altDateChanged(date) {
    this.orderConfirmation.orderAlternativeDateView = new Date(date);
    this.orderConfirmation.orderAlternativeDate = new Date(date);

    const currentDate = new Date(date);
    this.customerService.GetOrderCountPerDate(currentDate.toISOString()).subscribe(count => {
      this.orderCount = count;
    });
    // this.orderConfirmation.orderAlternativeDate = this.orderConfirmation.orderAlternativeDate.split('.')[0];
  }
  //   isFirstDate(){
  // this.disableFirstDate=false;
  // this.disableAltDate=false;

  //   }
  //     isFirstDate(){
  // this.disableFirstDate=false;
  // this.disableAltDate=false;

  //   }
  ReassigntoNewFactory() {
    this.loading = true;
    if (this.assignedFactory != '') {
      console.log(this.orderConfirmation.fk_WorkOrder_Id)
      this.customerService.ReassinFactory(this.orderConfirmation.fk_WorkOrder_Id, this.assignedFactory).subscribe(status => {
        console.log('Can Assign : ' + status);
        if (status == true) {
          this.customerService.UpdateOrderAssignmentToFactory(this.orderConfirmation.fk_WorkOrder_Id, this.assignedFactory).subscribe(status => {
            this.loading = false;
            this.alertService.success('Updated Successfully');
            // this.Resetall();
          },
            err => {
              this.loading = false;
              this.alertService.error('your session has timed out please login again !');
            });
        }
      },
        err => {
          this.loading = false;
          this.alertService.error('invalid to assign to the factory');
          // this.Resetall();

        });
    }
    else {
      this.loading = false;
      this.alertService.error('No factory selected');
      // this.Resetall();
    }
  }

  isFirstDate(): boolean {
    return this.orderConfirmation && this.orderConfirmation.selectedDate && this.orderConfirmation.selectedDate == 1;
  }

  isAltDate(): boolean {
    return this.orderConfirmation && this.orderConfirmation.selectedDate && this.orderConfirmation.selectedDate == 2;
  }
  isDecline(): boolean {
    return this.orderConfirmation && this.orderConfirmation.isDeclined && this.orderConfirmation.isDeclined == 1;
  }

  isNotDecline(): boolean {
    return this.orderConfirmation && this.orderConfirmation.isDeclined && this.orderConfirmation.isDeclined == 2;
  }

  handleChange(event) {
    if (event.checked) {
      this.showAllConfirmationData = true;
    }
    else {
      this.showAllConfirmationData = false;

    }


  }
  saveConfirmationDate() {
    debugger;
    if (this.orderConfirmation.fk_ConfirmationStatus_Id == 0) {
      this.alertService.error('Confirmation status is required  !');
      return false;

    }

    if (this.orderConfirmation.factoryId == null) {
      this.alertService.error('Please assign the order to factory !');
      return false;

    }
    if (!this.showAllConfirmationData) {
      if (this.orderConfirmation.selectedDate == undefined || this.orderConfirmation.selectedDate == 0 || this.orderConfirmation.selectedDate == null) {
        this.alertService.error('Confirmation type is required  !');
        return false;
      }

      if (this.orderConfirmation.selectedDate == undefined && this.isMovement) {
        this.alertService.error('Confirmation type is required  !');
        return false;
      }
      this.loading = true;

      if (this.orderConfirmation.orderAlternativeDateView != null) {
        this.orderConfirmation.orderAlternativeDateView = new Date(this.orderConfirmation.orderAlternativeDateView);
        this.orderConfirmation.orderAlternativeDate = this.orderConfirmation.orderAlternativeDateView.toISOString();

      }

    }
    else {
      if (isNullOrUndefined(this.orderConfirmation.reason) || this.orderConfirmation.reason=="") {
        this.alertService.error('Please enter the reason for the cancellation  !');
        return;
      }
      this.orderConfirmation.confirmationStatus = null;
      this.orderConfirmation.workOrder = null;


    }
    this.lookupService.postConfirmation(this.orderConfirmation)
      .subscribe((res) => {

          if (res != null) {
            this.alertService.success('Added Successfully');
            this.loading = false
            this.closeModal();


          } else {
            this.loading = false;
            this.alertService.error('No factory selected');
            // this.Resetall();
          }



      },
        err => {
          this.loading = false;
          this.closeModal();

          this.alertService.error('Server Error, Please try Again later !');
        });
  }

  closeModal1(): void {
    this.AddEquipment = false;
  }

  checkStatusValue(orderConfirmation) {
    orderConfirmation.orderAlternativeDate = orderConfirmation.orderAlternativeDateView;

    let statusName = this.confirmationStatus.filter((state) => {
      return state.id == this.orderConfirmation.fk_ConfirmationStatus_Id;
    })[0].name;
    if (statusName == 'Confirmed By Movement Controller') {
      orderConfirmation.orderAlternativeDate = null;
      this.isResecdual = false;
    } else if (statusName == 'Need Movement Controller Confirmation') {
      orderConfirmation.orderAlternativeDate = null;
      this.isResecdual = false;
    } else if (statusName == 'Need To Reschedule By Customer') {
      orderConfirmation.orderAlternativeDate = null;
      this.isResecdual = true;
    } else if (statusName == 'Confirmed By Customer') {
      orderConfirmation.orderAlternativeDate = null;
      this.isResecdual = false;
    } else if (statusName === 'Need To Change Date By Movement Controller') {
      this.isResecdual = true;
    }
    return orderConfirmation;
  }

  closeModal() {
    if (this.isCallCenter) {
      this.isModalClosed.emit('isCallCenter');
    } else {
      this.isModalClosed.emit('all');
    }
  }

}
