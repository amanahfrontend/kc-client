import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AuthGuardGuard } from './../api-module/guards/auth-guard.guard';
import { SharedModuleModule } from './../shared-module/shared-module.module';
import { DropdownModule, CalendarModule } from 'primeng/primeng';

import { AddNewCustomerComponent } from './add-new-customer/add-new-customer.component';
import { CustomerSearchPageComponent } from './customer-search-page/customer-search-page.component';
import { ExistingCustomerComponent } from './existing-customer/existing-customer.component';
import { CustomerinfoComponent } from './existing-customer/customerinfo/customerinfo.component';
import { ContractComponent } from './existing-customer/contract/contract.component';
import { WordOrderComponent } from './existing-customer/word-order/word-order.component';
import { ComplainsComponent } from './existing-customer/complains/complains.component';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { OrderConfirmationModalComponent } from './order-confirmation-modal/order-confirmation-modal.component';
import { CustomerSearchComponent } from './customer-search/customer-search.component';
import { WordOrderEditComponent } from './existing-customer/word-order-edit/word-order-edit.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerSearchPageComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'SuperCallCenter', 'Sales', 'Admin'] },

    children: [
      { path: 'customer/:CurrentCustomer', component: ExistingCustomerComponent },
    ]
  },
  {
    path: 'new',
    component: AddNewCustomerComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'SuperCallCenter', 'Sales', 'Admin'] }
  }
  ,
  {
    path: 'orderConfirmation',
    component: OrderConfirmationComponent,
    // canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'SuperCallCenter', 'Admin'] }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CalendarModule,
    SharedModuleModule,
    RouterModule.forChild(routes),
    DropdownModule
  ],
  declarations: [
    AddNewCustomerComponent,
    CustomerSearchPageComponent,
    ExistingCustomerComponent,
    CustomerinfoComponent,
    ContractComponent,
    WordOrderComponent,
    ComplainsComponent,
    OrderConfirmationComponent,
    CustomerSearchComponent,
    WordOrderEditComponent
  ]
})

export class CustomerModule {
}
