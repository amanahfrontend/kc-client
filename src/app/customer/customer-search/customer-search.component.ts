import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {AlertServiceService} from '../../api-module/services/alertservice/alert-service.service';
import {Router} from '@angular/router';
import {CustomerCrudService} from '../../api-module/services/customer-crud/customer-crud.service';
import {UtilitiesService} from '../../operations/utilities.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Page } from '../../shared-module/Page';
import { ExcelService } from '../../report/excel-service.service';

@Component({
  selector: 'app-customer-search',
  templateUrl: './customer-search.component.html',
  styleUrls: ['./customer-search.component.css']
})

export class CustomerSearchComponent implements OnInit, OnChanges, OnDestroy {
  public showLatestCustomer: boolean;
    AllCustomersSupscription: any;
  @Input() set data(data) {
    this.showLatestCustomer = data;
  }
  showloading = false;

  page: Page = new Page();
  public allCustomers: any[] = [];
  public UserExistance = '';
  public SearchInput = '';
  public allSearchCustomers: any[] = [];
  selectedCustomer = [];

  loading = false;
  currentUser: any = {};
  isSales: boolean = false;
  public latestCustomers: any[] = [];
 // selectedCustomer: any;
  public loadingImage: string = "data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="
  showRemoveButton: boolean = false;
  constructor(private router: Router,private excelService: ExcelService, 
    private customerService: CustomerCrudService,
    private alertService: AlertServiceService,
    private utilities: UtilitiesService) {
    this.page.pageNumber = 1;
    this.page.pageSize = 5;
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser.roles[0].toLowerCase() == 'admin') {
      this.showRemoveButton = true;
    }
    if (this.currentUser.roles[0].toLowerCase() == 'sales' || this.currentUser.roles[0].toLowerCase() == 'admin') {
      this.isSales = true;
    }
    this.utilities.customerSearch && (this.SearchInput = this.utilities.customerSearch);
    let currentUrl = this.router.url;
    if (currentUrl.indexOf('customer') <= 0 && currentUrl.indexOf('orderConfirmation') <= 0) {
      this.showLatestCustomer = true;
      this.getAllCustomers({ offset: 0 });
    }
  }



  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.getAllCustomers(this.page.pageNumber);

  }

  exportCsv() {
    let i = 0;
    let allCustomerData = [];
    this.customerService.GetAllCustomers().
      subscribe(allCus => {
        allCustomerData = allCus;
        let exportData = [];
        allCustomerData.map((item) => {
          i += 1;
          exportData.push({
            "Ser": i ,
            'Customer Name': item.name,
            'Customer Phone': this.getAllPhones(item.phones),
            'KCRM ID': item.kcrM_Customer_Id,
            'Customer Category': item.customerCategory.name,
            'Customer Type': item.customerType.name,
            'Remark': item.remarks
          })
        });
        this.excelService.exportAsExcelFile(exportData, 'Schedule Report');


      },
        err => {

          this.alertService.error('There is and issue in the importing  !!');
        });



  }
  getAllPhones(customer_Phone: any[]) {
    let allPhones: string = "";
    if (customer_Phone.length > 0) {
      for (var i = 0; i < customer_Phone.length; i++) {
        allPhones = allPhones + customer_Phone[i];
        if (customer_Phone.length > 1) {
          allPhones += "/";
        }

      }
      if (allPhones.length <3) {
        allPhones = "";
      }
      return allPhones;

    }

  }

  //ngOnDestroy() {
  //  // this.showLatestCustomer = this.utilities.showLatestCustomer = true;
  //  // this.getLatestCustomers();
  //}

  ngOnChanges() {
    // debugger;
    // this.utilities.customerSearch && (this.SearchInput = this.utilities.customerSearch);
    // this.showLatestCustomer = true;
    // this.getLatestCustomers();
  }

  deleteCustomer() {
    this.customerService.DeleteCustomer(this.selectedCustomer[0].id).
      subscribe(state => {
        this.getAllCustomers({ offset: 0 });
        this.alertService.success(this.selectedCustomer[0].name + ' Deleted Successfully');
      },
      err => {

          this.alertService.error('This Item is already Used !!');
        });
  }

  SearchCustonmer() {

    this.loading = true;

    this.customerService.GoSearchCustomer(this.SearchInput).subscribe(users => {
        this.loading = false;
        console.log(users);

        if (users.length > 0) {
          this.UserExistance = 'true';
          this.allSearchCustomers = users;
        }
        else {
          this.allSearchCustomers = [];
          this.UserExistance = 'false';
        }

      },
      err => {
        this.loading = false;
        this.allSearchCustomers = [];
        this.UserExistance = '';
        this.alertService.error('your session has timed out please login again !');
      });
  }

  AddNewCustomer() {
    this.router.navigate(['search/new']);
  }

  checktoSearch(event) {
    if (this.SearchInput.length > 3) {
      this.SearchCustonmer();
    }
    else {
      this.allSearchCustomers = [];
    }
  }

  MoreDetail(id) {
    this.showLatestCustomer = false;
    this.utilities.customerSearch = this.SearchInput;
    this.allSearchCustomers = [];
    this.router.navigate(['search/customer/' + id]);

  }


  // getLatestCustomers() {
  //   this.customerService.GetLatestCustomers().subscribe(customers => {
  //     this.latestCustomers = customers;
  //     this.latestCustomers.map((customer) => {
  //       if (customer.phones && customer.phones.length > 0) {
  //         customer.phone = customer.phones[0];
  //       }
  //     });
  //     console.log('customers');
  //     console.log(customers);
  //   });
  // }

  getAllCustomers(pageNumber: any) {
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let pagingData = Object.assign({}, this.page);
    pagingData.pageNumber = this.page.pageNumber + 1;

   this.customerService.getAllCustomersByPaging(pagingData).subscribe((customers) => {

        this.allCustomers = customers.item1;
        this.page.totalElements = customers.item2;

  
        this.allCustomers.map(
          customer => {
            if(customer.phones && customer.phones.length > 0) {
              customer.phone = customer.phones[0];
            }
          }
        );
      }
    );
  }

  viewCustomerDetails(row) {
    this.showLatestCustomer = this.utilities.showLatestCustomer = false;
    this.router.navigate(['search/customer/' + row.id]);
  }
  ngOnDestroy() {
    // this.utilities.setSavedNotificationText('');
    this.AllCustomersSupscription && this.AllCustomersSupscription.unsubscribe();
  }
}
