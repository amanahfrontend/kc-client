import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { CustomerCrudService } from '../../api-module/services/customer-crud/customer-crud.service';
import {  DatatableComponent } from '@swimlane/ngx-datatable';
import { LookupService } from './../../api-module/services/lookup-services/lookup.service';
import { AlertServiceService } from './../../api-module/services/alertservice/alert-service.service';
import { DateFormatPipe } from '../../pipes/filter.pipe';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Page } from '../../shared-module/Page';
import { ExcelService } from '../../report/excel-service.service';
import * as moment from 'moment';
import { UtilitiesService } from '../../operations/utilities.service';


@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.css']
})

export class OrderConfirmationComponent implements OnInit {

  public allItemsToPoured: any[] = [];
  page: Page = new Page();

  confirmations: any[]=[];
  orderConfirmationModal: boolean;
  orderConfirmation: any;
  showAddConfirmation: boolean;
  NotificationAllData: any[] = [];
  UserAllRoles;
  allApprovedOrders: any[] = [];
  @ViewChild(DatatableComponent) myTable: DatatableComponent;
  isHistory: boolean = false;
  public showLatestCustomer: boolean;
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
  query: string = '';
  query1: string = '';
  public Factories: any[];
  filteredList: any = [];
  public allStatus: any[];
  public search = {
    'selectedStatus': '',
    'searchField': '',
    'selectedPriority': '',
    'startDateF': '',
    'startDateS': '',
    'startDate': '',
    'endDate': '',
    'customerName': '',
    'locationTitle': '',
    'kcrmId': '',
    'phoneCus':''
  };
  public pendingOrders = [];
  Periorities: any = [];
  @Output() filterdArrayByDate: any = new Array();
  approvedOrders: any;
  editWorkOrder: boolean = false;
    workOrderObjectToEdit: any;
    contractCode: any;
    showWorkOrder: boolean;
    currentOrder: any;
    selectedDate: string;
    showSelectedDate: boolean;
    isFilter: boolean;
    AllApprovedOrders: any;
    selectedLang: string;

  constructor(private customerService: CustomerCrudService,private utilities: UtilitiesService, private lookupService: LookupService, private Lookup: LookupService,private excelService: ExcelService,
    private alertService: AlertServiceService, private dateFormatPipe: DateFormatPipe) {
    this.page.pageNumber = 1;
    this.page.pageSize = 5;
    if (window.localStorage.getItem('lang')) {
      console.log(window.localStorage.getItem('lang'));
      this.selectedLang = window.localStorage.getItem('lang');
    } else {
      this.selectedLang = this.utilities.languages[1].symbol;
    }


  }

  ngOnInit() {
    this.GetAllItemPoured();
    this.showLatestCustomer = false;
    this.getNeededConfirmations({ offset: 0 });
    this.UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
    this.GetAllLookUp();

  }

  GetAllLookUp() {

    this.lookupService.GetallStatuses().subscribe(allStatus => {
      this.allStatus = allStatus;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
    this.lookupService.GetallPeriorities().subscribe(Periorities => {
      this.Periorities = Periorities;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetallFactories().subscribe(Factories => {
      this.Factories = Factories;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }


  getFilteredDate(startDate: string, endDate: string): void {
    debugger;
    this.pendingOrders = this.dateFormatPipe.transform(this.pendingOrders, startDate, endDate);
    console.log(this.pendingOrders)
  }




  exportCsv() {
    this.loading = true;
    this.Lookup.GetAllApprovedOrders().
      subscribe(AllApprovedOrders => {
        this.allApprovedOrders = AllApprovedOrders;

        let exportData = [];
        this.allApprovedOrders.forEach(order => {
          if (new Date(order.startDate).getFullYear() != 1) {
            order.startDate = new Date(order.startDate);
            order.startDate.setHours(order.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
            order.startDate = moment(order.startDate).format("YYYY-MM-DD HH:mm:ss");
          }

          if (new Date(order.endDate).getFullYear() == 1 || new Date(order.endDate).getFullYear() == 1970) {
            order.endDate = '';
          }
          else {
            order.endDate = new Date(order.endDate);
            order.endDate.setHours(order.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
            order.endDate = moment(order.endDate).format("YYYY-MM-DD HH:mm:ss");
          }

        });

        this.allApprovedOrders.map((item) => {
          let customerPhone = "";
          if (item.customer.phones.length > 1) {
            customerPhone = item.customer.phones[0].phone != null ? item.customer.phones[0].phone : "";
            customerPhone += "/";
            customerPhone += item.customer.phones[1].phone != null ? item.customer.phones[1].phone : "";
          }
          else {
            customerPhone = item.customer.phones[0].phone;
          }

          exportData.push({
            'Customer Name': item.customer.name,
            'Customer Phone': customerPhone,
            'KCRM ID': item.customer.kcrM_Customer_Id,
            'Order Code': item.code,
            'Quantity': item.amount,
            'First Date  ': item.startDate,
            'Alt Date': item.endDate
          })
        });

        this.excelService.exportAsExcelFile(exportData, 'Approved Orders');
        this.loading = false;
      },
        err => {
          this.loading = false;
          this.alertService.error('error')
        });
  }

  GetAllItemPoured() {
    this.loading = true;
    this.Lookup.GetAllItemPoured().
      subscribe(ItemPoured => {


        this.allItemsToPoured = ItemPoured;
        this.loading = false;
      },
        err => {
          this.loading = false;
          this.alertService.error('error')
        });
  }

  getNeededConfirmations(pageNumber: any) {
    this.isFilter = false;
    this.loading = true;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let pagingData = Object.assign({}, this.page);
    pagingData.pageNumber = this.page.pageNumber + 1;

    this.customerService.getPendingApprovedOrdersAsync(pagingData)
      .subscribe((orders) => {
        this.page.totalElements = orders.totalCount;

        orders.model.map((order) => {
          if (order.customer.phones) {
            order.customer.phone = order.customer.phones[0];
          }
          if (order.confirmationHistories && order.confirmationHistories.length > 0) {
            order.confirmationStatus = order.confirmationHistories[0].confirmationStatus.name;
          }

        });
        this.pendingOrders = orders.model;
        this.pendingOrders.forEach(order => {
          if (new Date(order.startDate).getFullYear() != 1) {
            order.startDate = new Date(order.startDate);
            order.startDate.setHours(order.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          }

          if (new Date(order.endDate).getFullYear() == 1) {
            order.endDate = null;
          }
          else {
            order.endDate = new Date(order.endDate);
            order.endDate.setHours(order.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          }

        });
        this.findAllpprovedOrders(this.pendingOrders);
        console.log(this.pendingOrders);
        this.loading = false;
      },
        err => {
          this.loading = false;
          console.log(err);
        });
  }



  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    if (this.isFilter == true) {
      this.filterAll(this.page.pageNumber);
    }
    else {
      this.getNeededConfirmations(this.page.pageNumber);

    }

  }

  ShowOrderDetails(Order) {
    if (Order.selectedDate == 1) {
      this.selectedDate = "First Date confirmed by movement controller"
      this.showSelectedDate = true;
    }
    else if (Order.selectedDate == 2) {
      this.selectedDate = "AlT Date confirmed by movement controller"
      this.showSelectedDate = true
    }
    if (Order.contract == undefined)
      Order.contract = {};
    this.currentOrder = Order;
    console.log(this.currentOrder);
    this.currentOrder.parentWorkOrder = this.currentOrder.parentWorkOrder || {};
    this.showWorkOrder = true;

    //this.body.classList.add('noOverflow');
  }

  findAllpprovedOrders(orders) {
    this.allApprovedOrders = JSON.parse(JSON.stringify(orders));
    this.allApprovedOrders = this.allApprovedOrders.filter(order => order.status.name == "Approved");
  }
  openEditModel(order) {
    this.contractCode = order.contract.id;
    if (order.fK_Status_Id == 2 || order.fK_Status_Id == 4) {

      this.workOrderObjectToEdit = order;
    

      this.workOrderObjectToEdit.source = "Order Confirmation";

      this.editWorkOrder = true;
    }
      if (order.fK_Status_Id == 2 || order.fK_Status_Id == 4 || order.isDeclined==1 ) {

          this.workOrderObjectToEdit = order;


          this.workOrderObjectToEdit.source = "Order Confirmation";

          this.editWorkOrder = true;
      }
    else {
      this.alertService.info('this order not editable')
    }

  }

  viewConfirmationDetails(order, bool) {
   
    if (order.status.name == "Pending" && bool == false) {
      this.alertService.info("The order still pending the approval from the movement");
      return false;

    }
      if (order.isDeclined==1)
      {
          if (order.reason != null && order.reason != undefined && order.reason.length > 0) {
            this.alertService.info('this order is  declined by movement beasuse ' + order.reason);
            return false;

          }
          else {
            this.alertService.info('this order is  declined by movement');
            return false;

          }      
    }
   else {
    console.log(order);
    this.orderConfirmation = {
      orderDate: order.startDate,
      orderAlternativeDate: order.endDate,
      orderNumber: order.code,
      fk_WorkOrder_Id: order.id,
        factoryId: order.fK_Factory_Id,
        fk_ConfirmationStatus_Id: order.fK_ConfirmationStatus_Id,
        isDeclined: order.isDeclined,
        selectedDate: order.selectedDate,
       reason : order.reason,
      description: order.description
    };
    this.isHistory = bool;
    this.showAddConfirmation = bool;
    if (bool === false) {
      this.toggleOrderConfirmationModal(true);
    } else {
      this.toggleOrderConfirmationModal(false);
    }
    this.customerService.getPendingApprovedOrder(order.id)
      .subscribe((confirmations) => {


        this.confirmations = confirmations;
        if (this.confirmations.length > 0) {

          this.confirmations.forEach(order => {
            if (new Date(order.orderDate).getFullYear() != 1) {
              order.orderDate = new Date(order.orderDate);
              order.orderDate.setHours(order.orderDate.getHours() + this.customerService.getTimeOffsetTimeZone());
            }

            if (new Date(order.orderAlternativeDate).getFullYear() == 1 || new Date(order.orderAlternativeDate).getFullYear() == 1970) {
              order.endDate = null;
            }
            else {
              order.orderAlternativeDate = new Date(order.orderAlternativeDate);
              order.orderAlternativeDate.setHours(order.orderAlternativeDate.getHours() + this.customerService.getTimeOffsetTimeZone());
            }

          });


        }
        console.log(confirmations);
      },
        err => {
          console.log(err);
        });
   }
  }

  toggleOrderConfirmationModal(isAdd) {
    if (isAdd === true) {
      this.toggleAddConfirmation(false);
    } else {
      this.showAddConfirmation = !this.showAddConfirmation;
    }
    this.orderConfirmationModal = !this.orderConfirmationModal;
    // this.getNeededConfirmations();

  }

  toggleAddConfirmation(isCloseAll) {
    if (isCloseAll) {
      this.toggleOrderConfirmationModal(false);
      if (this.isFilter==true) {
        this.filterAll({ offset: 0 });

      }
      else {
        this.getNeededConfirmations({ offset: 0 });
      }
    } else if (isCloseAll == 'isCallCenter') {
      this.toggleOrderConfirmationModal(false);
    
    }

    this.showAddConfirmation = !this.showAddConfirmation;
  }

  loadPendingOrders(mesg) {
    console.log(mesg);
    //this.getNeededConfirmations();
  }


  ResetAllSearch() {
    this.isFilter = false;
    this.search = {
      'selectedStatus': '',
      'searchField': '',
      'selectedPriority': '',
      'startDateF': '',
      'startDateS': '',
      'startDate':'',
      'endDate': '',
      'customerName': '',
      'locationTitle': '',
      'kcrmId': '',
      'phoneCus': ''


    };
    this.pendingOrders = [];
    this.getNeededConfirmations({ offset: 0 })
  }


  Resetall() {
    this.editWorkOrder = false;
    this.showWorkOrder = false;
    //this.getNeededConfirmations({ offset: 0 });
  }
  filterAll(pageNumber) {
    this.loading = true;
    this.pendingOrders = [];
    this.isFilter = true;
    let filter = [];

    if (this.search.startDate == undefined) {
      this.search.startDateS = "";
    }
    if (this.search.startDateF !== '' && this.search.startDateF != undefined && this.search.startDateS == "") {
      let startDateF = this.customerService.convertDateForSaving(this.search.startDateF);
      let startDateFirst = this.customerService.convertDateForSaving(new Date());

      filter.push(
        {
          'Type': 'StartDate',
          'Value': [startDateF + 'T00:00:00.00', startDateFirst + 'T23:59:59.59' ,true]
        }
      );
    }
    if (this.search.startDateF !== '' && this.search.startDateF != undefined && this.search.startDateS != undefined && this.search.startDateS != '') {
      let startDateF = this.customerService.convertDateForSaving(this.search.startDateF);
      let startDateS = this.customerService.convertDateForSaving(this.search.startDateS);

      filter.push(
        {
          'Type': 'StartDate',
          'Value': [startDateF + 'T00:00:00.00', startDateS + 'T23:59:59.59']
        }
      );
    }

    if (this.search.endDate !== '' && this.search.endDate != undefined) {
      let endDate = this.customerService.convertDateForSaving(this.search.endDate);
      filter.push(
        {
          'Type': 'EndDate',
          'Value': [endDate + 'T00:00:00.00', endDate+ 'T23:59:59.59']
        }
      );
    }

    if (this.search.searchField !== '' && this.search.searchField != undefined) {
      filter.push(
        {
          'Type': 'Code',
          'Value': this.search.searchField
        }
      );
    }


    if (this.search.phoneCus !== '' && this.search.phoneCus != undefined) {
      filter.push(
        {
          'Type': 'phoneCus',
          'Value': this.search.phoneCus
        }
      );
    }
    if (this.search.customerName !== '' && this.search.customerName != undefined) {
      filter.push(
        {
          'Type': 'customerName',
          'Value': this.search.customerName
        }
      );
    }

    if (this.search.kcrmId !== '' && this.search.kcrmId != undefined) {
      filter.push(
        {
          'Type': 'kcrmId',
          'Value': this.search.kcrmId
        }
      );
    }

    if (this.search.locationTitle !== '' && this.search.locationTitle != undefined) {
      filter.push(
        {
          'Type': 'locationTitle',
          'Value': this.search.locationTitle
        }
      );
    }

    if (this.search.selectedStatus !== '' && this.search.selectedStatus != undefined) {
      filter.push(
        {
          'Type': 'FK_Status_Id',
          'Value': Number(this.search.selectedStatus)
        }
      );
    }
    if (this.search.selectedPriority != '' && this.search.selectedStatus != undefined) {
      filter.push(
        {
          'Type': 'FK_Priority_Id',
          'Value': Number(this.search.selectedPriority)
        }
      );


    }
    if (filter.length == 0) {
      this.loading = false;
      return;
    }
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let pagingData = Object.assign({}, this.page);
    pagingData.pageNumber = this.page.pageNumber + 1;

    filter[0].pagingParameterModel = pagingData;
    this.lookupService.FillterSubOrders(filter).subscribe((orders) => {

      this.page.totalElements = orders.totalCount;

      let allOrders = orders.result.result;

      allOrders.map((order) => {
          if (order.customer.phones) {
            order.customer.phone = order.customer.phones[0].phone;
          }
          if (order.confirmationHistories && order.confirmationHistories.length > 0) {
            order.confirmationStatus = order.confirmationHistories[0].confirmationStatus.name;
          }
        
      })
      console.log(orders);
      orders = <any[]>allOrders;

      let phoneExist = false;
      this.loading = false;
      if (this.search.phoneCus != '') {
        for (var i = 0; i < allOrders.length; i++) {
          let isExist = allOrders[i].customer.phones.some(x => x.phone == this.search.phoneCus);
          if (isExist)
            this.pendingOrders.push(allOrders[i]);
        }
        phoneExist = true;
      }
      if (this.pendingOrders.length == 0 && phoneExist == false) {
        this.pendingOrders = allOrders;
        this.pendingOrders.forEach(order => {
          if (new Date(order.startDate).getFullYear() != 1) {
            order.startDate = new Date(order.startDate);
            order.startDate.setHours(order.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          }

          if (new Date(order.endDate).getFullYear() == 1 || new Date(order.endDate).getFullYear()==1970) {
            order.endDate = null;
          }
          else {
            order.endDate = new Date(order.endDate);
            order.endDate.setHours(order.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          }

        });





      }

      if (this.search.startDateF !== '' && this.search.startDateF != undefined && this.search.startDateS != undefined && this.search.startDateS != '') {
        let firstDate = moment(moment(this.search.startDateF).format("YYYY-MM-DD HH:mm:ss"));
        let lastDate = moment(moment(this.search.startDateS).format("YYYY-MM-DD HH:mm:ss"));


        this.pendingOrders = this.pendingOrders.filter(x => moment(x.startDate).isBetween(firstDate, lastDate, 'hour'));

      }


      if (this.search.endDate != undefined && this.search.endDate != null && this.search.endDate !== "")
        this.pendingOrders = this.pendingOrders.filter(x => moment(x.endDate).isSame(moment(this.search.endDate), 'day'))




     // this.myTable.offset = 0;

    });
  }

}
