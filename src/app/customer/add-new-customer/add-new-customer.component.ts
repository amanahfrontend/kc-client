import { Router } from '@angular/router';
import { newDetailsCustomer } from './../../api-module/models/newCustomer';
import { Observable } from 'rxjs';
import { LookupService } from './../../api-module/services/lookup-services/lookup.service';
import { allCustomerLookup } from './../../api-module/models/lookups-modal';
import { AlertServiceService } from './../../api-module/services/alertservice/alert-service.service';
import { CustomerCrudService } from './../../api-module/services/customer-crud/customer-crud.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import * as $ from 'jquery';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { customer } from '../../api-module/models/customer-model';
import { UtilitiesService } from '../../operations/utilities.service';
import { isUndefined } from 'util';
import moment = require('moment');

@Component({
  selector: 'app-add-new-customer',
  templateUrl: './add-new-customer.component.html',
  styleUrls: ['./add-new-customer.component.css']
})
export class AddNewCustomerComponent implements OnInit {
  public newCust: any = {
    customer: {},
    contract: {}
    // locations:{}
  };
  kcrmIsExist: boolean;
  public firstSubmitTry = false;
  public contractMixTypeModal = false;
  public firstStep = false;
  public CustNewData: any = {};
  public readyMixTypes: any[] = [];
  public listOfMixTypes: any[] = [];
  // public selectedMixType: any[] = [];
  public singleMixType: any = {};
  public contractObj: any = {};

  isOn: boolean;
  isDisabled: boolean;
  public selectedTab = 'tab1';
  public prevTab = '';
  files: File;
  base64textString: string;
  // --------- Lookups ---------------
  public CustomerType: Subscription;
  public CustomerCategory: Subscription;
  public ContractType: Subscription;
  public Locations: Subscription;
  public Governorates: Subscription;
  public areas: any[];
  public blocks: any[];
  public streets: any[];
  public AddedNewLocation: any = {};
  public TotalPhone = [1];
  public totalPhonesData = [];
  model: any = {};

  // ---------------- Location -----------------
  public newLocation: any = {};
  public AddNewLocation: boolean = false;
  public PACI = '';
  public showG: boolean = true;
  public showB: boolean = true;
  public showA: boolean = true;
  public showS: boolean = true;
  selectedLang: string;

  public loadingImage: string = "assets/Images/loading.gif"
  loading: boolean = false;
  mixTypeData: any;
  id: any;
  numberCount: number = 8;
  phoneState: boolean;
  messageError: string;
  isBlockExist: boolean =false;

  constructor(private utilitiesService: UtilitiesService,private router: Router, private customerService: CustomerCrudService, private lookupService: LookupService, private alertService: AlertServiceService) {

  }

  ngOnInit() {

    if (window.localStorage.getItem('lang')) {
      console.log(window.localStorage.getItem('lang'));
      this.selectedLang = window.localStorage.getItem('lang');
      // this.utilities.chooseLang(this.selectedLang);
    } else {
      this.selectedLang = this.utilitiesService.languages[1].symbol;
    }

    this.loading = true;
    this.newCust.customer.phones = [];
    // ----------------- Call all the Lookups ---------------------
    this.newCust.contract.amount = 0;
    this.getReadyMix();

    this.lookupService.GetAllCustomerTypes().subscribe(types => {
      this.CustomerType = types;
      this.loading = false;
      $('.loading-logo').css('display', 'none')
    },
      err => {
        this.loading = false;
        $('.loading-logo').css('display', 'none')
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetAllCustomerCategories().subscribe(categories => {
      this.CustomerCategory = categories;
      this.loading = false;
      $('.loading-logo').css('display', 'none')
    },
      err => {
        this.loading = false;
        $('.loading-logo').css('display', 'none')
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetAllContractTypes().subscribe(contractTypes => {
      this.ContractType = contractTypes;
      this.loading = false;
      $('.loading-logo').css('display', 'none')
    },
      err => {
        this.loading = false;
        $('.loading-logo').css('display', 'none')
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetallLocations().subscribe(locations => {
      this.Locations = locations;
      this.loading = false;
      $('.loading-logo').css('display', 'none')
    },
      err => {
        this.loading = false;
        $('.loading-logo').css('display', 'none')
        this.alertService.error('your session has timed out please login again !');
      });
    this.lookupService.GetallGovernorates().subscribe(Governorates => {
      this.Governorates = Governorates;
      this.showG = false;
      this.loading = false;
      $('.loading-logo').css('display', 'none')
    },
      err => {
        this.loading = false;
        $('.loading-logo').css('display', 'none')
        this.alertService.error('your session has timed out please login again !');
      });
  }

  /********** Ragab Code *********/

  //checkAmountValidity(list) {
  //  return !list.filter((item) => {
  //    return item.amount < 0.001;
  //  }).length;
  //}

  toggleContractMixTypeModal(form: NgForm) {
    this.contractMixTypeModal = !this.contractMixTypeModal;
  }



  AddToListOfMixEdit() {
    if (this.listOfMixTypes.length < this.readyMixTypes.length) {
      this.listOfMixTypes.push({ price: null });
    }
  }

  removeReadyMixType(readyMixType) {
    let removedFlag: boolean;
    this.listOfMixTypes.map((mixType) => {
      if (JSON.stringify(mixType) == JSON.stringify(readyMixType) && !removedFlag) {
        this.listOfMixTypes.splice(this.listOfMixTypes.indexOf(readyMixType), 1);
        removedFlag = true;
      }
    });
    this.calculateAmount();
  }

  checkSelected(readyMixTypeId) {
    if (this.listOfMixTypes.length) {
      let foundFlag = false;
      for (let i = 0, l = this.listOfMixTypes.length; i < l; i++) {
        if (this.listOfMixTypes[i].id == readyMixTypeId) {
          foundFlag = true;
        }
      }
      return foundFlag;
    } else {
      return false;
    }
  }
  checkNegative(num) {
    if (num < 0) {
      this.alertService.error("you can't enter negative numbers.");

    }

  }
  chooseSelectedType(type) {

    if (type.id != undefined) {
      this.mixTypeData = this.readyMixTypes.find(x => x.id == type.id);
      //if (this.mixTypeData != undefined) {
      //  let itemPriceIndex = this.listOfMixTypes.findIndex(x => x.id == this.mixTypeData.id);
      //  this.listOfMixTypes[itemPriceIndex].price = this.mixTypeData.price;
      //}
    }

    console.log('change');
    console.log(type);
    this.readyMixTypes.map((readyMixType) => {
      if (readyMixType.id == type.id) {
        type.name = readyMixType.name;
      }
    });
  }

  calculateAmount() {
    console.log('change on time');
    this.newCust.contract.amount = 0;
    this.listOfMixTypes.map((type) => {
      this.newCust.contract.amount += Number(type.amount);
    });
  }

  /***********************/

  getFiles(event) {
    this.files = event.target.files[0];
    console.log(this.files);
    this.newCust.contract.fileName = event.target.files[0].name;
    var reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsBinaryString(this.files);

  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.newCust.contract.file = btoa(binaryString);
    console.log(btoa(binaryString));
  }

  CallArea() {
    this.loading = true;
    this.AddedNewLocation.governorate = this.getSelectedNameGov(this.Governorates, this.newLocation.governorate);

    this.lookupService.GetallAreas(this.newLocation.governorate).subscribe(areas => {
      this.areas = areas;
      if (areas != null) {
        this.showA = false;
      }
      this.loading = false;
      console.log(areas);
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  CallBlock() {
    this.loading = true;
    if (this.isBlockExist == false) {
      this.AddedNewLocation.area = this.getSelectedNameGov(this.areas, this.newLocation.area);
      this.loading = false;

    }
   else{
      this.AddedNewLocation.area = this.getSelectedNameGov(this.areas, this.newLocation.area);
    this.lookupService.GetallBlocks(this.newLocation.area).subscribe(blocks => {
      this.blocks = blocks;
      this.loading = false;
      if (blocks != null) {
        this.showB = false;
      }
      console.log(blocks);
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
   }
  }

  CallStreet() {
    this.loading = true;
    this.AddedNewLocation.block = this.newLocation.block;

    this.lookupService.GetallStreets(this.newLocation.area, this.newLocation.block, this.newLocation.governorate).subscribe(streets => {
      this.streets = streets;
      if (streets != null) {
        this.showS = false;
      }
      this.loading = false;
      console.log(streets);

    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  CallGetStreat() {
    if(this.isBlockExist)
    this.AddedNewLocation.street = this.getSelectedName(this.streets, this.newLocation.street);
    else
    {
      this.AddedNewLocation.street =this.newLocation.street;

    }
  }

  getSelectedName(list, id) {
    if (list != null) {
      for (var i = 0; i < list.length; i++) {
        if (list[i].id == id) {
          return list[i].name;
        }
      }
    }
  }

  getSelectedNameGov(list, id) {
    if (list != null) {
      for (var i = 0; i < list.length; i++) {
        if (list[i].id == id) {
          if (this.selectedLang == "en") {
            return list[i].name;

          }
          else {

            return list[i].arabicName;

          }
        }
      }
    }
  }

  checktoSearch(event) {
    if (this.PACI.length > 7) {
      this.GetLocationByPaci();
    }
    else {
      this.newLocation = {};
      // this.AddedNewLocation={};
    }
  }

  GetLocationByPaci() {
    this.loading = true;
    this.lookupService.GetLocationByPaci(+this.PACI).subscribe(loc => {
      if (loc != null) {
        this.newLocation.governorate = loc.governorate.id;
        this.showG = false;
        this.CallArea();
        if (loc.area.id) {
          this.newLocation.area = loc.area.id;
        }
        console.log(this.newLocation.area)
        this.CallBlock();
        if (loc.block.name) {
          this.newLocation.block = loc.block.name;
        }
        this.CallStreet();
        if (loc.street.id) {
          this.newLocation.street = loc.street.id;
        }
        console.log('-----------');
        this.CallGetStreat();
        this.loading = false;

        console.log(this.newLocation);
      }
      else {
        this.showG = false;
        this.showA = true;
        this.showB = true;
        this.showS = true;
        this.newLocation.area = '';
        this.newLocation.block = '';
        this.newLocation.street = '';
        this.loading = false;
        this.alertService.error('there is no location with this PACI number !');
      }
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  createNewLocation() {

    if (this.selectedLang == "en") {
      this.AddedNewLocation.selectedLang = "en"
    }
    else {
      this.AddedNewLocation.selectedLang = "ar"

    }
    // this.AddedNewLocation.fk_Customer_Id= this.id;
    this.AddedNewLocation.PACINumber = this.PACI;
    this.AddedNewLocation.title = this.newLocation.title;
   // this.AddedNewLocation.governorate = this.newLocation.governorate;
   // this.AddedNewLocation.area = this.newLocation.area;
    this.AddedNewLocation.block = this.newLocation.block;
    this.AddedNewLocation.street = this.newLocation.street;
    this.AddedNewLocation.addressNote = this.newLocation.addressNote;

    this.CallGetStreat();

    if (this.AddedNewLocation.PACINumber != '') {
      this.AddedNewLocation.PACINumber = this.PACI;

    }
    console.log(this.AddedNewLocation);
    console.log('AddedNewLocation');

    console.log(this.AddedNewLocation);
    // this.ResetallLocation();

    this.AddNewLocation = false;
    // this.reloadLocation();
  }


  ResetallLocation() {
    this.newLocation = {};
    this.newLocation.governorate = {};
    this.AddedNewLocation = {};
    this.PACI = '';
    this.showG = false;
    this.showB = true;
    this.showA = true;
    this.showS = true;
    this.AddNewLocation = false;
  }

  onSubmit(form) {
    if (this.newCust.customer.phones != undefined) {
      console.log('Valid Form');
      this.AddnewCustomer(false, form);
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');
    }
  }

  checkPhone(phoneValue) {

    let index = phoneValue.charAt(0);
    if (index == 1) {
      if (phoneValue.length != 7) {
        this.alertService.error("Hotline should be only 7 numbers ");
        this.messageError="Hotline should be only 7 numbers ";
        return false;
      }
      else {
        return true;
      }
    }
    else {
      if (phoneValue.length != 8) {
        this.alertService.error("Mobile should be only 8 numbers ");
        this.messageError="Mobile should be only 8 numbers";

        return false;
      }
      else {
        return true;
      }
    }

  }

  eventHandler(event, i) {

    if (this.newCust.customer.phones[i] != undefined) {
      let index = this.newCust.customer.phones[i].charAt(0);
      if (index == 1)
        this.numberCount = 7;
      else
        this.numberCount = 8;
    }
    console.log(event, event.keyCode, event.keyIdentifier);
    if (event.which < 48 || event.which > 57) {
      event.preventDefault();
    }
  }

  preventOverSix(event) {
    if (this.newCust.customer.kcrM_Customer_Id) {
      if (this.newCust.customer.kcrM_Customer_Id.toString().length > 5) {
        event.preventDefault();
      }
    }
  }

  AddToListOfMixAdd() {
    if (this.listOfMixTypes.length < this.readyMixTypes.length) {
      this.listOfMixTypes.push({ price: null });
    }
  }

  AddnewCustomer(isOnly, f: NgForm) {
    console.log(this.newCust);
    this.loading = true;


    if (!isOnly) {

      if (this.validateContractdate(this.newCust.contract.startDate, this.newCust.contract.endDate)) {
        this.loading = false;
        return;
      }
      this.newCust.contract.items = this.listOfMixTypes.slice();


      if (this.listOfMixTypes.slice().length == 0) {
        this.alertService.error('you must add quantity first');
        this.loading = false;
      }
      else {
        if (this.AddedNewLocation.area == null || this.AddedNewLocation.area == null) {
          this.alertService.error('you must add location first');
          this.loading = false;
        }
        else {
          this.newCust.contract.items.map((item) => {
            delete item.name;
            delete item.availableAmount;
            item.fK_Item_Id = item.id;
            delete item.id;
          });

          this.checkAllPhones();

          if( this.phoneState== false)
          {
            this.loading = false;
            return false;
          }
          if (this.AddedNewLocation != isUndefined || this.AddedNewLocation != null) {
            
            this.newCust.location = this.AddedNewLocation;
            //this.newCust.location.contract = null;
            //this.newCust.location.Fk_Contract_Id = 0;
          }
          this.customerService.addCustomer(this.newCust).subscribe(users => {
            console.log(users);
            console.log(users.contract.id)
            this.AddedNewLocation.fK_Contract_Id = users.contract.id;
            console.log(this.AddedNewLocation.fK_Contract_Id)
            this.loading = false;
            //this.customerService.AddNewLocation(this.AddedNewLocation)
            //  .subscribe(() => {
            //    console.log(this.AddedNewLocation)
            //  },
            //    err => {
            //    });
            //console.log(this.AddedNewLocation)

            this.alertService.success('Customer added Successfully');
            //  this.ResetallLocation();
            //this.router.navigate(['search/customer/' + users.customer.id]);
            this.id = users.customer.id;
            this.newCust.contract = {};
            this.listOfMixTypes = [];

            this.ResetallLocation();

          },
            err => {
              this.loading = false;
            });
        }
      }
    }
    else {
      delete this.newCust.contract;
      this.customerService.addCustomer(this.newCust).subscribe(users => {
        console.log(users);
        this.alertService.success('Customer added Successfully');
        this.loading = false;
        this.router.navigate(['search/customer/' + users.customer.id]);

      },
        err => {
          this.loading = false;
        })
    }

  }

  private checkAllPhones() {
    for (let i = 0; i <  this.newCust.customer.phones.length; i++) {
      let result = this.checkPhone(this.newCust.customer.phones[i]);
      if (result == false) {
        this.phoneState = false;
        break;
      }
      else {
        this.phoneState = true;
      }
    }
  }
  validateContractdate(startDate, endData) {
    let sDate = moment(startDate);
    let eDate = moment(endData);
    if (eDate.isBefore(sDate)) {
      this.alertService.error("The contract start date can't be exceed the contract end date ")
      return true;
    }
    else {
      return false;

    }

  }
  Addcontract() {
    this.loading = true;
    this.listOfMixTypes.map((type) => {
      type.fK_Item_Id = type.id;
    });
    this.newCust.contract.startDate = new Date(new Date(this.newCust.contract.startDate).toISOString());
    if (this.newCust.contract.endDate) {
      this.newCust.contract.endDate = new Date(new Date(this.newCust.contract.endDate).toISOString());
    }
    else {
      this.newCust.contract.endDate = null;
    }

    if (this.validateContractdate(this.newCust.contract.startDate, this.newCust.contract.endDate)) {
      this.loading = false;
      return;
    }

    this.newCust.contract.fK_Customer_Id = this.id + '';
    this.newCust.contract.items = {};
    this.newCust.contract.items = this.newCust.contract;


    const savedLocations = [];
    savedLocations.push(this.AddedNewLocation);
    this.contractObj = {
      contract: this.newCust.contract,
      locations: savedLocations
    };
    this.customerService.chechContractNum(this.newCust.contract.contractNumber).subscribe(result => {

      if (this.listOfMixTypes.slice().length == 0) {
        this.alertService.error('you must add quantity first');
        this.loading = false;
      }
      else {
        if (this.AddedNewLocation.area == null) {
          this.alertService.error('you must add location first');
          this.loading = false;
        }

        else {
          this.newCust.contract.items = this.listOfMixTypes.slice();
          this.newCust.contract.items.map((item) => {
            delete item.id;
            delete item.name;
          });

          this.customerService.AddContract(this.contractObj).subscribe(state => {
            if (state) {
              // this.AddedNewLocation.fK_Contract_Id = state.id;
              this.AddedNewLocation.fK_Contract_Id = state.id
              console.log(this.AddedNewLocation.fK_Contract_Id)
              this.loading = false;
              this.customerService.AddNewLocation(this.AddedNewLocation)
                .subscribe(() => {

                },
                  err => {
                  });
              this.newCust.contract = {};
              this.ResetallLocation();
              this.listOfMixTypes = [];

              this.alertService.success('contract added successfully');
              this.loading = false;
            }
            else {
              this.loading = false;
              this.alertService.error('failed To update');
            }
          },
            err => {
              this.loading = false;
              this.alertService.error('we have Error !!');
            });
        }


      }
    }


      , error => {
        this.loading = false;
      })

  }




  KcrmIsExist(kcrmId: any) {
    this.customerService.KCRMIdExist(kcrmId).
      subscribe(response => {
        this.kcrmIsExist = response;
        if (this.kcrmIsExist) {
          this.isDisabled = true;
          this.isOn = false;
          this.alertService.error("KCRM is already exist ");

        }
        else if (this.kcrmIsExist == false) {
          this.isDisabled = false;
          this.isOn = true;
        }
      },
        err => {
        })

  }

  GoToCustomerDetails() {
    if (this.id > 0)
      this.router.navigate(['search/customer/' + this.id]);

  }

  CreateNewCustomerOnly(form, isContinue) {
    if (this.kcrmIsExist == true) {
      this.alertService.error('KCRM is already exist.');
      return false;
    }
    this.ContinueAdd(form, isContinue);
    // this.AddnewCustomer(true);
  }

  ContinueAdd(form, isContinue) {
    if (this.kcrmIsExist == true) {
      this.alertService.error('KCRM is already exist.');
      return false;
    }
    this.loading = true;
    let isOk = true;

    if (isOk) {

      this.checkAllPhones();


      if( this.phoneState== false)
      {
        this.loading = false;
        return false;
      }

      let serviceArr = [];
      this.customerService.chechCivilId(this.newCust.customer.civilId).subscribe(result => {
        if (result == true && this.newCust.customer.civilId != "") {
          this.alertService.info('this civil id alreadt exsit, please change it');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.customerService.checkCustomerNameExist(this.newCust.customer.name).subscribe(exist => {
            if (exist == false) {
              if (this.newCust.customer != undefined) {
                if (!isContinue) {
                  this.AddnewCustomer(true, form);
                } else {
                  this.selectedTab = 'tab2';
                  this.prevTab = 'tab1';
                }
              }
              else {
                this.loading = false;
                this.firstStep = true;
                this.alertService.error('please insert all the required fields to continue');
              }
            }
            else {
              this.loading = false;
              this.alertService.error('this Customer name already exist !!');
            }

          },
            err => {
              this.loading = false;
              this.alertService.error('we have Error !!');
            });
        }
      })
    }

  }

  AddPhoneItem() {
    this.TotalPhone.push(1);
    this.totalPhonesData = this.newCust.customer.phones;
    this.newCust.customer.phones = [];
    this.newCust.customer.phones = this.totalPhonesData;
  }

  DeletePhoneItem(index) {
    console.log(this.newCust.customer.phones);
    console.log(index);
    this.TotalPhone.splice(index, 1);
    this.newCust.customer.phones.splice(index, 1);
    this.totalPhonesData = this.newCust.customer.phones;
    this.newCust.customer.phones = [];
    this.newCust.customer.phones = this.totalPhonesData;
    console.log(this.newCust.customer.phones);
  }

  /*api calls*/

  getReadyMix() {
    this.loading = true;
    this.lookupService.GetallWorkItems().subscribe((readyMixTypes) => {
      this.readyMixTypes = readyMixTypes;
      console.log(this.readyMixTypes);
      this.loading = false;
    },
      err => {

      });
  }

  keyPress(event: any) {

    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  keyPressNegative(e: any) {
    if (!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58)
      || e.keyCode == 8)) {
      return false;
    }
  }

  preventpressNumber(e) {
    e = e || window.event;
    var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
    var charStr = String.fromCharCode(charCode);
    if (/\d/.test(charStr)) {
      return false;
    }
  }

  chechCivilId(civilId) {
    this.customerService.chechCivilId(civilId).subscribe(result => {
      if (result == true) {
        this.alertService.info('this civil id alreadt exsit, please change it')
      }
    }, error => {
      console.log('error')
    })
  }

  stringDate(date) {
    let d = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(),
      date.getMinutes(),
      date.getSeconds()));
    return d.toISOString();
  }


}
