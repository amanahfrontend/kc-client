// import {AuthGuardGuard} from './api-module/guards/auth-guard.guard';
import {NgModule, ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {path: '', loadChildren: './login-register-module/login-register-module.module#LoginRegisterModuleModule'},
  {path: 'search', loadChildren: './customer/customer.module#CustomerModule'},
  {path: 'admin', loadChildren: './admin/admin.module#AdminModule'},
  {path: 'workOrderBoard', loadChildren: './work-order-board/work-order-board.module#WorkOrderBoardModule'},
  {
    path: 'workOrderBoardCalendar',
    loadChildren: './work-order-calendar/work-order-calendar.module#workOrderBoardCalendarModule'
  },
  {path: 'tracking', loadChildren: './tracking/tracking-management.module#TrackingModule'},
  {path: 'report', loadChildren: './report/report-management.module#ReportModule'},
  {path: 'operations', loadChildren: './operations/operations.module#OperationsModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}

export const RoutingComponents = [];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);
