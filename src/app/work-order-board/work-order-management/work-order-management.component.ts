import { equipmentInFactory } from './../../api-module/models/newCustomer';
import { LookupService } from './../../api-module/services/lookup-services/lookup.service';
import { Subscription } from 'rxjs/Subscription';
import { AlertServiceService } from './../../api-module/services/alertservice/alert-service.service';
import { CustomerCrudService } from './../../api-module/services/customer-crud/customer-crud.service';
import { Router, ActivatedRoute, RouterStateSnapshot } from '@angular/router';
import { Component, ElementRef, OnInit, Renderer, OnDestroy, ViewChild } from '@angular/core';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import * as signalR from "@aspnet/signalr";
import * as Config from '../../api-module/services/globalPath';
import { WorkOrderDetails, contract } from '../../api-module/models/customer-model';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import * as moment from 'moment';
import { Page } from '../../shared-module/Page';
import { UtilitiesService } from '../../operations/utilities.service';

@Component({
  selector: 'app-work-order-management',
  templateUrl: './work-order-management.component.html',
  styleUrls: ['./work-order-management.component.css']
})

export class WorkOrderManagementComponent implements OnInit {
  public _hubConnection: signalR.HubConnection;;
  public NotificationAllData: any = [];
  public RemiderAllData;
  public TotalNotification = 0;
  public TotalRemind = 0;
  public showNotifWorkOrder = false;
  public selectedState = 'assign';
  public oneFactory;
  public invalidTotalAmount: boolean = false;
  @ViewChild(DatatableComponent) myTable: DatatableComponent;
  page: Page = new Page();

  public ListOfDividesFactories: Array<{ id: string, amount: number }> = [];
  public EditedWorkOrder: WorkOrderDetails = {};
  public allWorkOrder;
  public Periorities: Subscription;
  public allStatus: any[] = [];
  showNote: boolean;
  confirmationDateModal: boolean = false;
  skipCount: number;
  public currentOrder: any;
  public showWorkOrder: boolean = false;
  public AddEquipment: boolean = false;
  public editWorkOrder: boolean = false;
  public reassignFactory: boolean = false;
  public CancelJobModal: boolean = false;
  public ReassignOption: boolean = false;
  public enableOrderEdit: boolean;
  public workOrderAssignTo = '';
  public JobId = '';
  public AllOrders;
  public NewJob: boolean = false;
  public ShowJobs: boolean = false;

  public showVechileStatus: boolean = false;
  public AllVehicle;
  public AllEqui: Array<equipmentInFactory>;
  public itemEqui: Array<equipmentInFactory>;
  public UserAllRoles;
  public Factories: Subscription;
  public assignedFactory = '';
  public assignedVehicle = '';
  public equipmentAll;
  public JobsAll = [];
  public vehicles;
  public formGroup: any;
  public body;
  private UserID;
  public VShowNot = false;
  public VShowRemind = false;
  public TotalAssignedFactories = 0;
  dispatcherRole: boolean;
  isCallCenter: boolean;
  movementRole: boolean;
  adminRole: boolean;
  public newJobItem: any = {};
  orderConfirmation: any = {};
  confirmationStatus: any[] = [];
  search: any = {
    'selectedCus': '',
    'selectedStatus': '',
    'searchField': '',
    'selectedPriority': '',
    'startDate': '',
    'endDate': '',
    'kcrmId': '',
    'phone': ''

  };
  allItemsToPoured: any[] = [];

  public searchResult = [];
  public type: string = 'success';
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
  phoneExist: boolean;
  isAllowed: boolean = true;
  amount: any;
  showJobAmount: boolean = false;
  cancelReason: string;
  makeJobDone: boolean;
  selectedLang: string;

  constructor(private dragulaService: DragulaService, private utilities: UtilitiesService, private el: ElementRef, private renderer: Renderer, private router: Router, private route: ActivatedRoute,
    private customerService: CustomerCrudService, private alertService: AlertServiceService, private lookupService: LookupService) {


    dragulaService.dropModel.subscribe((value) => {
      this.onDropModel(value.slice(1));
    });
    dragulaService.removeModel.subscribe((value) => {
      this.onRemoveModel(value.slice(1));
    });
    dragulaService.setOptions('another-bag', {
      copy: false
    });

    const bag: any = this.dragulaService.find('another-bag');
    if (bag !== undefined) this.dragulaService.destroy('another-bag');
    this.dragulaService.setOptions('another-bag', { revertOnSpill: true });
    this.page.pageNumber = 1;
    this.page.pageSize = 5;

    if (window.localStorage.getItem('lang')) {
      console.log(window.localStorage.getItem('lang'));
      this.selectedLang = window.localStorage.getItem('lang');
    } else {
      this.selectedLang = this.utilities.languages[1].symbol;
    }
  }



  ngOnDestroy() {
    this.dragulaService.destroy('another-bag');
  }

  getStatusName(status: any) {
    let result = "";
    if (this.selectedLang == "en") {
      result = status == 7 ? 'Dispatched' : (status == 6 ? 'In Progress' : (status == 4 ? 'Cancelled' : (status == 5 ? 'Completed' : '')))

    }
    else {
      result = status == 7 ? 'تم الصب' : (status == 6 ? 'جاري العمل' : (status == 4 ? 'تم الالغاء':(status == 5 ? 'منتهي' : '')))

    }
    return result;
  }

  ngOnInit() {


    let currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this._hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(Config.hub + currentUser.token, { skipNegotiation: true, transport: signalR.HttpTransportType.WebSockets })
      .build();

   
   



    this.GetAllItemPoured();

    this.UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
    console.log(this.UserAllRoles);
    this.dispatcherRole = this.UserAllRoles.includes('Dispatcher');
    this.movementRole = this.UserAllRoles.includes('MovementController');
    this.isCallCenter = this.UserAllRoles.includes('CallCenter');
    this.adminRole = this.UserAllRoles.includes('Admin');
    this.UserID = JSON.parse(localStorage.getItem('currentUser')).id;
    //------------------- if Dispatcher -------------------
    if (this.UserAllRoles.includes('Dispatcher')) {
      this.getAllworkOrders('Dispatcher', { offset: 0 });
    }
    //------------------- if MovementController ----------------
    else if (this.UserAllRoles.includes('MovementController')) {
      this.getAllworkOrders('MovementController', { offset: 0 });
    } else if (this.UserAllRoles.includes('Admin')) {
      this.getAllworkOrders('Admin', { offset: 0 });
    }

    this.CallNeededLookups();
    this.body = document.getElementsByTagName('body')[0];
    this._hubConnection
      .start()
      .then(() => {
        console.log("Connection Started...");
        this.registerSignalEvents();
        this.setUserActiveGroup();
        this.getNotificationAndReminder();
      }).catch(err => console.log('Error while starting connection: ' + err))


 
  }

  setUserActiveGroup() {

    if ((this.UserAllRoles.indexOf('Dispatcher') > -1)) {
      this._hubConnection.invoke('JoinGroup', 'DispatchersGroup');
    }
    else if ((this.UserAllRoles.indexOf('MovementController') > -1)) {
      this._hubConnection.invoke('JoinGroup', 'MovementControllersGroup');
    }
  }

  private registerSignalEvents() {
    this._hubConnection.on("SignalMessageReceived", (data: any) => {
      this.setNotification(data);
    });

    this._hubConnection.on("TemperatureExceeded", (data: any) => {
      
      this.setNotification(data);
    });
  }

 
  setNotification(orders) {
    this.NotificationAllData.push(orders);
    this.TotalNotification = this.NotificationAllData.length;
    this.VShowNot = false;
    

  }
  setNotifications(orders) {




    console.log(orders['entity'].toLowerCase() == 'orderconfirmationhistory');
    if (orders['resultObject']) {
      console.log('resultObject');
      if (orders['entity'].toLowerCase() == 'workorder') {
        console.log('Job');
        let orderIndex = this.searchResult.find((item) => {
          return item.code == orders['resultObject']['workOrder']['code'];
        });
        console.log(orderIndex);
        if (orderIndex) {
          orders['resultObject']['entity'] = orders['entity'];
          this.NotificationAllData.push(orders['resultObject']);
          this.TotalNotification = this.NotificationAllData.length;
          this.VShowNot = false;
        }
      }
      else if (orders['entity'].toLowerCase() == 'job') {
        let order = this.searchResult.find(item => item.id == orders['resultObject']['workOrder']['fK_WorkOrder_ID']);
        if (order) {
          order['entity'] = orders['entity'];
          this.NotificationAllData.push(order);
          this.TotalNotification = this.NotificationAllData.length;
          this.VShowNot = false;
        }
      }
      else if (orders['entity'].toLowerCase() == 'orderconfirmationhistory') {
        console.log('confirmationsss');
        let order = this.searchResult.find(item => item.code == orders['resultObject']['orderNumber']);
        if (order) {
          console.log(order);
          order['entity'] = orders['entity'];
          this.NotificationAllData.push(order);
          this.TotalNotification = this.NotificationAllData.length;
          this.VShowNot = false;
        }
      }
    }
  }











  getNotificationAndReminder() {
    this.NotificationAllData = [];
 

          // listen to late job event////////////////
          this._hubConnection.on(Config.lateJob, (data: any) => {
            this.setNotification(data);
          });

          //// listen to notification event////////////////
          this._hubConnection.on(Config.updatedJob, (data: any) => {
            console.log(data);
            this.setNotification(data);
          });

          //// listen to reminder event////////////////
          this._hubConnection.on(Config.upcomingJob, (data: any) => {
            console.log(data);
            this.setReminder(data);
          });

          //// listen to Order confirmation event////////////////
          this._hubConnection.on(Config.orderConfirmation, (data: any) => {
            console.log(data);
            this.setNotification(data);
          });

    
  }

  GetAllItemPoured() {
    this.loading = true;
    this.lookupService.GetAllItemPoured().
      subscribe(ItemsPoured => {


        this.allItemsToPoured = ItemsPoured;
        this.loading = false;
      },
        err => {
          this.loading = false;
          this.alertService.error('error')
        });
  }


  ngAfterViewInit() {
    this.formGroup = this.el.nativeElement;
  }



  getAllworkOrders(RoleName, pageNumber) {
    debugger;
    this.loading = true;
    this.isFilter = false;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let pagingData = Object.assign({}, this.page);
    pagingData.pageNumber = this.page.pageNumber + 1;

    if (RoleName == 'Dispatcher') {
      this.customerService.GetAllWorkOrdersDispature().subscribe(WorkOrder => {
        this.allWorkOrder = WorkOrder;
        this.searchResult = this.allWorkOrder;
        this.searchResult.forEach(order => {
          if (new Date(order.startDate).getFullYear() != 1) {
            order.startDate = new Date(order.startDate);
            order.startDate.setHours(order.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          }

          if (new Date(order.endDate).getFullYear() == 1) {
            order.endDate = null;
          }
          else {
            order.endDate = new Date(order.endDate);
            order.endDate.setHours(order.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          }

        });

        this.loading = false;
      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        }, () => {
          this.loading = false;


        });
    }
    else if (RoleName == 'MovementController') {
      this.customerService.GetAllWorkOrdersMovementController(pagingData).subscribe(WorkOrder => {
        this.page.totalElements = WorkOrder.item2;

        this.allWorkOrder = WorkOrder.item1;
        this.searchResult = this.allWorkOrder;
        this.searchResult.forEach(order => {
          if (new Date(order.startDate).getFullYear() != 1) {
            order.startDate = new Date(order.startDate);
            order.startDate.setHours(order.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          }

          if (new Date(order.endDate).getFullYear() == 1) {
            order.endDate = null;
          }
          else {
            order.endDate = new Date(order.endDate);
            order.endDate.setHours(order.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          }

        });

        this.loading = false;
        this.searchResult.map((order) => {


          if (order.status.name.toLowerCase() == 'pending') {
            order.sortingId = 0;
          } else if (order.status.name.toLowerCase() == 'in confirmation') {
            order.sortingId = 1;
          } else {
            order.sortingId = 2;
          }
        });
        this.searchResult = this.searchResult.sort((a, b) => {
          return a.sortingId - b.sortingId;
        });
        console.log(this.searchResult);
      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        }, () => {
          // this.searchResult = this.allWorkOrder

        });
    }
    else if (RoleName == 'Admin') {
      this.customerService.GetAllWorkOrdersMovementController(pagingData).subscribe(WorkOrder => {
        this.page.totalElements = WorkOrder.item2;

        this.allWorkOrder = WorkOrder.item1;
        this.searchResult = this.allWorkOrder;
        this.searchResult.forEach(order => {
          if (new Date(order.startDate).getFullYear() != 1) {
            order.startDate = new Date(order.startDate);
            order.startDate.setHours(order.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          }

          if (new Date(order.endDate).getFullYear() == 1) {
            order.endDate = null;
          }
          else {
            order.endDate = new Date(order.endDate);
            order.endDate.setHours(order.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
          }

        });

        this.loading = false;
        this.searchResult.map((order) => {
          if (order.status.name.toLowerCase() == 'pending') {
            order.sortingId = 0;
          } else if (order.status.name.toLowerCase() == 'in confirmation') {
            order.sortingId = 1;
          } else {
            order.sortingId = 2;
          }
        });
        this.searchResult = this.searchResult.sort((a, b) => {
          return a.sortingId - b.sortingId;
        });
        console.log(this.searchResult);
      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        }, () => {
          // this.searchResult = this.allWorkOrder

        });
    }


  }


  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;

    if (this.isFilter == true) {
      this.filterAll(this.page.pageNumber);
    }
    else {
      if (this.UserAllRoles.includes('Dispatcher')) {
        this.getAllworkOrders('Dispatcher', this.page.pageNumber);
      }
      //------------------- if MovementController ----------------
      else if (this.UserAllRoles.includes('MovementController')) {
        this.getAllworkOrders('MovementController', this.page.pageNumber);
      }
      else if (this.UserAllRoles.includes('Admin')) {
        this.getAllworkOrders('Admin', this.page.pageNumber);
      }
    }
  }

  SetStatus(t) {
    var statusName = '';
    if (this.selectedLang == 'en') {
      if (t == "Approved") {
        statusName = 'Approved';
      }
      else if (t == "Pending") {
        statusName = 'Pending';
      }
      else if (t == "Completed") {
        statusName = 'Completed';
      }

      else if (t == "Dispatched") {
        statusName = 'Dispatched';
      }


      else if (t == "In Progress") {
        statusName = 'InProgress';
      }
      else if (t == "Rejected") {
        statusName = 'Rejected';
      }
      else if (t == "Cancelled") {
        statusName = 'Cancelled';
      }
      else if (t == "Confirmed by customer") {
        statusName = 'Confirmed by customer';
      }
      else if (t == "Declined") {
        statusName = 'Declined ';
      }

      return statusName;
    }
    else {
      if (t == "Approved") {
        statusName = 'محجوز';
      }
      else if (t == "Pending") {
        statusName = 'قيد الانتظار';
      }
      else if (t == "Completed") {
        statusName = 'منتهي';
      }

      else if (t == "Dispatched") {
        statusName = 'تم الصب';
      }
      else if (t == "In Progress") {
        statusName = 'جاري العمل ';
      }
      else if (t == "Rejected") {
        statusName = 'تم الرفض';
      }
      else if (t == "Cancelled") {
        statusName = 'تم الالغاء';
      }
      else if (t == "Confirmed by customer") {
        statusName = 'مثبت من العميل';
      }
      else if (t == "Declined") {
        statusName = 'تم الالغاء';
      }

      return statusName;
    }

  }

  SetStatusStyle(t) {
    var statusName = '';
    if (t == "Approved") {
      statusName = 'Approved';
    }
    else if (t == "Pending") {
      statusName = 'Pending';
    }
    else if (t == "Completed") {
      statusName = 'Completed';
    }

    else if (t == "Dispatched") {
      statusName = 'Dispatched';
    }


    else if (t == "In Progress") {
      statusName = 'InProgress';
    }
    else if (t == "Rejected") {
      statusName = 'Rejected';
    }
    else if (t == "Cancelled") {
      statusName = 'Cancelled';
    }
    else if (t == "Confirmed by customer") {
      statusName = 'Confirmed by customer';
    }
    else if (t == "Declined") {
      statusName = 'Declined ';
    }

    return statusName;


  }


  CallNeededLookups() {
    this.loading = true;
    this.lookupService.GetallPeriorities().subscribe(Periorities => {
      this.Periorities = Periorities;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
    this.lookupService.GetAllVehicles().subscribe(vehicle => {
      console.log('vehicles are');
      console.log(vehicle);
      this.vehicles = vehicle;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
    this.customerService.GetAllWorkOrder().subscribe(orders => {
      this.AllOrders = orders;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

    this.lookupService.GetallFactories().subscribe(Factories => {
      this.Factories = Factories;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });


    this.lookupService.GetallStatus().subscribe(allStatus => {
      if (this.movementRole) {
        // this.allStatus.filter(status => status.name !== 'Cancelled')
        this.allStatus = allStatus;
        this.allStatus.map(status => {

          if (status.name == "ChangeDate")
            status.name = "Change Date"
        })
        this.allStatus.splice(1, 1);
      }
      else {
        this.allStatus = allStatus;
      }
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

  }

  private isFilter: boolean = false;

  filterAll(pageNumber) {
    this.loading = true;
    this.searchResult = [];
    this.isFilter = true;
    let filter = [
      // {
      //   "Type": "createdDate",
      //   "Value": ["2017-12-01T00:00:00.00","2018-12-31T00:00:00.00"]
      // },
    ];


    if (this.search.startDate !== '' && this.search.startDate != undefined) {
      let startDate = this.customerService.convertDateForSaving(this.search.startDate);
      filter.push(
        {
          'Type': 'StartDate',
          'Value': [startDate + 'T00:00:00.00', startDate + 'T23:59:59.59']
        }
      );
    }

    if (this.search.endDate !== '' && this.search.endDate != undefined) {

      let endDate = this.customerService.convertDateForSaving(this.search.endDate);
      filter.push(
        {
          'Type': 'EndDate',
          'Value': [endDate + 'T00:00:00.00', endDate + 'T23:59:59.59']
        }
      );
      console.log(this.searchResult);
    }


    if (this.search.phone !== '' && this.search.phone != undefined) {
      filter.push(
        {
          'Type': 'phoneCus',
          'Value': this.search.phone
        }
      );
    }

    if (this.search.searchField !== '' && this.search.searchField != undefined) {
      filter.push(
        {
          'Type': 'Code',
          'Value': this.search.searchField
        }
      );
    }

    if (this.search.kcrmId !== '' && this.search.kcrmId != undefined) {
      filter.push(
        {
          'Type': 'kcrmId',
          'Value': this.search.kcrmId
        }
      );
    }

    if (this.search.selectedStatus !== '' && this.search.selectedStatus != undefined) {
      filter.push(
        {
          'Type': 'FK_Status_Id',
          'Value': Number(this.search.selectedStatus)
        }
      );
    }
    if (this.search.selectedPriority != '' && this.search.selectedPriority != undefined) {
      filter.push(
        {
          'Type': 'FK_Priority_Id',
          'Value': Number(this.search.selectedPriority)
        }
      );
    }
    if (this.search.selectedCus != '' && this.search.selectedCus != undefined) {
      filter.push(
        {
          'Type': 'customerName',
          'Value': this.search.selectedCus
        }
      );
    }

    if (filter.length == 0) {
      this.loading = false;
      return;

    }


    if (this.movementRole || this.adminRole) {
      if (pageNumber > 0)
        this.page.pageNumber = pageNumber;
      else
        this.page.pageNumber = 0;


      let pagingData = Object.assign({}, this.page);
      pagingData.pageNumber = this.page.pageNumber + 1;

      filter[0].pagingParameterModel = pagingData;
      this.lookupService.FillterSubOrders(filter).subscribe((orders) => {
        this.page.totalElements = orders.totalCount;

        let allOrders = orders.result.result;

        orders = <any[]>allOrders;


        let phoneExist = false;

        if (this.search.phone != '' && this.search.phone != undefined) {
          for (var i = 0; i < orders.length; i++) {
            let isExist = orders[i].customer.phones.some(x => x.phone == this.search.phone);


            if (isExist)
              this.searchResult.push(orders[i]);
          }
          phoneExist = true;
        }
        if (this.searchResult.length == 0 && phoneExist == false) {
          {
            this.searchResult = orders;

            this.searchResult.forEach(order => {
              if (new Date(order.startDate).getFullYear() != 1) {
                order.startDate = new Date(order.startDate);
                order.startDate.setHours(order.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
              }

              if (new Date(order.endDate).getFullYear() == 1 || new Date(order.endDate).getFullYear() == 1970) {
                order.endDate = null;
              }
              else {
                order.endDate = new Date(order.endDate);
                order.endDate.setHours(order.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
              }

            });


          }

          if (this.search.startDate != undefined && this.search.startDate != null && this.search.startDate !== "")
            this.searchResult = this.searchResult.filter(x => moment(x.startDate).isSame(this.search.startDate, 'day'));


          if (this.search.endDate != undefined && this.search.endDate != null && this.search.endDate !== "")
            this.searchResult = this.searchResult.filter(x => moment(x.endDate).isSame(this.search.endDate, 'day'))

        }


        this.loading = false;


      });


    }
    else if (this.dispatcherRole) {

      this.lookupService.FilterSubordersForDispatcher(filter).subscribe((orders) => {

        let allOrders = orders;

        orders = <any[]>allOrders;


        let phoneExist = false;

        if (this.search.phone != '' && this.search.phone != undefined) {
          for (var i = 0; i < orders.length; i++) {
            let isExist = orders[i].customer.phones.some(x => x.phone == this.search.phone);


            if (isExist)
              this.searchResult.push(orders[i]);
          }
          phoneExist = true;
        }
        if (this.searchResult.length == 0 && phoneExist == false) {
          {
            this.searchResult = orders;

            this.searchResult.forEach(order => {
              if (new Date(order.startDate).getFullYear() != 1) {
                order.startDate = new Date(order.startDate);
                order.startDate.setHours(order.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
              }

              if (new Date(order.endDate).getFullYear() == 1 || new Date(order.endDate).getFullYear() == 1970) {
                order.endDate = null;
              }
              else {
                order.endDate = new Date(order.endDate);
                order.endDate.setHours(order.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
              }

            });


          }

          if (this.search.startDate != undefined && this.search.startDate != null && this.search.startDate !== "")
            this.searchResult = this.searchResult.filter(x => moment(x.startDate).isSame(this.search.startDate, 'day'));


          if (this.search.endDate != undefined && this.search.endDate != null && this.search.endDate !== "")
            this.searchResult = this.searchResult.filter(x => moment(x.endDate).isSame(this.search.endDate, 'day'))

        }


        this.loading = false;


      });


    }




  }

  checkexist(item) {
    for (var i = 0; i < this.searchResult.length; i++) {
      if (item['id'] == this.searchResult['id']) {
        return true;
      }
    }
    return false;


  }

  ResetAllSearch() {
    this.isFilter = false;
    this.search = {
      'selectedStatus': '',
      'searchField': '',
      'selectedPriority': '',
      'startDate': '',
      'endDate': ''
    };
    this.searchResult = [];

    if (this.UserAllRoles.includes('Dispatcher')) {
      this.getAllworkOrders('Dispatcher', { offset: 0 });
    }
    //------------------- if MovementController ----------------
    else if (this.UserAllRoles.includes('MovementController')) {
      this.getAllworkOrders('MovementController', { offset: 0 });
    }
    else if (this.UserAllRoles.includes('Admin')) {
      this.getAllworkOrders('Admin', { offset: 0 });
    }

  }

  ShowOrderDetails(Order) {
    if (Order.contract == undefined)
      Order.contract = {};
    this.showWorkOrder = true;
    this.currentOrder = Order;
    console.log(this.currentOrder);
    this.currentOrder.parentWorkOrder = this.currentOrder.parentWorkOrder || {};
    this.body.classList.add('noOverflow');
  }

  editOrder(Order) {

    if (Order.status.name.toLowerCase() == "pending") {
      this.alertService.info("The order is pending , please add the approval from the recommendation date ")
      return;
    }

    if (new Date(Order.endDate).getFullYear() == 1 || new Date(Order.endDate).getFullYear() == 1970) {
      Order.endDate = null;
    }
    if (this.UserAllRoles.includes('Dispatcher')) {
      if (Order.status.name.toLowerCase() == "in progress" || Order.status.name.toLowerCase() == "dispatched") {
        this.editWorkOrder = true;
        this.currentOrder = Order;
        console.log(Order);
        console.log(Order.fK_Factory_Id);
        if (Order.fK_Factory_Id) {
          this.assignedFactory = Order.fK_Factory_Id;
          console.log(this.assignedFactory);
        }
        else {
          this.assignedFactory = null;
        }
        // this.enableOrderEdit = this.UserAllRoles.includes('MovementController') && this.currentOrder.fK_Parent_WorkOrder_Id;

        if (((this.movementRole) && (this.currentOrder.status.name.toLowerCase() == 'pending' || this.currentOrder.status.name.toLowerCase() == 'approved')) || (this.dispatcherRole) || (this.adminRole)) {
          this.enableOrderEdit = true;
        } else {
          this.enableOrderEdit = false;
        }

        // this.enableOrderEdit = (this.movementRole && (this.currentOrder.status.name.toLowerCase() == 'pending' || this.currentOrder.status.name.toLowerCase() == 'approved')) || (this.dispatcherRole && this.currentOrder.status.name.toLowerCase() == 'dispatched');

        this.body.classList.add('noOverflow');
      }
    }
    else if (this.UserAllRoles.includes('MovementController') || this.UserAllRoles.includes('Admin')) {
      // if (Order.fK_Status_Id == 2) {
      this.editWorkOrder = true;
      this.currentOrder = Order;
      console.log(Order);
      console.log(Order.fK_Factory_Id);
      if (Order.fK_Factory_Id) {
        this.assignedFactory = Order.fK_Factory_Id;
        console.log(this.assignedFactory);
      }
      // this.enableOrderEdit = this.UserAllRoles.includes('MovementController') && this.currentOrder.fK_Parent_WorkOrder_Id;

      if (((this.movementRole) && (this.currentOrder.status.name.toLowerCase() == 'pending' || this.currentOrder.status.name.toLowerCase() == 'approved')) || (this.dispatcherRole) || (this.adminRole)) {
        this.enableOrderEdit = true;
      } else {
        this.enableOrderEdit = false;
      }

      // this.enableOrderEdit = (this.movementRole && (this.currentOrder.status.name.toLowerCase() == 'pending' || this.currentOrder.status.name.toLowerCase() == 'approved')) || (this.dispatcherRole && this.currentOrder.status.name.toLowerCase() == 'dispatched');

      this.body.classList.add('noOverflow');
      // }
    }

    else {
      this.alertService.info('this order not editable')
    }
  }

  checkEditEnable() {
    let statusName = this.allStatus.filter((state) => {
      return state.id == this.currentOrder.fK_Status_Id;
    })

    if (statusName && statusName.length > 0) {
      return ((this.movementRole) && (statusName[0].name.toLowerCase() != 'pending'));
    }

  }

  enableALtDate() {
    let statusName = this.allStatus.filter((state) => {
      return state.id == this.currentOrder.fK_Status_Id;
    })
    if (statusName[0].name == "Change Date") {
      this.isAllowed = false;
    }
    else {
      this.isAllowed = true;
    }
  }

  /************** RAGAB Logic *************/

  toggleConfirmationDateModal() {
    this.confirmationDateModal = !this.confirmationDateModal;
    if (this.isFilter) {
      this.filterAll(this.page.pageNumber);

    }
    else {
      if (this.UserAllRoles.includes('MovementController')) {
        // this.getAllworkOrders('MovementController');
        this.getAllworkOrders('MovementController', this.page.pageNumber);
      } else if (this.UserAllRoles.includes('Admin')) {
        this.getAllworkOrders('Admin', { offset: 0 });
      }

    }
  }
  toggleConfirmationData() {
    this.confirmationDateModal = !this.confirmationDateModal;

  }

  showRecomendationModal(order) {
    debugger;

    //if (new Date(order.startDate).getFullYear() != 1) {
    this.orderConfirmation.orderDate = new Date(order.startDate);
    //  //this.orderConfirmation.orderDate.setHours(this.orderConfirmation.orderDate.getHours() + this.customerService.getTimeOffsetTimeZone());
    //}
    if (new Date(order.endDate).getFullYear() == 1 || new Date(order.endDate).getFullYear() == 1970) {
      this.orderConfirmation.orderAlternativeDate = null;
    }
    else {
      this.orderConfirmation.orderAlternativeDate = new Date(order.endDate);
      //this.orderConfirmation.orderAlternativeDate.setHours(this.orderConfirmation.orderAlternativeDate.getHours() + this.customerService.getTimeOffsetTimeZone());
    }
    this.orderConfirmation.orderNumber = order.code;
    this.orderConfirmation.fk_WorkOrder_Id = order.id;
    this.orderConfirmation.factoryId = order.fK_Factory_Id;
    this.orderConfirmation.selectedDate = order.selectedDate;
    this.orderConfirmation.fk_ConfirmationStatus_Id = order.fK_ConfirmationStatus_Id;
    this.orderConfirmation.isDeclined = order.isDeclined;
    this.orderConfirmation.reason = order.reason;
    this.orderConfirmation.description = order.description;




    this.toggleConfirmationData();

    console.log(this.orderConfirmation);
  }


  //filterStatus(e) {
  //  console.log(e);
  //  console.log(e.srcElement.textContent);
  //  this.selectStatus(e.srcElement.textContent);
  //  this.filterAll();
  //}

  selectStatus(statusVlaue) {
    this.allStatus.map((state) => {
      if (statusVlaue.toLowerCase() == state.name.toLowerCase()) {
        this.search.selectedStatus = state.id;
      }
    });
  }

  saveWorkOrderEdits() {
    this.loading = true;
    this.invalidTotalAmount = false;
    console.log(this.UserAllRoles);
    console.log(this.UserAllRoles.includes('MovementController'));
    console.log(this.EditedWorkOrder);
    console.log(this.currentOrder);
    this.currentOrder.SubOrdersFactoryPercent = this.EditedWorkOrder.subOrdersFactoryPercent;
    this.currentOrder.WorkOrder = {};
    this.currentOrder.WorkOrder.id = this.currentOrder.id;
    this.currentOrder.WorkOrder.code = this.currentOrder.code;
    this.currentOrder.WorkOrder.amount = this.currentOrder.amount;
    this.currentOrder.WorkOrder.StartDate = this.currentOrder.startDate;
    this.currentOrder.WorkOrder.EndDate = this.currentOrder.endDate;
    this.currentOrder.WorkOrder.FK_Location_Id = this.currentOrder.fK_Location_Id;
    this.currentOrder.WorkOrder.FK_Priority_Id = this.currentOrder.fK_Priority_Id;
    this.currentOrder.WorkOrder.FK_Customer_Id = this.currentOrder.fK_Customer_Id;
    this.currentOrder.WorkOrder.FK_Contract_Id = this.currentOrder.fK_Contract_Id;
    this.currentOrder.WorkOrder.fK_Status_Id = this.currentOrder.fK_Status_Id;

    if (this.currentOrder.items != null) {
      this.currentOrder.WorkOrder.fK_Item = this.currentOrder.items.id;

    }


    if (this.currentOrder.items && this.currentOrder.items.length) {
      this.currentOrder.WorkOrder.items = this.currentOrder.items.slice();
    }
    else {
      this.currentOrder.WorkOrder.items = this.currentOrder.items;
    }

    if (this.UserAllRoles.includes('MovementController') || this.UserAllRoles.includes('Admin')) {


      // delete this.currentOrder.items;
      // delete this.currentOrder.fK_Status_Id;
      // delete this.currentOrder.fK_Contract_Id;
      // delete this.currentOrder.fK_Customer_Id;
      // delete this.currentOrder.fK_Priority_Id;
      // delete this.currentOrder.fK_Location_Id;
      // delete this.currentOrder.startDate;
      // delete this.currentOrder.code;
      // delete this.currentOrder.id;
      // delete this.currentOrder.amount;
      let totalDivide = 0;

      if (this.selectedState == 'assign') {
        console.log('assign');
        console.log(this.currentOrder.amount);

        this.EditedWorkOrder.subOrdersFactoryPercent = [];
        this.EditedWorkOrder.subOrdersFactoryPercent.push({ 'factory': { 'id': this.assignedFactory }, 'amount': this.currentOrder.amount });
        totalDivide += this.currentOrder.amount;
      }
      else {
        console.log('divid');
        this.EditedWorkOrder.subOrdersFactoryPercent = [];
        for (var i = 0; i < this.ListOfDividesFactories.length; i++) {
          this.EditedWorkOrder.subOrdersFactoryPercent.push({
            'factory': { 'id': this.ListOfDividesFactories[i].id },
            'amount': this.ListOfDividesFactories[i].amount
          });
        }
        this.EditedWorkOrder.subOrdersFactoryPercent.map((subOrder) => {
          totalDivide += subOrder.amount;
        });
      }

      this.currentOrder.SubOrdersFactoryPercent = this.EditedWorkOrder.subOrdersFactoryPercent.slice();

      if (this.currentOrder.amount == totalDivide) {
        this.currentOrder.status = null;
        this.currentOrder.priority = null;

        // this.allStatus.filter(status => {
        if (this.currentOrder.fK_Status_Id == 4) {
          this.customerService.EditWorkOrderCancelled(this.currentOrder.WorkOrder.id, 4).subscribe(workOrderUpdated => {
            console.log(workOrderUpdated);
            this.loading = false;
            this.alertService.success('Updated Successfully');
            this.invalidTotalAmount = false;
            this.Resetall();
          },
            err => {
              this.loading = false;
              this.alertService.error('your session has timed out please login again !');
            });
        }

        else {

          if (this.currentOrder.fK_Status_Id != 4) {
            if (this.selectedState == 'assign' && this.assignedFactory == '') {
              this.alertService.error('please select factory first !');
              this.loading = false;
            }
            else {
              this.customerService.EditWorkOrderServ(this.currentOrder).subscribe(workOrderUpdated => {
                console.log(workOrderUpdated);
                this.loading = false;
                this.alertService.success('Updated Successfully');
                this.invalidTotalAmount = false;
                this.Resetall();
              },
                err => {
                  this.loading = false;
                  this.alertService.error('your session has timed out please login again !');
                });
            }
          }
        }

        // })
      } else {
        this.invalidTotalAmount = true;
      }
    } else if (this.UserAllRoles.includes('Dispatcher')) {
      //this.currentOrder.status = null;
      //this.currentOrder.priority = null;
      //this.currentOrder.customer = null;

      this.customerService.UpdateSubOrder(this.currentOrder.WorkOrder).subscribe(workOrderUpdated => {
        this.editWorkOrder = false;
        console.log(workOrderUpdated);
        this.loading = false;
        this.alertService.success('Updated Successfully');
        this.Resetall();
      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !');
        });
    }

  }

  setSelectedState(state) {
    console.log(state);
    this.selectedState = state;
    if (state == 'assign') {
      this.ListOfDividesFactories = [];
      this.oneFactory = undefined;
      this.TotalAssignedFactories = 0;
    } else {
      this.assignedFactory = undefined;
    }
    console.log(this.oneFactory);
  }

  /********************************************/

  DeleteFactoryPer(facto) {
    for (var i = 0; i < this.ListOfDividesFactories.length; i++) {

      if (this.ListOfDividesFactories[i]['id'] == facto) {

        this.ListOfDividesFactories.splice(i, 1);
      }
    }
    this.getTotalAmount();
    console.log(this.ListOfDividesFactories);
  }

  AddToListofFactories() {
    if (this.oneFactory && !this.checkExistanceOfFactory(this.oneFactory)) {
      this.ListOfDividesFactories.push({ 'id': this.oneFactory, 'amount': 0 });
      console.log(this.ListOfDividesFactories);
    }
    // this.oneFactory = undefined;
    console.log(this.oneFactory);
  }

  checkExistanceOfFactory(factory): boolean {
    for (let i of this.ListOfDividesFactories) {
      if (i['id'] == factory) {
        return true;
      }
    }
    return false;
  }

  getTotalAmount() {
    let total = 0;
    for (let i of this.ListOfDividesFactories) {
      total = total + (i['amount']);
    }
    this.TotalAssignedFactories = total;
    console.log(this.TotalAssignedFactories);
  }

  /****************************************/

  Resetall() {
    console.log(this.NewJob);
    this.showNotifWorkOrder = false;
    this.cancelReason = "";

    this.showWorkOrder = false;
    this.AddEquipment = false;
    this.editWorkOrder = false;
    this.reassignFactory = false;
    this.workOrderAssignTo = '';
    this.NewJob = false;
    this.ShowJobs = false;
    this.assignedFactory = '';
    this.JobsAll = [];
    this.AllEqui = [];
    this.itemEqui = [];
    // this.assignedFactory = '';
    this.currentOrder = { 'id': '' };
    this.body.classList.remove('noOverflow');

    if (!this.isFilter) {

      //------------------- if Dispatcher ------------------------------

      if ((this.UserAllRoles.indexOf('Dispatcher') > -1)) {
        this.getAllworkOrders('Dispatcher', this.page.pageNumber);

      }
      //------------------- if MovementController ------------------------------
      else if ((this.UserAllRoles.indexOf('MovementController') > -1)) {
        this.getAllworkOrders('MovementController', this.page.pageNumber);

      }
      else if ((this.UserAllRoles.indexOf('Admin') > -1)) {
        this.getAllworkOrders('Admin', this.page.pageNumber);

      }
    }
    else {
      this.filterAll(this.page.pageNumber);

    }
  }

  reassignToFactory(Order) {

    this.reassignFactory = true;
    this.currentOrder = Order;
    this.body.classList.add('noOverflow');
  }

  ReassigntoNewFactory() {
    this.loading = true;
    if (this.assignedFactory != '') {
      this.customerService.ReassinFactory(this.currentOrder.id, this.assignedFactory).subscribe(status => {
        console.log('Can Assign : ' + status);
        if (status == true) {

          this.customerService.UpdateOrderAssignmentToFactory(this.currentOrder.id, this.assignedFactory).subscribe(status => {
            this.loading = false;
            this.alertService.success('Updated Successfully');
            this.Resetall();
          },
            err => {
              this.loading = false;
              this.alertService.error('your session has timed out please login again !');
            });

        }

      },
        err => {
          this.loading = false;
          this.alertService.error('invalid to assign to the factory');
          this.Resetall();

        });
    }
    else {
      this.loading = false;
      this.alertService.error('No factory selected');
      this.Resetall();

    }
  }

  addEquipment(Order) {
    debugger;
    this.loading = true;
    let factoryId = Order.fK_Factory_Id;
    console.log(factoryId);
    this.customerService.getEquipmentByOrder(Order.id).subscribe((equipments) => {
      console.log(equipments);
      this.itemEqui = equipments;
      this.loading = false;
    },
      err => {
        this.loading = false;
      });

    this.customerService.GetAvailableEquipmentInFactory(factoryId).subscribe(equipment => {
      this.equipmentAll = equipment;
      console.log(equipment);
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      }, () => {
        this.AllEqui = this.equipmentAll;

      });
    this.AddEquipment = true;
    this.currentOrder = Order;
    this.body.classList.add('noOverflow');

  }

  chengeVechileStatus() {

    this.router.navigate(['workOrderBoard/' + 'FactoryVehicles']);
  }

  showAllJobs(Order) {
    let dateNow = moment(new Date()).local();
    let isValid = true;

    if (Order.selectedDate == 1) {
      let firstDate = moment(Order.startDate).local();
      let duration = moment.duration(dateNow.diff(firstDate)).asHours();

      if (duration < 1) {
        this.alertService.error("You can't add an order before one hour of its start");
        isValid = false;
      }
    }
    else if (Order.selectedDate == 2) {
      let endDate = moment(Order.endDate).local();
      let duration = moment.duration(endDate.diff(dateNow)).asHours();

      if (duration > 1) {
        this.alertService.error("You can't add an order before one hour of its start");
        isValid = false;
      }

    }
    if (Order.status.name == "Declined") {
      this.alertService.error('this order declined by movement');
      isValid = false;
    }

    if (isValid) {

      this.ShowJobs = true;
      this.currentOrder = Order;
      this.body.classList.add('noOverflow');

      this.customerService.GetJobDetails(Order.id).subscribe(jobDetails => {
        this.JobsAll = jobDetails;
        //this.JobsAll.map(job => {

        //  if (job.fK_Status_Id=6) {
        //    job.jobStatus ="In Progress"
        //  }
        //  else if (job.fK_Status_Id = 7) {
        //    job.jobStatus = "Dispatched"
        //  }
        //     else if (job.fK_Status_Id = 7) {
        //    job.jobStatus = "Dispatched"
        //  }
        //});
        console.log('---------- jobDetails ----------');
        console.log(jobDetails);

      },
        err => {
          this.alertService.error('your session has timed out please login again !');
        });

    }




  }

  check

  getcurrentValue(JobItem) {

    console.log(JobItem);
    var now = new Date();
    var nowUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
    var Start = new Date(JobItem.startDate);
    console.log('Start >', Start);
    console.log('utc >', nowUTC);
    console.log('min >', Start.getMinutes());
    console.log('min - 2 >', Start.getMinutes() - 2);
    Start.setMinutes(Start.getMinutes() - 2);

    console.log(nowUTC);
    var date = (nowUTC.getTime() - Start.getTime());
    console.log('Subtract');
    var minutes = Math.floor(date / (1000 * 60));

    console.log('elmafrood dyyyy >> ');

    console.log((date / (1000 * 60)));
    console.log('amount');
    console.log(((minutes / 120) * 100));

    if ((minutes / 120) > 1) {
      return 100;
    }
    else if ((minutes / 120) < 0) {
      return 0;

    }
    else {
      return (((minutes / 120) * 100));
    }
  }

  getcurrentValue2(JobItem) {

    console.log(JobItem);
    var now = new Date();
    var nowUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
    var Start = new Date(JobItem.startDate);
    console.log('F > default min ', Start.getMinutes());
    Start.setMinutes(Start.getMinutes() - 2);
    console.log(Start);
    console.log(nowUTC);
    var date = (nowUTC.getTime() - Start.getTime());
    console.log('date > ', date);

    //var minutes = Math.floor(date / (1000 * 60));
    return (date / (1000 * 60)).toFixed(0);

    //console.log("Subtract");
    //console.log((date / (1000 * 60)));
    //console.log("percentage");
    //console.log(((minutes / 120) * 100));

    //if ((minutes / 120) > 1) {
    //  return 100;
    //}
    //else if ((minutes / 120) < 0) {
    //  return 0;

    //}
    //else {
    //  return (((minutes / 120) * 100));

    //}
  }

  toggleNote() {
    this.showNote = !this.showNote;
  }

  AddNewJobdata(order, type?) {
    this.loading = true;
    // if (this.newJobItem.fK_Vehicle_Id != '' && this.newJobItem.amount != '') {
    // let statuses;
    this.newJobItem.fK_WorkOrder_ID = order.id;
    if (type == 'skip') {
      this.lookupService.GetallStatus()
        .subscribe((statusesApi) => {
          console.log(statusesApi);
          this.newJobItem.fk_Status_Id
            = statusesApi.filter((state) => {
              console.log(state.name.toLowerCase() == 'skipped');
              return state.name.toLowerCase() == 'skipped';
            })[0].id;
          // order.deliveryNote = '';
          this.newJobItem.amount = 0;
          console.log(this.newJobItem);
          this.customerService.AddJop(this.newJobItem)
            .subscribe(() => {
              this.customerService.GetReadyVehicle(order.fK_Factory_Id, ++this.skipCount).subscribe(Vehicles => {
                console.log('---------- Vehicle ----------');
                this.toggleNote();
                if (Vehicles && Vehicles.length > 0) {
                  this.AllVehicle = [Vehicles];
                  this.newJobItem.fK_Vehicle_Id = Vehicles.id;
                  console.log(Vehicles);
                  this.loading = false;
                } else {
                  this.AllVehicle = [];
                  this.loading = false;
                }
                this.loading = false;
                this.alertService.success('Vehicle Skipped!');

              },
                err => {
                  this.loading = false;
                  this.alertService.error('your session has timed out please login again !');
                });
            },
              err => {
                this.loading = false;
                this.alertService.error('your session has timed out please login again !');
              });
        },
          err => {
            this.loading = false;
          });
    }



    else {
      if (this.newJobItem.amount > 12 || this.newJobItem.amount <= 0) {
        this.alertService.error('The job should be greater than 0 and less than 12 !');
        this.loading = false;
        return;
      }
      this.customerService.AddJop(this.newJobItem)
        .subscribe(newAddedJob => {
          if (newAddedJob) {
            this.alertService.success('new Job added Successfully!');
          } else {
            this.alertService.error('The added quantity is more than the order quantity!');
          }
          this.cancelSidemodal(this.currentOrder);
          this.loading = false;
        },
          err => {
            this.loading = false;
            this.alertService.error('your session has timed out please login again !');
          });
    }

    console.log(this.newJobItem);

    // }
    // else {
    //   this.alertService.error('required field is missing');
    // }
  }

  AddNewJob(order) {
    this.loading = true;
    this.skipCount = 0;
    this.showNote = false;
    order.deliveryNote = '';
    this.customerService.GetAvailableVehicleInFactory(order.fK_Factory_Id).subscribe(Vehicles => {
      console.log('---------- Vehicle ----------');
      if (Vehicles) {
        this.AllVehicle = Vehicles;
        console.log(Vehicles);
        this.newJobItem.fK_Vehicle_Id = Vehicles.id;
      } else {
        this.AllVehicle = [];
      }
      this.newJobItem.amount = 0;
      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });

    this.ShowJobs = false;
    this.NewJob = true;
    this.body.classList.add('noOverflow');
  }

  skipVehicle() {

  }

  cancelSidemodal(Order) {
    this.NewJob = false;
    this.cancelReason = "";
    this.ShowJobs = true;
    this.CancelJobModal = false;
    this.ReassignOption = false;
    this.AllVehicle = [];
    this.JobId = '';
    this.workOrderAssignTo = '';

    this.newJobItem = {
      'fK_Vehicle_Id': '',
      'amount': '',
      'fK_WorkOrder_ID': '',
      'deliveryNote': ''
    };

    this.customerService.GetJobDetails(Order.id).subscribe(jobDetails => {
      this.JobsAll = jobDetails;
      console.log('---------- jobDetails ----------');
      console.log(jobDetails);
    },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });
  }

  CancelJob(Id) {

    this.JobId = Id;
    this.CancelJobModal = true;
    this.ShowJobs = false;
    this.body.classList.add('noOverflow');

  }
  JobDone(Id) {
    this.makeJobDone = true;
    this.JobId = Id;


  }
  JobQuantity(job) {
    this.ShowJobs = false;
    this.showJobAmount = true;
    this.amount = job.amount;

  }



  closeModel() {
    this.showJobAmount = false;
    this.ShowJobs = true;

  }



  confirmDoneJob() {
    this.makeJobDone = false;

    this.customerService.JobDone(this.JobId).subscribe(cancelJob => {
      this.JobsAll.map(job => {
        if (job.id == this.JobId) {
          job.fK_Status_Id = 7;
        }

      });
      this.alertService.success('Saved Successfully');

    },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });
  }
  cancelJobDone() {
    this.makeJobDone = false;

  }

  confirmCancelJob() {
    if (this.cancelReason == undefined || this.cancelReason == null || this.cancelReason.length <= 0) {
      this.alertService.error('Please enter job cancellation');
      return;
    }
    this.customerService.CancelJop(this.JobId, this.cancelReason).subscribe(cancelJob => {
      console.log(cancelJob);
      this.cancelSidemodal(this.currentOrder);
      this.alertService.success('this job Cancelled Successfully');

    },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });

  }


  confirmReassignJob(order) {
    if (this.workOrderAssignTo != '') {
      this.customerService.ReassignJop(this.JobId, this.workOrderAssignTo).subscribe(ReassignJob => {
        console.log(ReassignJob);
        this.cancelSidemodal(this.currentOrder);
        this.alertService.success('this job reassigned Successfully');

      },
        err => {
          this.alertService.error('your session has timed out please login again !');
        });
    }
    else {
      this.alertService.error('No changed to save');

    }

  }

  ReassignJob(Id) {
    this.JobId = Id;
    this.ReassignOption = true;
    this.ShowJobs = false;
    this.body.classList.add('noOverflow');
  }

  private onDropModel(args) {
    let [el, target, source] = args;
    console.log(this.itemEqui);
    console.log('AllEqui');
    console.log(this.AllEqui);
  }

  private onRemoveModel(args) {
    let [el, source] = args;
    console.log(this.itemEqui);
    console.log('AllEqui');
    console.log(this.AllEqui);

  }

  saveEquipments() {
    let allEquIds = [];

    if (this.itemEqui.length > 0) {
      for (var i = 0; i < this.itemEqui.length; i++) {
        allEquIds.push({ 'id': this.itemEqui[i].id });
      }

      let UpdatedEqui = {
        'WorkOrder': {
          'id': this.currentOrder.id
        },
        'Equipment': allEquIds
      };

      this.customerService.UpdateAssignmentToWorkOrder(UpdatedEqui).subscribe(equipmentStatus => {
        if (equipmentStatus) {
          this.alertService.success('Updated Successfully');
          this.Resetall();
        }
      },
        err => {
          this.alertService.error('your session has timed out please login again !');
        });
    } else if (this.AllEqui.length > 0) {

      console.log('Un assign this');
      for (var i = 0; i < this.AllEqui.length; i++) {
        allEquIds.push({ 'id': this.AllEqui[i].id });
      }

      let UpdatedEqui = {
        'WorkOrder': {
          'id': this.currentOrder.id
        },
        'Equipment': allEquIds
      };

      this.customerService.UnassignEquipmentFromWorkOrder(UpdatedEqui).subscribe(equipmentStatus => {

        if (equipmentStatus) {
          this.alertService.success('Updated Successfully');
          this.Resetall();
        }
      },
        err => {
          this.alertService.error('your session has timed out please login again !');
        });
    }
    else {
      this.alertService.error('No Data to Update');
      this.Resetall();
    }
  }

  ShowReminder() {
    this.VShowNot = false;
    this.VShowRemind = true;
    this.TotalRemind = 0;
  }

  ShowNotifications() {
    this.VShowNot = true;
    this.VShowRemind = false;
    this.TotalNotification = 0;
  }

  CloseNot(event) {
    event.stopPropagation();
    this.VShowNot = false;
  }

  CloseRemi(event) {
    event.stopPropagation();
    this.VShowRemind = false;
  }

  ShowNotiOrder(orderData) {
      //this.showNotifWorkOrder = true;
      //this.body.classList.add('noOverflow');
   
  }




  

  setReminder(data) {
    if (data['resultObject']) {
      this.RemiderAllData = data;
      this.TotalRemind = this.RemiderAllData.length;
      this.VShowRemind = false;
    }
  }

  getDataEntity() {

  }

  keyPressNegative(e: any) {
    if (!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58)
      || e.keyCode == 8)) {
      return false;
    }
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

}
