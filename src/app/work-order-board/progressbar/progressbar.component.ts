import { ProgressDirective } from './../progress.directive';
import {Component, Input} from '@angular/core';
import { NgClass, NgStyle } from '@angular/common';


@Component({
  selector: 'progressbar, [progressbar]',
  providers: [ProgressDirective],
  template: `
  <div progress [animate]="animate" [max]="max">
    <app-bar [type]="type" [value]="value">
        <ng-content></ng-content>
    </app-bar>
  </div>
`
})
export class ProgressbarComponent  {
  @Input() public animate:boolean;
  @Input() public max:number;
  @Input() public type:string;
  @Input() public value:number;


}
