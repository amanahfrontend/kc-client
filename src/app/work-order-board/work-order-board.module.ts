import { AuthGuardGuard } from './../api-module/guards/auth-guard.guard';
import { RouterModule, Routes } from '@angular/router';
import { SharedModuleModule } from './../shared-module/shared-module.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkOrderManagementComponent } from './work-order-management/work-order-management.component';
import { FilterWorkOrderPipe } from './filter-work-order.pipe';
import { ProgressDirective } from './progress.directive';
import { BarComponent } from './bar/bar.component';
import { ProgressbarComponent } from './progressbar/progressbar.component';
import {CalendarModule} from "ap-angular2-fullcalendar";
import { VehicleTypeComponent } from '../admin/admin-main-page/vehicle/vehicle-type.component';
// import {CalendarComponent} from "ap-angular2-fullcalendar";


const routes: Routes = [
  {
    path: '', component:WorkOrderManagementComponent, canActivate: [AuthGuardGuard],data: { roles: ['MovementController','Dispatcher', 'Admin'] }



  },
  {
    path: 'FactoryVehicles', component: VehicleTypeComponent, canActivate: [AuthGuardGuard], data: { roles: ['MovementController', 'Dispatcher', 'Admin'] }

  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CalendarModule,
    SharedModuleModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WorkOrderManagementComponent, FilterWorkOrderPipe, ProgressDirective, BarComponent, ProgressbarComponent]
})
export class WorkOrderBoardModule { }
