import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterWorkOrder'
})
export class FilterWorkOrderPipe implements PipeTransform {

  transform(items: any[], criteria: any): any {
    console.log(criteria);
    if (items){
      return items.filter(item => {
        for (let key in item) {
          if (("" + item[key]).includes(criteria.searchField)) {
            return true;
          }
        }
        return false;
      });
    }

    if(items && criteria == {}){
      return true;
      
    }

  }

}
