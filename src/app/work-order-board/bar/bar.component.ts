import { ProgressDirective } from './../progress.directive';
import {Component, OnInit, OnDestroy, Input, Host} from '@angular/core';
import {NgClass, NgStyle} from '@angular/common';
//[attr.aria - valuemax] = "max"

@Component({
  selector: 'app-bar',
  template: `
<div class="progress-bar"
  style="min-width: 0;"
  role="progressbar"
  [ngClass]="type && 'progress-bar-' + type"
  [ngStyle]="{width: (percent < 100 ? percent : 100) + '%', transition: transition}"
  aria-valuemin="0"
  [attr.aria-valuenow]="value"
  [attr.aria-valuetext]="percent.toFixed(0) + '%'"

  ><ng-content></ng-content></div>
`
})
export class BarComponent implements  OnInit, OnDestroy {
  @Input() public type:string;

  @Input() public get value():number {
      return this._value;
  }

  public set value(v:number) {
      if (!v && v !== 0) {
          return;
      }
      this._value = v;
      this.recalculatePercentage();
  }

  public percent:number = 0;
  public transition:string;

  private _value:number;

  constructor(@Host() public progress:ProgressDirective) {
  }

  ngOnInit() {
      this.progress.addBar(this);
  }

  ngOnDestroy() {
      this.progress.removeBar(this);
  }

  public recalculatePercentage() {
    console.log("F > value ", this.value);
    console.log("F > progress max ", this.progress.max);
      this.percent = +(100 * this.value / this.progress.max).toFixed(2);
      console.log("F > percent ", this.percent );

      let totalPercentage = this.progress.bars.reduce(function (total, bar) {
          return total + bar.percent;
      }, 0);
      console.log("F > totalPercentage ", totalPercentage);

      if (totalPercentage > 100) {
          this.percent -= totalPercentage - 100;
      }
  }
}
