import {Component, DoCheck} from '@angular/core';
import {AuthenticationServicesService} from './api-module/services/authentication/authentication-services.service';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {UtilitiesService} from './operations/utilities.service';

// import { Resolve } from '@angular/router';
// import { Injectable } from '@angular/core';
// import { SignalRModule } from 'ng2-signalr';
// import { IConnectionOptions, SignalR,SignalRConnection } from 'ng2-signalr';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements DoCheck {
  title = 'app';
  public showSpinner = true;
  toggleMenuIcon: boolean;
  callCenterRole: boolean;
  dispatcherRole: boolean;
  movementRole: boolean;
  adminRole: boolean;
  operatorRole: boolean;
  salesRole: boolean;

  constructor(private auth: AuthenticationServicesService, translate: TranslateService, private router: Router, private utilities: UtilitiesService) {
    translate.setDefaultLang('en');
  }


  ngDoCheck() {
    this.toggleMenuIcon = this.auth.CurrentUser();
    if (this.auth.CurrentUser()) {
      this.callCenterRole = this.auth.CurrentUser().roles.includes('CallCenter') || this.auth.CurrentUser().roles.includes('SuperCallCenter');
      this.dispatcherRole = this.auth.CurrentUser().roles.includes('Dispatcher');
      this.movementRole = this.auth.CurrentUser().roles.includes('MovementController');
      this.adminRole = this.auth.CurrentUser().roles.includes('Admin');
      this.operatorRole = this.auth.CurrentUser().roles.includes('Operator');
      this.salesRole = this.auth.CurrentUser().roles.includes('Sales');
    }
  }

  openNav() {
    document.getElementById('mySidenav').style.width = '250px';
    document.getElementById('main').style.marginRight = '250px';
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
    document.getElementById('main').style.marginRight = '0';
  }

  routeToSearch() {
    this.closeNav();
    this.utilities.customerSearch = '';
    this.utilities.showLatestCustomer = true;
    this.router.navigate(['search/']);
  }

// someFunction() {
//   let options: IConnectionOptions = { qs: this.currentUser.token};
//   console.log('ConnectionResolver. Resolving...');
//   this._signalR.connect(options).then((c) => {
//     console.log(c)
//   });
// }

}
