import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-admin-main-page',
  templateUrl: './admin-main-page.component.html',
  styleUrls: ['./admin-main-page.component.css', '../../customer/existing-customer/existing-customer.component.css']
})
export class AdminMainPageComponent implements OnInit {

  AllItems = ['Status', 'PouredElement','ApplicationConfiguration', 'Role', 'User', 'Priority', 'CustomerType', 'CustomerCategory', 'ContractType', 'EquipmentType', 'Equipment', 'Item', 'Factory', 'FactoryVehicles', 'Washing', 'FactoryUser'];
  public manageItem: any = this.AllItems[0];

  constructor(private router: Router) {
  }

  ngOnInit() {

  }

  ChangeRoute() {
    // if (this.manageItem === 'FactoryVehicles') {
    //   this.manageItem = 'VehicleType';
    //   this.router.navigate(['admin/' + this.manageItem]);
    // } else {
      this.router.navigate(['admin/' + this.manageItem]);
    // }

  }

  componentAdded(event) {
    var routName = event.constructor.name.replace('Component', '');
    console.log(routName.charAt(0).toUpperCase() + routName.substr(1, routName.length - 1));
    this.manageItem = routName.charAt(0).toUpperCase() + routName.substr(1, routName.length - 1);
  }


}
