import {LookupService} from './../../../api-module/services/lookup-services/lookup.service';
import {allEquipmentTypeLookup} from './../../../api-module/models/lookups-modal';
import {ActivatedRoute} from '@angular/router';
import {AlertServiceService} from './../../../api-module/services/alertservice/alert-service.service';
import {Subscription} from 'rxjs/Subscription';
import {Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter} from '@angular/core';


@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.css', './../../../customer/existing-customer/existing-customer.component.css']
})
export class EquipmentComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd = false;
  public isDirty: boolean = false;
  public EquipmentInfo: Array<allEquipmentTypeLookup>;
  public newEquipment: allEquipmentTypeLookup = {};
  public Equipment: allEquipmentTypeLookup;
  public EquipmentLookup: any;
  public FactoryLookup: any;

  public editEquipment: boolean = false;
  public AddEquipment: boolean = false;

  EquipmentColumns: any[] = [
    {name: 'id'},
    {name: 'name'},
    {name: 'number'},
    {name: 'fK_EquipmentType_Id'},
    {name: 'isAvailable'}
  ];
  selectedEquipment = [];


  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup: LookupService) {
  }

  ngOnInit() {
    this.GetAllEquipment();
    this.GetLookup();

  }


  ResetInfo() {

    this.GetAllEquipment();
    this.editEquipment = false;
    this.AddEquipment = false;
    this.selectedEquipment = [];
    this.newEquipment = {};
    this.firstSubmitTryadd = false;
    this.firstSubmitTry = false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editEquipment = false;
        this.ResetInfo();
      }
      else {
        this.EditEquipment();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddEquipment = false;
        this.ResetInfo();
      }
      else {
        this.AddnewEquipment();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  EditEquipment() {
    this.Lookup.EditEquipment(this.selectedEquipment[0]).subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
        }
        else {
          this.alertService.error('failed To update');
        }
      },
      err => {
        this.alertService.error('we have Error !!');
      });
  }

  AddnewEquipment() {
    console.log(this.newEquipment);
    this.Lookup.AddEquipment(this.newEquipment).subscribe(Equipment => {
        console.log(Equipment);
        if (Equipment) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
        }
        else {
          this.alertService.error('failed To update');
        }
      },
      err => {
        this.alertService.error('we have Error !!');
      });
  }


  //----------------- Complains Functions -----------------

  DeleteAllEquipment() {

    // for(var i=0;i<this.selectedEquipment.length;i++){
    this.Lookup.DeleteEquipment(this.selectedEquipment[0].id).subscribe(state => {
        this.alertService.success(this.selectedEquipment[0].name + ' Deleted Successfully');
        this.ResetInfo();
      },
      err => {
        this.alertService.error('This Item is already Used !!');
        this.ResetInfo();
      });
    // }
  }

  GetAllEquipment() {
    this.Lookup.GetallEquipment().subscribe(Equipment => {

        console.log(Equipment);
        this.EquipmentInfo = Equipment;
      },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });
  }


  GetLookup() {
    this.Lookup.GetallEquipmentType().subscribe(Equipment => {

        console.log(Equipment);
        this.EquipmentLookup = Equipment;
      },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });

    this.Lookup.GetallFactories().subscribe(factories => {
        console.log(factories);
        this.FactoryLookup = factories;
      },
      err => {
        this.alertService.error('your session has timed out please login again !');
      });
  }
}
