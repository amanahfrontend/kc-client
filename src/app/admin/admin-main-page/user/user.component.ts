import {LookupService} from './../../../api-module/services/lookup-services/lookup.service';
import {allUserLookup, allRolesLookup} from './../../../api-module/models/lookups-modal';
import {ActivatedRoute} from '@angular/router';
import {AlertServiceService} from './../../../api-module/services/alertservice/alert-service.service';
import {Subscription} from 'rxjs/Subscription';
import {Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.compone' +
  'nt.html',
  styleUrls: ['./user.component.css', './../../../customer/existing-customer/existing-customer.component.css']
})

export class UserComponent implements OnInit {

  public SelectedAllRoles: any[] = [];
  public AllRoles: allRolesLookup;
  public RoleItem: any = '';
  public stat1: any = false;
  public stat2: any = false;

  public firstSubmitTry = false;
  public firstSubmitTryadd = false;
  public isDirty: boolean = false;
  public UserInfo: Array<allUserLookup>;
  public newUser: allUserLookup = {};
  public User: allUserLookup;
  public repreatPass: any = '';
  passwordRegex;
  public editUser: boolean = false;
  public resetPassword: boolean = false;
  public AddUser: boolean = false;

  UserColumns: any[] = [
    {name: 'id'},
    {name: 'userName'},
    {name: 'phoneNumber'},
    {name: 'email'},
    {name: 'password'},
    {name: 'firstName'},
    {name: 'lastName'},
    {name: 'roleNames'}

  ];

  private editedUser: any =
    {
      'phoneNumber': '',
      'id': '',
      'userName': '',
      'email': '',
      'firstName': '',
      'lastName': '',
      'roleNames': []
    };

  private resetPasswordObj: any =
    {
      'password': '',
      'userName': '',
    };


  selectedUser = [];
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;

  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup: LookupService) {
  }

  ngOnInit() {
    this.passwordRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    this.GetAllUser();
    this.getLookup();
  }


  ResetInfo() {

    this.GetAllUser();
    this.editUser = false;
    this.AddUser = false;
    this.selectedUser = [];
    this.newUser = {};
    this.firstSubmitTryadd = false;
    this.firstSubmitTry = false;
    this.repreatPass = '';
    this.stat1 = false;
    this.stat2 = false;


  }

  // onSubmit(form) {
  //   if (form.valid) {
  //     console.log('Valid Form');
  //     if (!this.isDirty) {
  //       this.alertService.error('No Changes !');
  //       this.editUser = false;
  //       this.ResetInfo();
  //     }
  //     else {
  //       this.EditUser();
  //     }
  //   } else {
  //     this.firstSubmitTry = true;
  //     console.log('In Valid Form');

  //   }
  // }

  onAdd(form) {

    this.newUser.roleNames = this.SelectedAllRoles;
    console.log(this.newUser);
    this.checkMail()
      .then(() => {
        this.checkName()
          .then(() => {

            console.log(this.stat2 && this.stat2);
            console.log(this.SelectedAllRoles.length > 0);
            console.log(this.repreatPass == this.newUser.password);
            console.log(form);

            if (form.valid && (this.repreatPass == this.newUser.password)
              && this.SelectedAllRoles.length > 0 && this.stat2 && this.stat2) {
              console.log('Valid Form');
              if (!this.isDirty) {
                this.alertService.error('No Changes !');
                this.AddUser = false;
                this.ResetInfo();
              }
              else {
                this.AddnewUser();
              }
            } else {
              this.firstSubmitTryadd = true;
              console.log('In Valid Form');
            }
          });
      });
  }

  checkMail() {
    console.log('check mail');
    return new Promise((resolve) => {
      this.Lookup.EmailExists(this.newUser.email).subscribe(isExist => {
          console.log('check mail: ' + isExist);
          if (!isExist) {
            this.stat1 = true;
          }
          else {
            this.alertService.error('Mail is aleady exist');
            this.stat1 = false;
          }
          resolve(this.stat1);
        },
        err => {
          this.alertService.error('we have Error !!');
          this.stat1 = false;
          resolve(this.stat1);
        });
    });
  }

  checkName() {
    console.log('check name');
    return new Promise((resolve) => {
      this.Lookup.UsernameExists(this.newUser.userName).subscribe(isExisted => {
          console.log('check name: ' + isExisted);
          if (!isExisted) {
            this.stat2 = true;
          }
          else {
            this.alertService.error('User Name is aleady exist');
            this.stat2 = false;
          }
          resolve(this.stat2);
        },
        err => {
          this.alertService.error('we have Error !!');
          this.stat2 = false;
          resolve(this.stat2);
        });
    });
  }

  // EditUser() {
  //   this.Lookup.EditUser(this.selectedUser[0]).
  //     subscribe(state => {
  //       console.log(state);
  //       if (state) {
  //         this.ResetInfo();
  //         this.alertService.success(' Updated Successfully');
  //       }
  //       else {
  //         this.alertService.error('failed To update')
  //       }
  //     },
  //     err => {
  //       this.alertService.error('we have Error !!')
  //     });
  // }

  AddnewUser() {
    this.loading = true;
    this.newUser.roleNames = this.SelectedAllRoles;
    this.Lookup.AddUser(this.newUser).subscribe(User => {
        console.log(User);
        if (User) {
          this.ResetInfo();
          this.loading = false;
          this.alertService.success(' Updated Successfully');
        }
        else {
          this.ResetInfo();
          this.loading = false;
          this.alertService.error('failed To update');
        }
      },
      err => {
        this.ResetInfo();
        this.loading = false;
        this.alertService.error('we have Error !!');
      });
  }


  //----------------- Complains Functions -----------------

  DeleteAllUser() {
debugger;
    // for(var i=0;i<this.selectedUser.length;i++){
    this.Lookup.DeleteUser(this.selectedUser[0]).subscribe(state => {
        this.alertService.success(this.selectedUser[0].userName + ' Deleted Successfully');
        this.ResetInfo();
      },
      err => {
        this.alertService.error('This Item is already Used !!');
        this.ResetInfo();
      });
    // }
  }

  GetAllUser() {
    this.loading = true;
    this.Lookup.GetallUser().subscribe(User => {

        console.log(User);
        this.UserInfo = User;
        this.loading = false;
      },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  getLookup() {
    this.loading = true;
    this.Lookup.GetallRole().subscribe(AllRoles => {

        console.log(AllRoles);
        this.AllRoles = AllRoles;
        this.loading = false;
      },
      err => {
        this.getLookup();
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }

  AddToListofRoles() {
    if (!this.checkExistanceOfRoles(this.RoleItem)) {
      this.SelectedAllRoles.push(this.RoleItem);
      this.RoleItem = '';
    }
  }

  checkExistanceOfRoles(Role): boolean {
    for (let i of this.SelectedAllRoles) {
      if (i == Role) {
        return true;
      }
    }
    return false;
  }

  DeleteRolePer(rol) {
    for (var i = 0; i < this.SelectedAllRoles.length; i++) {
      if (this.SelectedAllRoles[i] == rol) {
        this.SelectedAllRoles.splice(i, 1);
      }
    }
  }

  SaveChanges() {
    this.loading = true;
    this.editedUser.id = this.selectedUser[0].id;
    this.editedUser.userName = this.selectedUser[0].userName;
    this.editedUser.firstName = this.selectedUser[0].firstName;
    this.editedUser.lastName = this.selectedUser[0].lastName;
    this.editedUser.phoneNumber = this.selectedUser[0].phoneNumber;
    this.editedUser.email = this.selectedUser[0].email;
    this.editedUser.roleNames.push(this.selectedUser[0].roleNames[0]);
    console.log(this.editedUser);
    debugger;
    this.Lookup.UpdateUser(this.editedUser).subscribe(res => {
        this.editUser = false;
        if (res) {
          this.loading = false;
          this.alertService.success('User Updated Successfully!');
        } else {
          this.loading = false;
          this.alertService.error('There is Server Error, Please try again!');
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('There is Server Error, Please try again!');
      });
  }
  SaveResetPassword(){
    this.loading = true;
    this.resetPasswordObj.userName = this.selectedUser[0].userName;
    this.resetPasswordObj.password = this.selectedUser[0].password;
    this.Lookup.ResetPassword(this.resetPasswordObj).subscribe(res => {
        this.resetPassword = false;
        if (res) {
          this.loading = false;
          this.alertService.success('Password Reset Successfully!');
        } else {
          this.loading = false;
          this.alertService.error('There is Server Error, Please try again!');
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('There is Server Error, Please try again!');
      });
  }

}
