import {LookupService} from './../../../api-module/services/lookup-services/lookup.service';
import {ActivatedRoute} from '@angular/router';
import {AlertServiceService} from './../../../api-module/services/alertservice/alert-service.service';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-factory-user',
  templateUrl: './factory-user.component.html',
  styleUrls: ['./factory-user.component.css', './../../../customer/existing-customer/existing-customer.component.css']
})
export class FactoryUserComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd = false;
  public isDirty: boolean = false;
  public FactoryUsers: Array<any>;
  public newAssign: any = {
    userId: '',
    factoryId: 0
  };

  public assignedDB: any = {
    userId: '',
    factoryId: 0,
    id: 0
  };

  public factories: any;
  public dispatchers: any;

  public editDispatcherToFactory: boolean = false;
  public addDispatcherToFactory: boolean = false;
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;

  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup: LookupService) {
  }


  FactoryUserColumns: any[] = [
    {name: 'id'},
    {name: 'number'}

  ];
  selectedFactoryUser = [];

  ngOnInit() {

    this.GetAllFactoryUsers();
    this.getAllDispatchers();
    this.getAllFactories();
  }


  ResetInfo() {

    this.GetAllFactoryUsers();
    this.editDispatcherToFactory = false;
    this.addDispatcherToFactory = false;
    this.selectedFactoryUser = [];
    this.newAssign = {};
    this.firstSubmitTryadd = false;
    this.firstSubmitTry = false;

  }

  onSubmit(form) {
    //if (form.valid) {
      console.log('Valid Form');
      // // if (!this.isDirty) {
      //   this.alertService.error('No Changes !');
      //   this.editDispatcherToFactory = false;
      //   this.ResetInfo();
      // }
      // else {
        this.EditDispatcherToFactory();
      // }
    // } else {
    //   this.firstSubmitTry = true;
    //   console.log('In Valid Form');
    //
    // }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.addDispatcherToFactory = false;
        this.ResetInfo();
      } else {
        this.AssignDispatcherToFactory();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');
    }
  }

  EditDispatcherToFactory() {
    this.loading = true;
    this.Lookup.UpdateDispatcherToFactory(this.selectedFactoryUser[0]).subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update');
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!');
      });
  }

  AssignDispatcherToFactory() {
    this.loading = true;
    this.Lookup.AssignDispatcherToFactory(this.newAssign).subscribe(assigned => {
        console.log(assigned);
        if (assigned) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.alertService.error('failed To update');
          this.loading = false;
        }
      },
      err => {
        this.alertService.error('we have Error !!');
        this.loading = false;
      });
  }

  DeleteDispatcherToFactory() {

    // for(var i=0;i<this.selectedContractType.length;i++){
    this.Lookup.DeleteDispatcherToFactory(this.selectedFactoryUser[0].id).subscribe(state => {
        this.alertService.success(this.selectedFactoryUser[0].number + ' Deleted Successfully');
        this.ResetInfo();
      },
      err => {
        this.alertService.error('This Item is already Used !!');
        this.ResetInfo();
      });
    // }
  }

  getAllDispatchers() {
    this.loading = true;
    this.Lookup.GetUsersByRoleName('Dispatcher').subscribe(users => {
      this.dispatchers = users;
      this.loading = false;
    });
  }

  getAllFactories() {
    this.loading = true;
    this.Lookup.GetallFactories().subscribe(factories => {
      this.factories = factories;
      this.loading = false;
    });
  }

  GetAllFactoryUsers() {
    this.loading = true;
    this.FactoryUsers = [];

    this.Lookup.GetAllFactoryControllers().subscribe(factoryUsers => {
        // factoryUsers.map(user => {
        //   this.assignedDB.factoryId = user.factory.name;
        //   this.assignedDB.userId = user.controller.userName;
        //   this.assignedDB.id = user.id;
        //   this.FactoryUsers.push(this.assignedDB);
        // });
      this.FactoryUsers = factoryUsers;
        console.log(this.FactoryUsers);
        this.loading = false;
      },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }
}
