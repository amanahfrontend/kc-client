import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { allEquipmentTypeLookup } from './../../../api-module/models/lookups-modal';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-application-configuration',
  templateUrl: './application-configuration.html',
  styleUrls: ['./application-configuration.css','./../../../customer/existing-customer/existing-customer.component.css']
})
export class ApplicationConfigurationComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd=false;
  public isDirty: boolean = false;
  public allAppConfigurationa: any[] = [];
 

  public editElement: boolean = false;
  public AddElement:boolean=false;

  EquipmentTypeColumns: any[] = [
    { name: 'id' },
    { name: 'name' }

  ];
  selectedAppConfiguration= [];
  public loadingImage: string = "assets/Images/loading.gif"
    loading = false;
  newAppConfiguration: any = {};
   
  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup:LookupService) { }
  
  ngOnInit() {

    this.GetAllApplicationConfigurarion();
  }


  ResetInfo() {

    this.GetAllApplicationConfigurarion();
      this.editElement = false;
      this.AddElement = false;
    this.selectedAppConfiguration = [];
    this.newAppConfiguration={};
    this.firstSubmitTryadd=false;
    this.firstSubmitTry=false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
          this.alertService.error('No Changes !');
          this.editElement = false;
        this.ResetInfo();
      }
      else {
        this.EditAppConfiguration();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
          this.AddElement = false;
        this.ResetInfo();
      }
      else {
          this.AddAppConfiguration();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  EditAppConfiguration() {
    this.Lookup.EditAppConfiguration(this.selectedAppConfiguration[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
        }
        else {
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.alertService.error('we have Error !!')
      });
  }

  AddAppConfiguration() {

      this.Lookup.AddAppConfiguration(this.newAppConfiguration).
        subscribe(newAppConfigure => {
          if (newAppConfigure) {
          this.ResetInfo();          
          this.alertService.success(' Updated Successfully')
        }
        else {
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.alertService.error('we have Error !!')
      });
  }
    

    //----------------- Complains Functions -----------------

  DeleteAppConfiguration() {

      // for(var i=0;i<this.selectedEquipmentType.length;i++){
      this.Lookup.DeleteAppConfiguration(this.selectedAppConfiguration[0].id).
        subscribe(state => {
          this.alertService.success(this.selectedAppConfiguration[0].name +' Deleted Successfully');
          this.ResetInfo();
        },
        err => {
          this.alertService.error('This Item is already Used !!');
          this.ResetInfo();
        });
      // }
    }

  GetAllApplicationConfigurarion(){
      this.loading = true;
      this.Lookup.GetAllApplicationConfigurarion().
            subscribe(appConfigurations => {
  

              this.allAppConfigurationa = appConfigurations;
        this.loading = false;
        },
        err => {              
          this.loading = false;
          this.alertService.error('error')        
        });
    }
  }
