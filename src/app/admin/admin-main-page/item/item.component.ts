import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { allWorkOrderItemsLookup } from './../../../api-module/models/lookups-modal';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Tree } from '@angular/router/src/utils/tree';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css', './../../../customer/existing-customer/existing-customer.component.css']
})
export class ItemComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd = false;
  public isDirty: boolean = false;
  public ItemInfo: Array<allWorkOrderItemsLookup>;
  public newItem: allWorkOrderItemsLookup = {};
  public Item: allWorkOrderItemsLookup;
  itemCodeIsExist: boolean;
  isOn: boolean;
  isDisabled: boolean;

  public editItem: boolean = false;
  public AddItem: boolean = false;

  ItemColumns: any[] = [
    { name: 'id' },
    { name: 'name' },
    { name: 'code' },
    { name: 'description' },
    { name: 'formula' },
    { name: 'availableAmount' }

  ];
  selectedItem = [];
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
  codeId: any;

  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup: LookupService) { }

  ngOnInit() {

    this.GetAllItem();
  }

  ItemCodeExists(codeId: any): any {
    this.Lookup.ItemCodeExists(codeId).
      subscribe(response => {
        this.itemCodeIsExist = response;
        if (this.itemCodeIsExist) {
          this.isDisabled = true;
          this.isOn = false;
          this.alertService.error("Item Code is already exist ");
          return true;
        }
        else if (this.itemCodeIsExist == false) {
          this.EditItem();
        }
      },
        err => {
        })
  }

  ResetInfo() {

    this.GetAllItem();
    this.editItem = false;
    this.AddItem = false;
    this.selectedItem = [];
    this.newItem = {};
    this.firstSubmitTryadd = false;
    this.firstSubmitTry = false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editItem = false;
        this.ResetInfo();
      }
      else {
        this.checkIfItemIsExist();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddItem = false;
        this.ResetInfo();
      }
      else {
        this.AddnewItem();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }
  onSelect(itemSelcted: any) {
    if (itemSelcted.selected[0].code != undefined) {
      this.codeId = itemSelcted.selected[0].code;
    }
  }
  checkIfItemIsExist() {
    if (this.codeId != this.selectedItem[0].code) {
      this.ItemCodeExists(this.selectedItem[0].code);
    }
    else {
      this.EditItem();
    }

  }
  EditItem() {
    this.loading = true;
  
    this.Lookup.EditItem(this.selectedItem[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update');
        }
      },
        err => {
          this.loading = false;
          this.alertService.error('we have Error !!')
        });

  }

  AddnewItem() {
    this.loading = true;
    if (this.itemCodeIsExist == true) {
      this.alertService.error('Item Code is already exist.');
      this.loading = false;
      return false;
    }
    this.Lookup.AddItem(this.newItem).
      subscribe(Item => {
        console.log(Item);
        if (Item) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully')
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
        err => {
          this.loading = false;
          this.alertService.error('we have Error !!')
        });
  }


  //----------------- Complains Functions -----------------

  DeleteAllItem() {

    // for(var i=0;i<this.selectedItem.length;i++){
    this.Lookup.DeleteItem(this.selectedItem[0].id).
      subscribe(state => {
        this.alertService.success(this.selectedItem[0].name + ' Deleted Successfully');
        this.ResetInfo();
      },
        err => {
          this.alertService.error('This Item is already Used !!');
          this.ResetInfo();
        });
    // }
  }

  GetAllItem() {
    this.loading = true;
    this.Lookup.GetallWorkItems().
      subscribe(Item => {

        console.log(Item);
        this.ItemInfo = Item;
        this.loading = false;
      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !')
        });
  }
}
