import {ImplicitReceiver} from '@angular/compiler';


// <<<<<<< HEAD
// import {LookupService} from './../../../api-module/services/lookup-services/lookup.service';
// import {allFactoryLookup} from './../../../api-module/models/lookups-modal';
// import {ActivatedRoute} from '@angular/router';
// import {AlertServiceService} from './../../../api-module/services/alertservice/alert-service.service';
// import {Subscription} from 'rxjs/Subscription';
// import {
//   Component,
//   OnInit,
//   OnDestroy,
//   ViewChild,
//   Input,
//   Output,
//   EventEmitter,
//   ElementRef,
//   NgZone,
//   ViewEncapsulation
// } from '@angular/core';
// import {FormControl, FormsModule, ReactiveFormsModule} from "@angular/forms";
// import {AgmCoreModule, MapsAPILoader} from '@agm/core';
// import {} from '@types/googlemaps';
// =======
import {LookupService} from './../../../api-module/services/lookup-services/lookup.service';
import {allAreaLookup, allFactoryLookup, point} from './../../../api-module/models/lookups-modal';
import {ActivatedRoute} from '@angular/router';
import {AlertServiceService} from './../../../api-module/services/alertservice/alert-service.service';
import {Subscription} from 'rxjs/Subscription';
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  NgZone,
  ViewEncapsulation
} from '@angular/core';
import {FormControl, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AgmCoreModule, MapsAPILoader} from '@agm/core';

//import { DrawingManager } from '@ngui/map';
// >>>>>>> 13561a5420c8a1209994293495095d9e1e8880b8

// import { asNativeElements } from '@angular/core/src/debug/debug_node';

@Component({
// <<<<<<< HEAD
//   selector: 'app-factory',
//   templateUrl: './factory.component.html',
//   styleUrls: ['./factory.component.css', './../../../customer/existing-customer/existing-customer.component.css'],
//   encapsulation: ViewEncapsulation.None
// =======
  selector: 'app-washing',
  templateUrl: './washing.component.html',
  styleUrls: ['./washing.component.css', './../../../customer/existing-customer/existing-customer.component.css'],
// >>>>>>> 13561a5420c8a1209994293495095d9e1e8880b8
})
export class WashingComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd = false;
  public isDirty: boolean = false;
  public FactoryInfo: Array<allFactoryLookup>;
  public AreaInfo: Array<allAreaLookup>;
  public newFactory: allFactoryLookup = {};
  public Factory: allFactoryLookup;
  public LocationLookup: any;
  public dragLat: any;
  public dragLng: any;
  public WashingArea: any;
  public editWashingArea: boolean = false;
  public AddWashingArea: boolean = false;
  selectedOverlay: any;

  FactoryColumns: any[] = [
    {name: 'id'},
    {name: 'name'},
    {name: 'fK_Location_Id'},
    {name: 'latitude'},
    {name: 'longitude'}

  ];
  selectedFactory = [];
  paths: any[] = [
    /*{ lat: 29.791629704426924, lng: 47.8624267578125 },
    { lat: 29.974729774949644, lng: 47.6317138671875 },
    { lat: 29.303145259199056, lng: 47.6646728515625 }*/
  ];

  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;

  constructor(private route: ActivatedRoute, private alertService: AlertServiceService,
              private Lookup: LookupService, private ngZone: NgZone, private mapsAPILoader: MapsAPILoader) {

  }

  // google maps zoom level
  zoom: number = 8;

  // initial center position for the map
  lat: number = 29.378586;
  lng: number = 47.990341;
  public searchControl: FormControl;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: MouseEvent) {
    console.log("clicked");
    console.log($event);
    this.paths.push({
      lat: $event['coords'].lat,
      lng: $event['coords'].lng
    });
  }

  onpolyDragEnd($event: MouseEvent) {
    console.log('onpolyDragEnd');
    console.log($event);
    console.log(this.paths);
  }

  onpolyMouseUp($event) {
    console.log($event);
    console.log($event.vertex);
    this.dragLat = $event.latLng.lat();
    this.dragLng = $event.latLng.lng();
    if ($event.vertex === undefined) {
      this.paths.push({
        lat: this.dragLat,
        lng: this.dragLng
      });
    } else {
      this.dragLat = $event.latLng.lat();
      this.dragLng = $event.latLng.lng();
      this.paths[$event.vertex] = {
        lat: this.dragLat,
        lng: this.dragLng
      }
    }

    console.log('onpolyMouseUp');
    this.dragLat = $event.latLng.lat();
    this.dragLng = $event.latLng.lng();
    /*this.paths.push({
      lat: this.dragLat,
      lng: this.dragLng
    });*/
    console.log(this.paths);
  }


  markerDragEndForExistingFactory(m, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
    this.isDirty = true;
    this.selectedFactory[0].latitude = $event['coords']['lat'];
    this.selectedFactory[0].longitude = $event['coords']['lng'];
  }

  markerDragEndForNewFactory(m, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
    this.newFactory.latitude = $event['coords']['lat'];
    this.newFactory.longitude = $event['coords']['lng'];
  }

// <<<<<<< HEAD
//   markers: any[] = [
//     {
//       lat: 29.378586,
//       lng: 47.990341,
//       label: 'A',
//       draggable: true
//     },
//     {
//       lat: 51.373858,
//       lng: 7.215982,
//       label: 'B',
//       draggable: false
//     },
//     {
//       lat: 51.723858,
//       lng: 7.895982,
//       label: 'C',
//       draggable: true
//     }
//   ]
//
//   ngOnInit() {
//     this.GetLookups()
//     this.GetAllFactory();
//     this.searchControl = new FormControl();
// =======
  ngOnInit() {
    this.GetLookups()
    this.GetAllFactory();

    /* var mapProp = {
       center: new google.maps.LatLng(51.508742, -0.120850),
       zoom: 5,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     };
     var map = new google.maps.Map(document.getElementById("gmap"), mapProp);*/

    /* this.drawingManager['initialized$'].subscribe(dm => {
       google.maps.event.addListener(dm, 'overlaycomplete', event => {
         if (event.type !== google.maps.drawing.OverlayType.MARKER) {
           dm.setDrawingMode(null);
           google.maps.event.addListener(event.overlay, 'click', e => {
             this.selectedOverlay = event.overlay;
             this.selectedOverlay.setEditable(true);
           });
           this.selectedOverlay = event.overlay;
         }
       });
     });*/

// >>>>>>> 13561a5420c8a1209994293495095d9e1e8880b8
  }

  ResetInfo() {

    this.GetAllFactory();
    this.editWashingArea = false;
    this.AddWashingArea = false;
    this.selectedFactory = [];
    this.newFactory = {};
    this.firstSubmitTryadd = false;
    this.firstSubmitTry = false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editWashingArea = false;
        this.ResetInfo();
      }
      else {
        this.UpdateWashingArea();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      /* console.log('Valid Form');
       if (!this.isDirty) {
         this.alertService.error('No Changes !');
         this.AddWashingArea = false;
         this.ResetInfo();
       }*/
      console.log(this.paths.length);
      if (this.paths.length > 0) {
        this.AddNewWashingArea();
      } else {
        this.firstSubmitTryadd = true;
        console.log('In Valid Form');

      }
    }
  }

// <<<<<<< HEAD
//   EditFactory() {
//     this.Lookup.EditFactory(this.selectedFactory[0]).subscribe(state => {
// =======
  UpdateWashingArea() {
    this.loading = true;
    this.Lookup.EditFactory(this.selectedFactory[0]).subscribe(state => {
// >>>>>>> 13561a5420c8a1209994293495095d9e1e8880b8
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.loading = false;
          this.alertService.success(' Updated Successfully');
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }

  AddNewWashingArea() {
    this.loading = true;
    this.paths.push(this.paths[0]);
    this.WashingArea = {
      polygon: this.paths
    };

// <<<<<<< HEAD
//     this.Lookup.AddFactory(this.newFactory).subscribe(Factory => {
//         console.log(Factory);
//         if (Factory) {
// =======
    console.log('post wash');
    console.log(this.WashingArea);

    this.Lookup.AddWashingArea(this.WashingArea).subscribe(wash => {
        console.log(wash);
        if (wash) {
// >>>>>>> 13561a5420c8a1209994293495095d9e1e8880b8
          this.ResetInfo();
          this.loading = false;
          this.alertService.success(' added Successfully')
        }
        else {
          this.loading = false;
          this.alertService.error('failed To add')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }


  //----------------- Complains Functions -----------------//

  DeleteAllFactory() {

    // for(var i=0;i<this.selectedFactory.length;i++){
    this.Lookup.DeleteFactory(this.selectedFactory[0].id).subscribe(state => {
        this.alertService.success(this.selectedFactory[0].name + ' Deleted Successfully');
        this.ResetInfo();
      },
      err => {
        this.alertService.error('This Item is already Used !!');
        this.ResetInfo();
      });
    // }
  }

  GetAllFactory() {
// <<<<<<< HEAD
//     this.Lookup.GetallFactories().subscribe(Factory => {
// =======
    this.Lookup.GetallWashingAreas().subscribe(Washing => {
// >>>>>>> 13561a5420c8a1209994293495095d9e1e8880b8

        console.log('washings');
        console.log(Washing);
        this.AreaInfo = Washing;
      },
      err => {
        this.alertService.error('your session has timed out please login again !')
      });
  }

  GetLookups() {
    this.loading = true;
    this.Lookup.GetallLocations().subscribe(data => {
        console.log(data);
        this.LocationLookup = data;
        this.loading = false;
      },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !')
      });
  }

  getAddress(place: Object) {
    var location = place['geometry']['location'];
    var lat = location.lat();
    var lng = location.lng();
    console.log("Address Object", place);
  }

// <<<<<<< HEAD
//
//   openAddFactoryPanel() {
//     this.AddFactory = true;
//
// =======
  openAddWashingAreaPanel() {
    this.AddWashingArea = true;
// >>>>>>> 13561a5420c8a1209994293495095d9e1e8880b8
  }

}
