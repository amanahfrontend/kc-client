import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { allRolesLookup } from './../../../api-module/models/lookups-modal';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css','./../../../customer/existing-customer/existing-customer.component.css']
})
export class RoleComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd=false;
  public isDirty: boolean = false;
  public RoleInfo: Array<allRolesLookup>;
  public newRole: allRolesLookup={};
  public Role:allRolesLookup;
 

  public editRole: boolean = false;
  public AddRole:boolean=false;

  RoleColumns: any[] = [
    { name: 'id' },
    { name: 'name' }

  ];
  selectedRole= [];
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
   
  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup:LookupService) { }
  
  ngOnInit() {
   
    this.GetAllRole();
  }


  ResetInfo() {

    this.GetAllRole();
    this.editRole= false;
    this.AddRole=false;
    this.selectedRole = [];
    this.newRole={};
    this.firstSubmitTryadd=false;
    this.firstSubmitTry=false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editRole = false;
        this.ResetInfo();
      }
      else {
        this.EditRole();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddRole = false;
        this.ResetInfo();
      }
      else {
        this.AddnewRole();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  EditRole() {
    this.loading = true;
    this.Lookup.EditRole(this.selectedRole[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }

  AddnewRole() {
    this.loading = true;
    this.Lookup.AddRole(this.newRole).
      subscribe(Role => {
        console.log(Role);
        if (Role) {
          this.ResetInfo();          
          this.alertService.success(' Updated Successfully')
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }
    

    //----------------- Complains Functions -----------------

    DeleteAllRole() {

      // for(var i=0;i<this.selectedRole.length;i++){
        this.Lookup.DeleteRole(this.selectedRole[0].id).
        subscribe(state => {
          this.alertService.success(this.selectedRole[0].name +' Deleted Successfully');
          this.ResetInfo();
        },
        err => {
          this.alertService.error('This Item is already Used !!');
          this.ResetInfo();
          
        });
      // }
    }

    GetAllRole(){
      this.loading = true;
      this.Lookup.GetallRole().
      subscribe(Role => {
  
        console.log(Role);
        this.RoleInfo=Role;
        this.loading = false;
        },
        err => {    
          this.loading = false;          
          this.alertService.error('your session has timed out please login again !')        
        });
    }
  }
