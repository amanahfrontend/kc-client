import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-vehicle-type',
  templateUrl: './vehicle-type.component.html',
  styleUrls: ['./vehicle-type.component.css', './../../../customer/existing-customer/existing-customer.component.css']
})
export class VehicleTypeComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd = false;
  public isDirty: boolean = false;
  public VehicleTypeInfo: Array<any>;
  public newVehicleType: any = {};
  public VehicleType: any;
  public factories: any;
  @ViewChild('myTable') table: any;

  public editVehicleType: boolean = false;
  public AddVehicleType: boolean = false;

  VehicleTypeColumns: any[] = [
    { name: 'id' },
    { name: 'number' }

  ];
  selectedVehicleType = [];
  allJobs: any[] = [];
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
  currentUser: any;
  showData: boolean = true;
  vechileNumber: string;
  allVehicles: any;
  showjobExistAlert: boolean;
  cancelJobModal: boolean;

  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup: LookupService) { }

  ngOnInit() {

    this.GetAllVehicleType();
    this.getAllFactories();


    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser.roles[0].toLowerCase() == 'dispatcher') {
      this.showData = false;
    }
  }



  ResetInfo() {

    this.GetAllVehicleType();
    this.editVehicleType = false;
    this.AddVehicleType = false;
    this.selectedVehicleType = [];
    this.newVehicleType = {};
    this.firstSubmitTryadd = false;
    this.firstSubmitTry = false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      this.EditVehicleType();

    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  seacrhByVechileNumber() {

    this.VehicleTypeInfo = JSON.parse(JSON.stringify(this.allVehicles));

    this.VehicleTypeInfo = this.VehicleTypeInfo.filter(x => x.number == this.vechileNumber);
   
    this.table.offset = 0;

  }
  Reset() {
    this.vechileNumber = undefined;
    if (this.vechileNumber == undefined || this.vechileNumber.length == 0) {
      this.VehicleTypeInfo = JSON.parse(JSON.stringify(this.allVehicles));
    }
    this.table.offset = 0;

  }

  GetVehicleJobs() {

    this.loading = false;


    this.Lookup.GetVehicleJobs(this.selectedVehicleType[0].id).subscribe(Vehicles => {
      this.allJobs = Vehicles;

      this.editVehicleType = true


      this.loading = false;
    },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });


  }

  handleChange(e: any) {


    if (e.checked && this.allJobs[0].fK_Status_Id != 7) {
      this.alertService.error('The vechile currently has a job, please change the job status into done.');
      this.editVehicleType = false;
      this.selectedVehicleType[0].isAvailable = false;
      this.GetAllVehicleType();
      return false;
    }

    

  }
  confirmCancelJob() {
    this.cancelJobModal = false;

  }
  cancelSidemodal() {
    this.cancelJobModal = false;
    this.editVehicleType = false;
    this.AddVehicleType = false;
    this.ResetInfo();


  }
  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddVehicleType = false;
        this.ResetInfo();
      }
      else {
        this.AddnewVehicleType();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');
    }
  }

  EditVehicleType() {
    this.loading = true;
    this.Lookup.EditVehicleType(this.selectedVehicleType[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.loading = false;
          this.alertService.success(' Updated Successfully');
          this.GetAllVehicleType();
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
        err => {
          this.loading = false;
          this.alertService.error('we have Error !!')
        });
  }

  AddnewVehicleType() {
    this.loading = false;
    this.Lookup.AddVehicleType(this.newVehicleType).
      subscribe(VehicleType => {
        console.log(VehicleType);
        if (VehicleType) {
          this.ResetInfo();
          this.loading = false;
          this.alertService.success(' Updated Successfully')
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
        err => {
          this.loading = false;
          this.alertService.error('we have Error !!')
        });
  }


  //----------------- Complains Functions -----------------

  DeleteAllVehicleType() {

    // for(var i=0;i<this.selectedContractType.length;i++){
    this.Lookup.DeleteVehicleType(this.selectedVehicleType[0].id).
      subscribe(state => {
        this.alertService.success(this.selectedVehicleType[0].number + ' Deleted Successfully');
        this.ResetInfo();
      },
        err => {
          this.alertService.error('This Item is already Used !!');
          this.ResetInfo();
        });
    // }
  }

  getAllFactories() {
    this.loading = true;
    this.Lookup.GetallFactories().subscribe(factories => {
      this.factories = factories;
      this.loading = false;
    });
  }

  GetAllVehicleType() {
    this.loading = true;
    this.Lookup.GetAllVehicleTypes().
      subscribe(VehicleType => {

        console.log(VehicleType);
        this.VehicleTypeInfo = VehicleType;
        this.allVehicles = JSON.parse(JSON.stringify(this.VehicleTypeInfo));

        this.loading = false;
      },
        err => {
          this.loading = false;
          this.alertService.error('your session has timed out please login again !')
        });
  }
}
