import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { allEquipmentTypeLookup } from './../../../api-module/models/lookups-modal';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-equipment-type',
  templateUrl: './equipment-type.component.html',
  styleUrls: ['./equipment-type.component.css','./../../../customer/existing-customer/existing-customer.component.css']
})
export class EquipmentTypeComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd=false;
  public isDirty: boolean = false;
  public EquipmentTypeInfo: Array<allEquipmentTypeLookup>;
  public newEquipmentType: allEquipmentTypeLookup={};
  public EquipmentType:allEquipmentTypeLookup;
 

  public editEquipmentType: boolean = false;
  public AddEquipmentType:boolean=false;

  EquipmentTypeColumns: any[] = [
    { name: 'id' },
    { name: 'name' }

  ];
  selectedEquipmentType= [];
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
   
  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup:LookupService) { }
  
  ngOnInit() {
   
    this.GetAllEquipmentType();
  }


  ResetInfo() {

    this.GetAllEquipmentType();
    this.editEquipmentType= false;
    this.AddEquipmentType=false;
    this.selectedEquipmentType = [];
    this.newEquipmentType={};
    this.firstSubmitTryadd=false;
    this.firstSubmitTry=false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editEquipmentType = false;
        this.ResetInfo();
      }
      else {
        this.EditEquipmentType();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddEquipmentType = false;
        this.ResetInfo();
      }
      else {
        this.AddnewEquipmentType();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  EditEquipmentType() {
    this.Lookup.EditEquipmentType(this.selectedEquipmentType[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
        }
        else {
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.alertService.error('we have Error !!')
      });
  }

  AddnewEquipmentType() {

    this.Lookup.AddEquipmentType(this.newEquipmentType).
      subscribe(EquipmentType => {
        console.log(EquipmentType);
        if (EquipmentType) {
          this.ResetInfo();          
          this.alertService.success(' Updated Successfully')
        }
        else {
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.alertService.error('we have Error !!')
      });
  }
    

    //----------------- Complains Functions -----------------

    DeleteAllEquipmentType() {

      // for(var i=0;i<this.selectedEquipmentType.length;i++){
        this.Lookup.DeleteEquipmentType(this.selectedEquipmentType[0].id).
        subscribe(state => {
          this.alertService.success(this.selectedEquipmentType[0].name +' Deleted Successfully');
          this.ResetInfo();
        },
        err => {
          this.alertService.error('This Item is already Used !!');
          this.ResetInfo();
        });
      // }
    }

    GetAllEquipmentType(){
      this.loading = true;
      this.Lookup.GetallEquipmentType().
      subscribe(EquipmentType => {
  
        console.log(EquipmentType);
        this.EquipmentTypeInfo=EquipmentType;
        this.loading = false;
        },
        err => {              
          this.loading = false;
          this.alertService.error('your session has timed out please login again !')        
        });
    }
  }
