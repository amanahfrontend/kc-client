import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { allPriorityLookup } from './../../../api-module/models/lookups-modal';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-priority',
  templateUrl: './priority.component.html',
  styleUrls: ['./priority.component.css','./../../../customer/existing-customer/existing-customer.component.css']
})
export class PriorityComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd=false;
  public isDirty: boolean = false;
  public PriorityInfo: Array<allPriorityLookup>;
  public newPriority: allPriorityLookup={};
  public Priority:allPriorityLookup;
 

  public editPriority: boolean = false;
  public AddPriority:boolean=false;

  PriorityColumns: any[] = [
    { name: 'id' },
    { name: 'name' }

  ];
  selectedPriority= [];
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
   
  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup:LookupService) { }
  
  ngOnInit() {
   
    this.GetAllPriority();
  }


  ResetInfo() {

    this.GetAllPriority();
    this.editPriority= false;
    this.AddPriority=false;
    this.selectedPriority = [];
    this.newPriority={};
    this.firstSubmitTryadd=false;
    this.firstSubmitTry=false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editPriority = false;
        this.ResetInfo();
      }
      else {
        this.EditPriority();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddPriority = false;
        this.ResetInfo();
      }
      else {
        this.AddnewPriority();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  EditPriority() {
    this.loading = true;
    this.Lookup.EditPeriorities(this.selectedPriority[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }

  AddnewPriority() {
    this.loading = true;
    this.Lookup.AddPeriorities(this.newPriority).
      subscribe(Priority => {
        console.log(Priority);
        if (Priority) {
          this.ResetInfo();          
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }
    

    //----------------- Complains Functions -----------------

    DeleteAllPriority() {

      // for(var i=0;i<this.selectedPriority.length;i++){
        this.Lookup.DeletePeriorities(this.selectedPriority[0].id).
        subscribe(state => {
          this.alertService.success(this.selectedPriority[0].name +' Deleted Successfully');
          this.ResetInfo();
        },
        err => {
          this.alertService.error('This Item is already Used !!');
          this.ResetInfo();
        });
      // }
    }

    GetAllPriority(){
      this.loading = true;
      this.Lookup.GetallPeriorities().
      subscribe(Priority => {
        console.log(Priority);
        this.PriorityInfo=Priority;
        this.loading = false;
        },
        err => {     
          this.loading = false;         
          this.alertService.error('your session has timed out please login again !')        
        });
    }
  }
