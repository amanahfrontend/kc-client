import {LookupService} from './../../../api-module/services/lookup-services/lookup.service';
import {allCustomerCategoryLookup} from './../../../api-module/models/lookups-modal';
import {ActivatedRoute} from '@angular/router';
import {AlertServiceService} from './../../../api-module/services/alertservice/alert-service.service';
import {Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-customer-category',
  templateUrl: './customer-category.component.html',
  styleUrls: ['./customer-category.component.css', './../customer-category/customer-category.component.css']
})
export class CustomerCategoryComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd = false;
  public isDirty: boolean = false;
  public CustomerCategoryInfo: Array<allCustomerCategoryLookup>;
  public newCustomerCategory: allCustomerCategoryLookup = {};
  public CustomerCategory: allCustomerCategoryLookup;
  public editCustomerCategory: boolean = false;
  public addNewCustomerCategory: boolean = false;

  CustomerCategoryColumns: any[] = [
    {name: 'id'},
    {name: 'name'}
  ];
  selectedCustomerCategory = [];
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;

  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup: LookupService) {
  }

  ngOnInit() {
    this.GetAllCustomerCategory();
  }


  ResetInfo() {
    this.GetAllCustomerCategory();
    this.editCustomerCategory = false;
    this.addNewCustomerCategory = false;
    this.selectedCustomerCategory = [];
    this.newCustomerCategory = {};
    this.firstSubmitTryadd = false;
    this.firstSubmitTry = false;
  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editCustomerCategory = false;
        this.ResetInfo();
      }
      else {
        this.EditCustomerCategory();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.addNewCustomerCategory = false;
        this.ResetInfo();
      }
      else {
        this.AddCustomerCategory();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  EditCustomerCategory() {
    this.loading = true;
    this.Lookup.EditCustomerCategory(this.selectedCustomerCategory[0]).subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update');
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!');
      });
  }

  AddCustomerCategory() {
    this.loading = true;
    this.Lookup.AddCustomerCategory(this.newCustomerCategory).subscribe(CustomerCategory => {
        console.log(CustomerCategory);
        if (CustomerCategory) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update');
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!');
      });
  }


  //----------------- Complains Functions -----------------//

  DeleteAllCustomerCategory() {

    // for(var i=0;i<this.selectedCustomerCategory.length;i++){
    this.Lookup.DeleteCustomerCategory(this.selectedCustomerCategory[0].id).subscribe(state => {
        this.alertService.success(this.selectedCustomerCategory[0].name + ' Deleted Successfully');
        this.ResetInfo();
      },
      err => {
        this.alertService.error('This Item is already Used !!');
        this.ResetInfo();
      });
    // }
  }

  GetAllCustomerCategory() {
    this.loading = true;
    this.Lookup.GetAllCustomerCategories().subscribe(CustomerCategory => {

        console.log(CustomerCategory);
        this.CustomerCategoryInfo = CustomerCategory;
        this.loading = false;
      },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !');
      });
  }
}
