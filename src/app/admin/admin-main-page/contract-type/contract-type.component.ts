import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { allContractTypeLookup } from './../../../api-module/models/lookups-modal';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-contract-type',
  templateUrl: './contract-type.component.html',
  styleUrls: ['./contract-type.component.css','./../../../customer/existing-customer/existing-customer.component.css']
})
export class ContractTypeComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd=false;
  public isDirty: boolean = false;
  public ContractTypeInfo: Array<allContractTypeLookup>;
  public newContractType: allContractTypeLookup={};
  public ContractType:allContractTypeLookup;
 

  public editContractType: boolean = false;
  public AddContractType:boolean=false;

  ContractTypeColumns: any[] = [
    { name: 'id' },
    { name: 'name' }

  ];
  selectedContractType= [];
  
   
  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup:LookupService) { }
  
  ngOnInit() {
   
    this.GetAllContractType();
  }


  ResetInfo() {

    this.GetAllContractType();
    this.editContractType= false;
    this.AddContractType=false;
    this.selectedContractType = [];
    this.newContractType={};
    this.firstSubmitTryadd=false;
    this.firstSubmitTry=false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editContractType = false;
        this.ResetInfo();
      }
      else {
        this.EditContractType();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddContractType = false;
        this.ResetInfo();
      }
      else {
        this.AddnewContractType();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  EditContractType() {
    this.Lookup.EditContractType(this.selectedContractType[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
        }
        else {
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.alertService.error('we have Error !!')
      });
  }

  AddnewContractType() {

    this.Lookup.AddContractType(this.newContractType).
      subscribe(ContractType => {
        console.log(ContractType);
        if (ContractType) {
          this.ResetInfo();          
          this.alertService.success(' Updated Successfully')
        }
        else {
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.alertService.error('we have Error !!')
      });
  }
    

    //----------------- Complains Functions -----------------

    DeleteAllContractType() {

      // for(var i=0;i<this.selectedContractType.length;i++){
        this.Lookup.DeleteContractType(this.selectedContractType[0].id).
        subscribe(state => {
          this.alertService.success(this.selectedContractType[0].name +' Deleted Successfully');
          this.ResetInfo();
        },
        err => {
          this.alertService.error('This Item is already Used !!');
          this.ResetInfo();
        });
      // }
    }

    GetAllContractType(){
      this.Lookup.GetAllContractTypes().
      subscribe(ContractType => {
  
        console.log(ContractType);
        this.ContractTypeInfo=ContractType;
        },
        err => {              
          this.alertService.error('your session has timed out please login again !')        
        });
    }
  }
