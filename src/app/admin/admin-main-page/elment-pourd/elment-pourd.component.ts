import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { allEquipmentTypeLookup } from './../../../api-module/models/lookups-modal';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-elment-poured',
    templateUrl: './elment-pourd.component.html',
    styleUrls: ['./elment-pourd.component.css','./../../../customer/existing-customer/existing-customer.component.css']
})
export class PouredElementComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd=false;
  public isDirty: boolean = false;
    public allItemsToPoured: any[] = [];
 

  public editElement: boolean = false;
  public AddElement:boolean=false;

  EquipmentTypeColumns: any[] = [
    { name: 'id' },
    { name: 'name' }

  ];
  selectedElementToPoured= [];
  public loadingImage: string = "assets/Images/loading.gif"
    loading = false;
    newElementToPoured: any = {};
   
  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup:LookupService) { }
  
  ngOnInit() {

      this.GetAllItemPoured();
  }


  ResetInfo() {

      this.GetAllItemPoured();
      this.editElement = false;
      this.AddElement = false;
      this.selectedElementToPoured = [];
    this.newElementToPoured={};
    this.firstSubmitTryadd=false;
    this.firstSubmitTry=false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
          this.alertService.error('No Changes !');
          this.editElement = false;
        this.ResetInfo();
      }
      else {
          this.EditElementToBePoured();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
          this.AddElement = false;
        this.ResetInfo();
      }
      else {
          this.AddElementToBePoured();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

    EditElementToBePoured() {
        this.Lookup.EditElementToBePoured(this.selectedElementToPoured[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
        }
        else {
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.alertService.error('we have Error !!')
      });
  }

    AddElementToBePoured() {

      this.Lookup.AddElementToBePoured(this.newElementToPoured).
      subscribe(elementPoured => {
          console.log(elementPoured);
          if (elementPoured) {
          this.ResetInfo();          
          this.alertService.success(' Updated Successfully')
        }
        else {
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.alertService.error('we have Error !!')
      });
  }
    

    //----------------- Complains Functions -----------------

    DeleteElementToBePoured() {

      // for(var i=0;i<this.selectedEquipmentType.length;i++){
        this.Lookup.DeleteElementToBePoured(this.selectedElementToPoured[0].id).
        subscribe(state => {
            this.alertService.success(this.selectedElementToPoured[0].name +' Deleted Successfully');
          this.ResetInfo();
        },
        err => {
          this.alertService.error('This Item is already Used !!');
          this.ResetInfo();
        });
      // }
    }

    GetAllItemPoured(){
        this.loading = true;
        this.Lookup.GetAllItemPoured().
            subscribe(ItemPoured => {
  

                this.allItemsToPoured = ItemPoured;
        this.loading = false;
        },
        err => {              
          this.loading = false;
          this.alertService.error('error')        
        });
    }
  }
