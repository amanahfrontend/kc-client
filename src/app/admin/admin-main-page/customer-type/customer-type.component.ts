import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { allCustomerTypeLookup } from './../../../api-module/models/lookups-modal';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-customer-type',
  templateUrl: './customer-type.component.html',
  styleUrls: ['./customer-type.component.css','./../../../customer/existing-customer/existing-customer.component.css']
})
export class CustomerTypeComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd=false;
  public isDirty: boolean = false;
  public CustomerTypeInfo: Array<allCustomerTypeLookup>;
  public newCustomerType: allCustomerTypeLookup={};
  public CustomerType:allCustomerTypeLookup;
 

  public editCustomerType: boolean = false;
  public AddCustomerType:boolean=false;

  CustomerTypeColumns: any[] = [
    { name: 'id' },
    { name: 'name' }

  ];
  selectedCustomerType= [];
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
   
  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup:LookupService) { }
  
  ngOnInit() {
   
    this.GetAllCustomerType();
  }


  ResetInfo() {

    this.GetAllCustomerType();
    this.editCustomerType= false;
    this.AddCustomerType=false;
    this.selectedCustomerType = [];
    this.newCustomerType={};
    this.firstSubmitTryadd=false;
    this.firstSubmitTry=false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editCustomerType = false;
        this.ResetInfo();
      }
      else {
        this.EditCustomerType();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddCustomerType = false;
        this.ResetInfo();
      }
      else {
        this.AddnewCustomerType();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  EditCustomerType() {
    this.loading = true;
    this.Lookup.EditCustomerType(this.selectedCustomerType[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }

  AddnewCustomerType() {
    this.loading = true;
    this.Lookup.AddCustomerType(this.newCustomerType).
      subscribe(CustomerType => {
        console.log(CustomerType);
        if (CustomerType) {
          this.ResetInfo();          
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }
    

    //----------------- Complains Functions -----------------

    DeleteAllCustomerType() {

      // for(var i=0;i<this.selectedCustomerType.length;i++){
        this.Lookup.DeleteCustomerType(this.selectedCustomerType[0].id).
        subscribe(state => {
          this.alertService.success(this.selectedCustomerType[0].name +' Deleted Successfully');
          this.ResetInfo();
        },
        err => {
          this.alertService.error('This Item is already Used !!');
          this.ResetInfo();
        });
      // }
    }

    GetAllCustomerType(){
      this.loading = true;
      this.Lookup.GetAllCustomerTypes().
      subscribe(CustomerType => {
  
        console.log(CustomerType);
        this.CustomerTypeInfo=CustomerType;
        this.loading = false;
        },
        err => {   
          this.loading = false;           
          this.alertService.error('your session has timed out please login again !')        
        });
    }
  }
