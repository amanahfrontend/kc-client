import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { allStatusLookup } from './../../../api-module/models/lookups-modal';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css','./../../../customer/existing-customer/existing-customer.component.css']
})
export class StatusComponent implements OnInit {
    
  public firstSubmitTry = false;
  public firstSubmitTryadd=false;
  public isDirty: boolean = false;
  public StatusInfo: Array<allStatusLookup>;
  public newStatus: allStatusLookup={};
  public Status:allStatusLookup;
 

  public editStatus: boolean = false;
  public AddStatus:boolean=false;

  StatusColumns: any[] = [
    { name: 'id' },
    { name: 'name' }

  ];
  selectedStatus= [];
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
   
  constructor(private route: ActivatedRoute, private alertService: AlertServiceService, private Lookup:LookupService) { }
  
  ngOnInit() {
   
    this.GetAllStatus();
  }


  ResetInfo() {

    this.GetAllStatus();
    this.editStatus= false;
    this.AddStatus=false;
    this.selectedStatus = [];
    this.newStatus={};
    this.firstSubmitTryadd=false;
    this.firstSubmitTry=false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editStatus = false;
        this.ResetInfo();
      }
      else {
        this.EditStatus();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddStatus = false;
        this.ResetInfo();
      }
      else {
        this.AddnewStatus();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  EditStatus() {
    this.loading = true;
    this.Lookup.EditStatus(this.selectedStatus[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }

  AddnewStatus() {
    this.loading = true;
    this.Lookup.AddStatus(this.newStatus).
      subscribe(Status => {
        console.log(Status);
        if (Status) {
          this.ResetInfo();          
          this.alertService.success(' Updated Successfully')
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }
    

    //----------------- Complains Functions -----------------

    DeleteAllStatus() {

      // for(var i=0;i<this.selectedStatus.length;i++){
        this.Lookup.DeleteStatus(this.selectedStatus[0].id).
        subscribe(state => {
          this.alertService.success(this.selectedStatus[0].name +' Deleted Successfully');
          this.ResetInfo();
        },
        err => {
          this.alertService.error('This Item is already Used !!');
          this.ResetInfo();
          
        });
      // }
    }

    GetAllStatus(){
      this.loading = true;
      this.Lookup.GetallStatus().
      subscribe(status => {
  
        console.log(status);
        this.StatusInfo=status;
        this.loading = false;
        },
        err => {  
          this.loading = false;            
          this.alertService.error('your session has timed out please login again !')        
        });
    }
  }
