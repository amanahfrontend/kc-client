declare var google: any;

import { LookupService } from './../../../api-module/services/lookup-services/lookup.service';
import { allFactoryLookup } from './../../../api-module/models/lookups-modal';
import { ActivatedRoute } from '@angular/router';
import { AlertServiceService } from './../../../api-module/services/alertservice/alert-service.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter, ElementRef, NgZone ,ViewEncapsulation} from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
// import { asNativeElements } from '@angular/core/src/debug/debug_node';

@Component({
  selector: 'app-factory',
  templateUrl: './factory.component.html',
  styleUrls: ['./factory.component.css', './../../../customer/existing-customer/existing-customer.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class FactoryComponent implements OnInit {

  public firstSubmitTry = false;
  public firstSubmitTryadd = false;
  public isDirty: boolean = false;
  public FactoryInfo: Array<allFactoryLookup>;
  public newFactory: allFactoryLookup = {};
  public Factory: allFactoryLookup;
  public LocationLookup: any;

  @ViewChild("search") searchElementRef: ElementRef;

  public editFactory: boolean = false;
  public AddFactory: boolean = false;
  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;

  FactoryColumns: any[] = [
    { name: 'id' },
    { name: 'name' },
    { name: 'fK_Location_Id' },
    { name: 'latitude' },
    { name: 'longitude' }

  ];
  selectedFactory = [];


  constructor(private route: ActivatedRoute, private alertService: AlertServiceService,
    private Lookup: LookupService, private ngZone: NgZone, private mapsAPILoader: MapsAPILoader) {

  }

  // google maps zoom level
  zoom: number = 8;

  // initial center position for the map
  lat: number = 29.378586;
  lng: number = 47.990341;
  public searchControl: FormControl;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event['coords'].lat,
      lng: $event['coords'].lng
    });
  }


  markerDragEndForExistingFactory(m, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
    this.isDirty = true;
    this.selectedFactory[0].latitude = $event['coords']['lat'];
    this.selectedFactory[0].longitude = $event['coords']['lng'];
  }

  markerDragEndForNewFactory(m, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
    this.newFactory.latitude = $event['coords']['lat'];
    this.newFactory.longitude = $event['coords']['lng'];
  }

  markers: any[] = [
    {
      lat: 29.378586,
      lng: 47.990341,
      label: 'A',
      draggable: true
    },
    {
      lat: 51.373858,
      lng: 7.215982,
      label: 'B',
      draggable: false
    },
    {
      lat: 51.723858,
      lng: 7.895982,
      label: 'C',
      draggable: true
    }
  ]
  ngOnInit() {
    this.GetLookups()
    this.GetAllFactory();
    this.searchControl = new FormControl();    
  }

  ResetInfo() {

    this.GetAllFactory();
    this.editFactory = false;
    this.AddFactory = false;
    this.selectedFactory = [];
    this.newFactory = {};
    this.firstSubmitTryadd = false;
    this.firstSubmitTry = false;

  }

  onSubmit(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.editFactory = false;
        this.ResetInfo();
      }
      else {
        this.EditFactory();
      }
    } else {
      this.firstSubmitTry = true;
      console.log('In Valid Form');

    }
  }

  onAdd(form) {
    if (form.valid) {
      console.log('Valid Form');
      if (!this.isDirty) {
        this.alertService.error('No Changes !');
        this.AddFactory = false;
        this.ResetInfo();
      }
      else {
        this.AddnewFactory();
      }
    } else {
      this.firstSubmitTryadd = true;
      console.log('In Valid Form');

    }
  }

  EditFactory() {
    this.loading = true;
    this.Lookup.EditFactory(this.selectedFactory[0]).
      subscribe(state => {
        console.log(state);
        if (state) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully');
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }

  AddnewFactory() {
    this.loading = true;
    this.Lookup.AddFactory(this.newFactory).
      subscribe(Factory => {
        console.log(Factory);
        if (Factory) {
          this.ResetInfo();
          this.alertService.success(' Updated Successfully')
          this.loading = false;
        }
        else {
          this.loading = false;
          this.alertService.error('failed To update')
        }
      },
      err => {
        this.loading = false;
        this.alertService.error('we have Error !!')
      });
  }


  //----------------- Complains Functions -----------------

  DeleteAllFactory() {

    // for(var i=0;i<this.selectedFactory.length;i++){
    this.Lookup.DeleteFactory(this.selectedFactory[0].id).
      subscribe(state => {
        this.alertService.success(this.selectedFactory[0].name + ' Deleted Successfully');
        this.ResetInfo();
      },
      err => {
        this.alertService.error('This Item is already Used !!');
        this.ResetInfo();
      });
    // }
  }

  GetAllFactory() {
    this.loading = true;
    this.Lookup.GetallFactories().
      subscribe(Factory => {

        console.log(Factory);
        this.FactoryInfo = Factory;
        this.loading = false;
      },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !')
      });
  }

  GetLookups() {
    this.loading = true;
    this.Lookup.GetallLocations().
      subscribe(data => {
        console.log(data);
        this.LocationLookup = data;
        this.loading = false;
      },
      err => {
        this.loading = false;
        this.alertService.error('your session has timed out please login again !')
      });
  }

  getAddress(place: Object) {
    var location = place['geometry']['location'];
    var lat = location.lat();
    var lng = location.lng();
    console.log("Address Object", place);
  }

  openAddFactoryPanel() {
    this.AddFactory = true;
    
  }
}
