import {AuthGuardGuard} from './../api-module/guards/auth-guard.guard';
import {RouterModule, Routes} from '@angular/router';
import {SharedModuleModule} from './../shared-module/shared-module.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminMainPageComponent} from './admin-main-page/admin-main-page.component';
import {StatusComponent} from './admin-main-page/status/status.component';
import {RoleComponent} from './admin-main-page/role/role.component';
import {UserComponent} from './admin-main-page/user/user.component';
import {PriorityComponent} from './admin-main-page/priority/priority.component';
import {CustomerTypeComponent} from './admin-main-page/customer-type/customer-type.component';
import {ContractTypeComponent} from './admin-main-page/contract-type/contract-type.component';
import {EquipmentComponent} from './admin-main-page/equipment/equipment.component';
import {EquipmentTypeComponent} from './admin-main-page/equipment-type/equipment-type.component';
import {ItemComponent} from './admin-main-page/item/item.component';
import {FactoryComponent} from './admin-main-page/factory/factory.component';
import {VehicleTypeComponent} from './admin-main-page/vehicle/vehicle-type.component';
import {WashingComponent} from './admin-main-page/washing/washing.component';
import {FactoryUserComponent} from './admin-main-page/FactoryUsers/factory-user.component';
import {AgmCoreModule, GoogleMapsAPIWrapper} from '@agm/core'
import * as config from '../api-module/services/globalPath';
import {CustomerCategoryComponent} from './admin-main-page/customer-category/customer-category.component';
import { PouredElementComponent } from './admin-main-page/elment-pourd/elment-pourd.component';
import { ApplicationConfigurationComponent } from './admin-main-page/application-configuration/application-configuration';

const routes: Routes = [
  {
    path: '', component: AdminMainPageComponent, canActivate: [AuthGuardGuard], data: {roles: ['Admin']},

    children: [
      {path: 'Status', component: StatusComponent},
      {path: 'User', component: UserComponent},
      {path: 'Role', component: RoleComponent},
      {path: 'Priority', component: PriorityComponent},
      {path: 'Item', component: ItemComponent},
      {path: 'Factory', component: FactoryComponent},
      {path: 'EquipmentType', component: EquipmentTypeComponent},
      {path: 'Equipment', component: EquipmentComponent},
      {path: 'CustomerType', component: CustomerTypeComponent},
      {path: 'CustomerCategory', component: CustomerCategoryComponent},
      {path: 'ContractType', component: ContractTypeComponent},
      {path: 'FactoryVehicles', component: VehicleTypeComponent},
      {path: 'Washing', component: WashingComponent},
      {path: 'FactoryUser', component: FactoryUserComponent},
      { path: 'PouredElement', component: PouredElementComponent },
      { path: 'ApplicationConfiguration', component: ApplicationConfigurationComponent },


      {path: '', redirectTo: 'Status'},
    ]
  }
];


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot(
      {
        apiKey: config.googlMapsAPiKey,
        libraries: ["places"]
      }
    ),
    SharedModuleModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ], providers: [GoogleMapsAPIWrapper],
  declarations: [AdminMainPageComponent, StatusComponent, RoleComponent, UserComponent, PriorityComponent, CustomerTypeComponent, CustomerCategoryComponent, ContractTypeComponent, EquipmentComponent, EquipmentTypeComponent, ItemComponent, FactoryComponent, WashingComponent, FactoryUserComponent, PouredElementComponent, ApplicationConfigurationComponent]
})
export class AdminModule {
}
