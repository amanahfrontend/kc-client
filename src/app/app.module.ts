import {SharedModuleModule} from './shared-module/shared-module.module';
import {RoutingModule, RoutingComponents} from './router.module';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpModule, BaseRequestOptions} from '@angular/http';
import {SpinnerModule} from 'angular2-spinner/src/';
import {SignalRModule} from 'ng2-signalr';
import {SignalRConfiguration} from 'ng2-signalr';
// import { LoadingModule } from 'ngx-loading';

// used to create fake backend

// import { MockBackend, MockConnection } from '@angular/http/testing';
let token = '';

// export function createConfig(): SignalRConfiguration {
//   const c = new SignalRConfiguration();
//   c.hubName = 'Ng2SignalRHub';
//   c.qs = { authorization: token };
//   c.url = 'http://localhost:50301/dispatchingHub';
//   c.logging = true;
//   return c;
// }


import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from "@angular/common/http";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

import {LocationStrategy, HashLocationStrategy} from '@angular/common'
import {UtilitiesService} from "./operations/utilities.service";
import {DateFormatPipe } from './pipes/filter.pipe';  


@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents,DateFormatPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    SpinnerModule,
    SharedModuleModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    // LoadingModule
    // SignalRModule.forRoot(createConfig)
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
      BaseRequestOptions, UtilitiesService,DateFormatPipe],
  bootstrap: [AppComponent],

})
export class AppModule {
}
