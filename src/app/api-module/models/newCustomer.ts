export interface newCustomer {

}
export interface newDetailsCustomer {
    customer?: newcustomer;
    contract?:newcontract;
    // locations?:newLocation;
}

export interface newcustomer {
    name?:string;
    civilId?:string;
    mobile1?:string;
    mobile2?: string;
    phones?: string[];
    remarks?: string;
    companyName?:string;
    division?:string;
    fK_CustomerType_Id?:string;
  fK_CustomerCategory_Id?:string;
  kcrM_Customer_Id?:string;
    fK_Location_Id?:string;

}

export interface newcontract {
    contractNumber?:string;
    startDate?:string;
    endDate?:string;
    amount?:string;
    price?:string;
    fK_ContractType_Id?:string;
    remarks?: string;
    fileName?:string;
    file?:string;

}
export interface newLocation {
    PACINumber?:string;
    governorate?:string;
    area?:string;
    block?:string;
    street?:string;
    title?: string;
    addressNote?:string;
}
export interface equipmentInFactory {
    id?:number;
    number?:string;
    name?:string;
    fK_EquipmentType_Id?: string;
    equipmentType?: string;
    fk_Factory_Id?: string;
    factory?: string;
    fK_WorkOrder_Id?: string;
    workOrder?: string;
    isAvailable?: string;
    fK_CreatedBy_Id?:string;
    fK_UpdatedBy_Id?: string;
    isDeleted?: string;
    createdDate?: string;
    updatedDate?: string;
    currentUser?: string;
}
