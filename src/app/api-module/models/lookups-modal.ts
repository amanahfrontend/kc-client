export interface allCustomerLookup {
  id: string;
  name: string;
  fK_CreatedBy_Id: string;
  fK_UpdatedBy_Id: string;
  fK_DeletedBy_Id: string;
  isDeleted: false;
  createdDate: string;
  updatedDate: string;
  deletedDate: string;
}

export interface allContractTypeLookup {
  id?: string;
  name?: string;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  fK_DeletedBy_Id?: string;
  isDeleted?: false;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;
}

export interface allWorkOrderItemsLookup {
  id?: string;
  code?: string;
  name?: string;
  description?: string;
  formula?: string;
  availableAmount?: string;
  workOrders?: string;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  fK_DeletedBy_Id?: string;
  isDeleted?: false;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;
}

export interface allLocationLookup {
  id: string;
  paciNumber: string;
  governorate: string;
  area: string;
  block: string;
  street: string;
  addressNote: string;
  latitude: string;
  longitude: string;
  fK_CreatedBy_Id: string;
  fK_UpdatedBy_Id: string;
  fK_DeletedBy_Id: string;
  isDeleted: false;
  createdDate: string;
  updatedDate: string;
  deletedDate: string;
}

export interface allStatusLookup {
  id?: string;
  name?: string;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  fK_DeletedBy_Id?: string;
  isDeleted?: false;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;
}

export interface allRolesLookup {
  id?: string;
  name?: string;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  fK_DeletedBy_Id?: string;
  isDeleted?: false;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;
}

export interface allPriorityLookup {
  id?: string;
  name?: string;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  fK_DeletedBy_Id?: string;
  isDeleted?: false;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;
}

export interface allCustomerTypeLookup {
  id?: string;
  name?: string;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  fK_DeletedBy_Id?: string;
  isDeleted?: false;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;
}

export interface allCustomerCategoryLookup {
  id?: string;
  name?: string;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  fK_DeletedBy_Id?: string;
  isDeleted?: false;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;
}

export interface allEquipmentTypeLookup {
  id?: string;
  name?: string;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  fK_DeletedBy_Id?: string;
  isDeleted?: false;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;
}


export interface allEquipmentTypeLookup {
  id?: string;
  number?: string;
  name?: string;
  fK_EquipmentType_Id?: string;
  fk_Factory_Id?: string;
  equipmentType?: string;
  isAvailable?: string;
  workOrderJobEquipment?: string
}

export interface allFactoryLookup {
  id?: string;
  name?: string;
  fK_Location_Id?: string;
  latitude?: string;
  longitude?: string;
  location?: string;
  vehicles?: any;
  workOrders?: any;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  fK_DeletedBy_Id?: string;
  isDeleted?: false;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;
}

export interface point {
  lng?: string;
  lat?: string;

}

export interface allAreaLookup {
  id?: string;
  fk_Factory_Id?: string;
  factory?: string;
  polygon?: Array<point>;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  isDeleted?: boolean,
  createdDate?: string;
  updatedDate?: string;
}

export interface allUserLookup {

  phoneNumber?: string;
  id?: string;
  phoneNumberConfirmed?: string;
  userName?: string;
  firstName?: string;
  lastName?: string;
  roleNames?: any;
  email?: string;
  emailConfirmed?: string;
  password?: string;
  fK_CreatedBy_Id?: string;
  fK_UpdatedBy_Id?: string;
  fK_DeletedBy_Id?: string;
  isDeleted?: boolean;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;

}
