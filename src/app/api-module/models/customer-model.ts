export interface CustomerHistory {
  customer?: customer;
  complains?: Array<complain>;
  contracts?: Array<contract>;
  workOrders?: Array<WorkOrderDetails>;
  isDeleted?: string;
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;

}

export interface customer {
  id?: string;
  name?: string;
  civilId?: string;
  mobile1?: string;
  mobile2?: string;
  creditLimit?: number;
  balance?: number;
  phone?: string;
  phones?: any[];
  remarks?: string;
  companyName?: string;
  division?: string;
  fK_CustomerType_Id?: string;
  fK_CustomerCategory_Id?: string;
  kcrM_Customer_Id?: string;
  fK_Location_Id?: string;
}

export interface complain {
  id?: string;
  note?: string;
  fK_Customer_Id?: string;
}

export interface contract {
    contractType: any;
    customer: any;
  lstLocationName: any[];
  id?: string;
  items: any[];
  contractNumber?: string;
  price?: string;
  startDate?: Date;
  endDate?: Date;
  amount?: any;
  fK_ContractType_Id?: string;
  pendingAmount?: string;
  remarks?: string;
  fK_Customer_Id?: string;
  contractTypeName?: string;
}

export interface WorkOrderDetails {
  workOrder?: workOrderData;
  subOrdersFactoryPercent?: any[];
  status?: any;
  priority?: any;
}


export interface workOrderData {
    fK_Item?: any;
    fK_ElementToBePoured_Id?: any;


  id?: string;
  code?: string;
  pendingAmount?: string;
  description?: string;
  amount?: string;
    startDate?: any;
    pouringTime?: any;
  endDate?: any;
  fK_Location_Id?: string;
  fK_Priority_Id?: string;
  fK_Customer_Id?: string;
  fK_Contract_Id?: string;
  fk_Status_Id?: string;
  hasSuborders?: boolean;
}

export interface Item {
  id?: string;
  price?: string;

}

export interface subOrdersFactoryPercent {
  workOrder?: workOrderData;
  factory?: Factory;
  c?: string,
  amount?: any,
  isDeleted?: boolean,
  percentage?: number;
}

export interface Factory {
  id?: string;
  name?: string;
}







