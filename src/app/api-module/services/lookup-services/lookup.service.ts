// import {allCustomerLookup} from './../../models/lookups-modal';
import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
// import {Observable} from 'rxjs/Observable';
import * as myGlobals from '../globalPath';
import {regExpEscape} from '@ng-bootstrap/ng-bootstrap/util/util';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class LookupService {
    currentDate: any;
    loggedInDate: any;

  constructor(private http: Http, private router: Router,) {
  }

  clientPath() {
    return myGlobals.ClientPath;
  }

  getAllContracts() {
    return this.http.get(myGlobals.allContracts, this.jwt()).map((response: Response) => response.json());
  }
getOrdersByContract(id) {
    console.log(myGlobals.ordersByContractId + id);
    return this.http.get(myGlobals.ordersByContractId + id, this.jwt()).map((response: Response) => response.json());
  }

  getJobsByOrder(id) {
    return this.http.get(myGlobals.jobsByOrderId + id, this.jwt()).map((response: Response) => response.json());
  }
  KCRMIdExist(kcrmId: string) {
    return this.http.get(myGlobals.KCRMIdExist + `${kcrmId}`, this.jwt()).map((response: Response) => response.json());
  }
  ItemCodeExists(code: string) {
    return this.http.get(myGlobals.ItemCodeExists + `${code}`, this.jwt()).map((response: Response) => response.json());
  }
  getConfirmationStatus() {
    return this.http.get(myGlobals.confirmationStatus, this.jwt()).map((response: Response) => response.json());
  }

  postJob(job) {
    return this.http.post(myGlobals.updateJob, job, this.jwt()).map((response: Response) => response.json());
  }

  postConfirmation(status) {
    return this.http.post(myGlobals.confirmationStatusPost, status, this.jwt()).map((response: Response) => response.json());
  }

  /*------------------------------------------------*/

  getAllOrdersLive() {
    return this.http.get(myGlobals.allOrdersLive, this.jwt()).map((response: Response) => response.json());
  }

  /*------------------------------------------------*/

  GetAllCustomerTypes() {
    return this.http.get(myGlobals.allCustomerTypes, this.jwt()).map((response: Response) => response.json());
  }

  GetAllCustomerCategories() {
    return this.http.get(myGlobals.allCustomerCategories, this.jwt()).map((response: Response) => response.json());
  }

  GetAllContractTypes() {
    return this.http.get(myGlobals.allContractType, this.jwt()).map((response: Response) => response.json());
  }

  GetAllVehicleTypes() {
    return this.http.get(myGlobals.allVehicleType, this.jwt()).map((response: Response) => response.json());
  }




  GetAllDrivers() {
    return this.http.get(myGlobals.allDrivers, this.jwt()).map((response: Response) => response.json());
  }

  GetallWorkItems() {
    return this.http.get(myGlobals.allWorkItems, this.jwt()).map((response: Response) => response.json());
  }

  GetContractMiXTypes(contractId) {
    return this.http.get(myGlobals.contractMixTypes + contractId, this.jwt()).map((response: Response) => response.json());
  }

  GetallLocations() {
    return this.http.get(myGlobals.allLocation, this.jwt()).map((response: Response) => response.json());
  }

  GetallPeriorities() {
    return this.http.get(myGlobals.allPeriorities, this.jwt()).map((response: Response) => response.json());

  }

  GetallFactories() {
    return this.http.get(myGlobals.allFactories, this.jwt()).map((response: Response) => response.json());
  }

  GetallWashingAreas() {
    return this.http.get(myGlobals.allWashing, this.jwt()).map((response: Response) => response.json());
  }

  GetallGovernorates() {
    return this.http.get(myGlobals.allGovernorates, this.jwt()).map((response: Response) => response.json());

  }

  GetallAreas(Gov: number) {
    return this.http.get(myGlobals.allAreas + '?govId=' + Gov, this.jwt()).map((response: Response) => response.json());

  }

  GetallBlocks(area: number) {
    return this.http.get(myGlobals.allBlocks + '?areaId=' + area, this.jwt()).map((response: Response) => response.json());

  }

  GetallStreets(area: number, block: string, gov: number) {
    return this.http.get(myGlobals.allStreets + '?govId=' + gov + '&areaId=' + area + '&blockName=' + block, this.jwt()).map((response: Response) => response.json());

  }

  GetLocationByPaci(PACI: number) {
    return this.http.get(myGlobals.allGetLocationByPaci + PACI, this.jwt()).map((response: Response) => response.json());

  }

  GetallStatus() {
    return this.http.get(myGlobals.allStatus, this.jwt()).map((response: Response) => response.json());
  }

  GetallStatuses() {
    return this.http.get(myGlobals.allStatuses, this.jwt()).map((response: Response) => response.json());
  }

  GetallVehiclesLocation() {
    return this.http.get(myGlobals.getAllVehiclesLocations, this.jwt()).map((response: Response) => response.json());
  }

  EditStatus(Status: any) {
    return this.http.post(myGlobals.editStatus, Status, this.jwt()).map((response: Response) => response.json());
  }

  AddStatus(Status: any) {
    return this.http.post(myGlobals.AddStatus, Status, this.jwt()).map((response: Response) => response.json());
  }

  AssignDispatcherToFactory(assigned: any) {
    return this.http.get(myGlobals.AssignDispatcherToFactory + assigned.userId + '&factoryId=' + assigned.factoryId, this.jwt()).map((response: Response) => response.json());
  }

  UpdateDispatcherToFactory(assigned: any) {
    return this.http.get(myGlobals.UpdateDispatcherFromFactory + assigned.controller.id + '&factoryId=' + assigned.factory.id + '&id=' + assigned.id, this.jwt()).map((response: Response) => response.json());
  }

  DeleteDispatcherToFactory(id: any) {
    return this.http.delete(myGlobals.DeleteDispatcherFromFactory + id, this.jwt()).map((response: Response) => response.json());
  }

  DeleteStatus(StatusID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.DeleteStatus,
      new RequestOptions({
        body: +StatusID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }


  GetallRole() {
    return this.http.get(myGlobals.allRole, this.jwt()).map((response: Response) => response.json());
  }

  EditRole(Status: any) {
    return this.http.post(myGlobals.editRole, Status, this.jwt()).map((response: Response) => response.json());
  }

  AddRole(Status: any) {
    return this.http.post(myGlobals.AddRole, Status, this.jwt()).map((response: Response) => response.json());
  }

  DeleteRole(Role: any) {
    console.log('Role');
    console.log(Role);

    return this.http.request(myGlobals.DeleteRole,
      new RequestOptions({
        body: '"' + Role + '"',
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }


  EditPeriorities(Status: any) {
    return this.http.post(myGlobals.editPeriorities, Status, this.jwt()).map((response: Response) => response.json());
  }

  AddPeriorities(Status: any) {
    return this.http.post(myGlobals.AddPeriorities, Status, this.jwt()).map((response: Response) => response.json());
  }

  DeletePeriorities(PerioritieID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.DeletePeriorities,
      new RequestOptions({
        body: +PerioritieID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }

  EditCustomerType(Status: any) {
    return this.http.post(myGlobals.editCustomerType, Status, this.jwt()).map((response: Response) => response.json());
  }

  EditCustomerCategory(Status: any) {
    return this.http.post(myGlobals.editCustomerCategory, Status, this.jwt()).map((response: Response) => response.json());
  }

  AddCustomerType(Status: any) {
    return this.http.post(myGlobals.AddCustomerType, Status, this.jwt()).map((response: Response) => response.json());
  }

  AddCustomerCategory(Status: any) {
    return this.http.post(myGlobals.AddCustomerCategory, Status, this.jwt()).map((response: Response) => response.json());
  }

  DeleteCustomerType(CustomerTypeID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.DeleteCustomerType,
      new RequestOptions({
        body: +CustomerTypeID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }

  DeleteCustomerCategory(CustomerCategoryID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.DeleteCustomerCategory,
      new RequestOptions({
        body: +CustomerCategoryID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }

  EditContractType(Status: any) {
    return this.http.post(myGlobals.editContractType, Status, this.jwt()).map((response: Response) => response.json());
  }

  EditContractToDisableItems(Contract: any) {
    return this.http.post(myGlobals.EditContract, Contract, this.jwt()).map((response: Response) => response.json());
  }

  AddContractType(Status: any) {
    return this.http.post(myGlobals.AddContractType, Status, this.jwt()).map((response: Response) => response.json());
  }

  DeleteContractType(ContractTypeID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.DeleteContractType,
      new RequestOptions({
        body: +ContractTypeID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }


  EditVehicleType(Status: any) {
    return this.http.post(myGlobals.editVehicleType, Status, this.jwt()).map((response: Response) => response.json());
  }

  GetVehicleJobs(Id: any): Observable<any> {
    return this.http.get(myGlobals.GetVehicleJobs + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  AddVehicleType(Status: any) {
    return this.http.post(myGlobals.addVehicleType, Status, this.jwt()).map((response: Response) => response.json());
  }


  GetAvailableVehiclesByNumber(Id: any): Observable<any> {
    return this.http.get(myGlobals.GetAvailableVehiclesByNumber + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }
  DeleteVehicleType(VehicleTypeID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.deleteVehicleType + VehicleTypeID,
      new RequestOptions({
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }

  GetallEquipmentType() {
    return this.http.get(myGlobals.allEquipmentType, this.jwt()).map((response: Response) => response.json());
    }
    GetAllItemPoured() {
        return this.http.get(myGlobals.allElementsToBePoured, this.jwt()).map((response: Response) => response.json());
  }

  GetAllApplicationConfigurarion() {
    return this.http.get(myGlobals.allAppConfiguration, this.jwt()).map((response: Response) => response.json());
  }

  GetAllApprovedOrders() {
    return this.http.get(myGlobals.GetAllApprovedOrders, this.jwt()).map((response: Response) => response.json());
  }


  EditEquipmentType(Status: any) {
    return this.http.post(myGlobals.editEquipmentType, Status, this.jwt()).map((response: Response) => response.json());
  }
    EditElementToBePoured(Status: any) {
        return this.http.post(myGlobals.editElementToBePoured, Status, this.jwt()).map((response: Response) => response.json());
  }
  EditAppConfiguration(Status: any) {
    return this.http.post(myGlobals.editAppConfiguration, Status, this.jwt()).map((response: Response) => response.json());
  }

  AddEquipmentType(Status: any) {
    return this.http.post(myGlobals.AddEquipmentType, Status, this.jwt()).map((response: Response) => response.json());
    }
    AddElementToBePoured(Status: any) {
        return this.http.post(myGlobals.addElementToBePoured, Status, this.jwt()).map((response: Response) => response.json());
    }
  AddAppConfiguration(Status: any) {
    return this.http.post(myGlobals.addAppConfiguration, Status, this.jwt()).map((response: Response) => response.json());
  }

  DeleteEquipmentType(EquipmentTypeID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.DeleteEquipmentType,
      new RequestOptions({
        body: +EquipmentTypeID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }
    DeleteElementToBePoured(ElementToBePouredID: any) {
        console.log(this.header());

        return this.http.request(myGlobals.deleteElementToBePoured,
            new RequestOptions({
                body: +ElementToBePouredID,
                headers: this.header(),
                method: 'DELETE'

            })).map((response: Response) => response.json());
  }

  DeleteAppConfiguration(appConfigurationID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.deleteAppConfiguration,
      new RequestOptions({
        body: +appConfigurationID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }


  GetallEquipment() {
    return this.http.get(myGlobals.allEquipment, this.jwt()).map((response: Response) => response.json());
  }

  EditEquipment(Status: any) {
    return this.http.post(myGlobals.editEquipment, Status, this.jwt()).map((response: Response) => response.json());
  }

  AddEquipment(Status: any) {
    return this.http.post(myGlobals.AddEquipment, Status, this.jwt()).map((response: Response) => response.json());
  }

  DeleteEquipment(EquipmentID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.DeleteEquipment,
      new RequestOptions({
        body: +EquipmentID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }

  EditItem(Status: any) {
    return this.http.post(myGlobals.editItem, Status, this.jwt()).map((response: Response) => response.json());
  }

  AddItem(Status: any) {
    return this.http.post(myGlobals.AddItem, Status, this.jwt()).map((response: Response) => response.json());
  }

  DeleteItem(EquipmentID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.DeleteItem,
      new RequestOptions({
        body: +EquipmentID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }


  EditFactory(Status: any) {
    return this.http.post(myGlobals.editFactory, Status, this.jwt()).map((response: Response) => response.json());
  }

  AddFactory(Status: any) {
    return this.http.post(myGlobals.AddFactory, Status, this.jwt()).map((response: Response) => response.json());
  }

  AddWashingArea(Washing: any) {
    return this.http.post(myGlobals.AddWashingArea, Washing, this.jwt()).map((response: Response) => response.json());
  }

  GetWashingAreaById(WashingAreaID: any) {
    return this.http.get(myGlobals.GetWashingAreaById + WashingAreaID, this.jwt()).map((response: Response) => response.json());
  }

  DeleteFactory(EquipmentID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.DeleteFactory,
      new RequestOptions({
        body: +EquipmentID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }

  GetallUser() {
    return this.http.get(myGlobals.alluser, this.jwt()).map((response: Response) => response.json());
  }

  GetAllVehicles() {
    return this.http.get(myGlobals.AllVehicles, this.jwt()).map((response: Response) => response.json());
  }


  AddUser(Status: any) {
    return this.http.post(myGlobals.Adduser, Status, this.jwt()).map((response: Response) => response.json());
  }

  UpdateUser(user: any) {
    return this.http.post(myGlobals.UpdateUser, user, this.jwt()).map((response: Response) => response.json());
  }

  ResetPassword(resetObj: any) {
    return this.http.post(myGlobals.ResetPassword, resetObj, this.jwt()).map((response: Response) => response.json());
  }

  DeleteUser(userID: any) {
    console.log(this.header());

    return this.http.request(myGlobals.Deleteuser,
      new RequestOptions({
        body: userID,
        headers: this.header(),
        method: 'DELETE'

      })).map((response: Response) => response.json());
  }

  UsernameExists(name: any) {
    return this.http.get(myGlobals.UsernameExists + `${name}`, this.jwt()).map((response: Response) => response.json());
  }

  GetAllFactoryControllers() {
    return this.http.get(myGlobals.GetAllFactoryControllers, this.jwt()).map((response: Response) => response.json());
  }

  GetUsersByRoleName(roleName) {
    return this.http.get(myGlobals.GetUsersByRoleName + roleName, this.jwt()).map((response: Response) => response.json());
  }

  EmailExists(mail: any) {
    return this.http.get(myGlobals.EmailExists + `${mail}`, this.jwt()).map((response: Response) => response.json());
  }

  FillterSubOrders(obj: any) {
    return this.http.post(myGlobals.filterSubOrder, obj, this.jwt()).map((response: Response) => response.json());
  }

  FilterSubordersForDispatcher(obj: any) {
    return this.http.post(myGlobals.FilterSubordersForDispatcher, obj, this.jwt()).map((response: Response) => response.json());
  }
  checkIfDateExceedAday() {
    this.currentDate = new Date();
    this.loggedInDate = new Date (JSON.parse(localStorage.getItem('currentDate')));
    let Diff = Math.floor(Math.abs(this.currentDate - this.loggedInDate) / 36e5);
    if (Diff >= 24) {
      this.router.navigate(['/login']);
      document.getElementById("mySidenav").style.width = "0";
      document.getElementById("main").style.marginRight = "0";
      return true;
    }
  }
  // private helper methods
  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.checkIfDateExceedAday() == true)
      return;

    if (currentUser && currentUser.token) {
      let headers = new Headers({'Authorization': 'Bearer ' + currentUser.token});
      headers.append('Content-Type', 'application/json');
      return new RequestOptions({headers: headers});
    }
  }

  private header() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      var headers = new Headers({'Authorization': 'Bearer ' + currentUser.token});
      headers.append('Content-Type', 'application/json');
      return headers;
    }
  }


}
