import {CustomerHistory, customer, contract, complain, WorkOrderDetails} from './../../models/customer-model';
import {Observable} from 'rxjs';
import {newDetailsCustomer} from './../../models/newCustomer';
import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import * as myGlobals from '../globalPath';

@Injectable()
export class CustomerCrudService {

  constructor(private http: Http) {
  }
  getTimeOffsetTimeZone() {
    let offset = new Date().getTimezoneOffset();
    let hours = (Math.abs(offset) / 60);
    return hours;
  }
  convertDateForSaving(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${year}/${month}/${day}`;
    //console.log(formatedDate);
    return formatedDate;
  }

  GoSearchCustomer(SearchField: string) {
    return this.http.get(myGlobals.SearchCustomer + `${SearchField}`, this.jwt()).map((response: Response) => response.json());
  }

  getPendingApprovedOrders() {
    return this.http.get(myGlobals.pendingApprovedOrders, this.jwt()).map((response: Response) => response.json());
  }

  getPendingApprovedOrdersAsync(pagingData) {
    return this.http.post(myGlobals.getPendingApprovedOrdersAsync, pagingData, this.jwt()).map((response: Response) => response.json());
  }

  addCustomer(customer: newDetailsCustomer) {
    return this.http.post(myGlobals.CreatenewCustomer, customer, this.jwt()).map((response: Response) => response.json());
  }
  KCRMIdExist(kcrmId: string) {
    return this.http.get(myGlobals.KCRMIdExist + `${kcrmId}`, this.jwt()).map((response: Response) => response.json());
  }


  getEquipmentByOrder(orderId) {
    return this.http.get(myGlobals.getEquipmentByOrder + orderId, this.jwt()).map((response: Response) => response.json());
  }

  getLocationByContract(contractId) {
    return this.http.get(myGlobals.locationsByContract + contractId, this.jwt()).map((response: Response) => response.json());
  }

  getPendingApprovedOrder(id) {
    return this.http.get(myGlobals.PendingApprovedOrder + id, this.jwt()).map((response: Response) => response.json());
  }

  GetCustomerDetail(Id: number): Observable<CustomerHistory> {
    return this.http.get(myGlobals.GetCustomerDetailsByID + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  GetCustomerDetailsByIDAsync(Id: number): Observable<any> {
    return this.http.get(myGlobals.GetCustomerDetailsByIdAsync + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  EditCustomerInfo(customer: customer) {
    
    return this.http.post(myGlobals.EditCustomerInfo, customer, this.jwt()).map((response: Response) => response.json());
  }

  GetContractById(contractId) {
    return this.http.get(myGlobals.GetContractById + contractId, this.jwt()).map((response: Response) => response.json());
  }

  GetNonExpiredContracts(customerId) {
    return this.http.get(myGlobals.getNonExpiredContracts + customerId, this.jwt()).map((response: Response) => response.json());
  }

  EditContract(contract: any) {
    return this.http.post(myGlobals.EditContract, contract, this.jwt())

      .map((response: Response) => response.json());
  }
  GetLocationByContractIds(ContractIds: any) {
    return this.http.post(myGlobals.GetLocationByContractIds, ContractIds, this.jwt()).map((response: Response) => response.json());
  }
  

  IsOrderHaveContractItem(contractId,itemId) {
    return this.http.get(myGlobals.IsOrderHaveContractItem.replace('{0}',contractId).replace('{1}',itemId), this.jwt()).map((response: Response) => response.json());
  }
  AddContract(contract: contract) {
    return this.http.post(myGlobals.AddContract, contract, this.jwt()).map((response: Response) => response.json());
  }

  EditComplain(complain: complain) {
    return this.http.post(myGlobals.EditComplain, complain, this.jwt()).map((response: Response) => response.json());
  }

  AddComplain(complain: complain) {
    return this.http.post(myGlobals.AddComplain, complain, this.jwt()).map((response: Response) => response.json());
  }

  EditWorkOrderServ(WorkOrderDetails: WorkOrderDetails) {
    return this.http.post(myGlobals.EditWorkOrder, WorkOrderDetails, this.jwt()).map((response: Response) => response.json());
  }
  EditWorkOrderCancelled(workOrderId,statusId) {
    return this.http.get(myGlobals.EditWorkOrderCancelled.replace('{0}',workOrderId).replace('{1}',statusId), this.jwt()).map((response: Response) => response.json());
  }

  GetOrderCountPerDate(currentDate) {
    return this.http.get(myGlobals.getOrderCountPerDate + currentDate, this.jwt()).map((response: Response) => response.json());
  }

  AddNewWorkOrder(WorkOrderDetails: WorkOrderDetails) {

    return this.http.post(myGlobals.AddNewWorkOrder, WorkOrderDetails, this.jwt()).map((response: Response) => response.json());
  }

  AddNewLocation(location: any) {
    return this.http.post(myGlobals.AddLocation, location, this.jwt()).map((response: Response) => response.json());
  }

  UpdateLocation(location: any) {
    return this.http.post(myGlobals.EditLocation, location, this.jwt()).map((response: Response) => response.json());
  }

  DeleteComplain(Id: any) {
    return this.http.delete(myGlobals.DeleteComplain + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }
  DeleteCustomer(Id: any) {
    return this.http.delete(myGlobals.DeleteCustomer + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  GetSelctedCustomer(Id: any) {
    return this.http.delete(myGlobals.GetSelctedCustomer + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }


  DeleteWorkOrder(Id: any) {
    return this.http.delete(myGlobals.DeleteWorkOrder + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  DeleteContract(Id: any) {
    return this.http.delete(myGlobals.DeleteContract + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  GetAllWorkOrdersDispature(): Observable<any> {
    return this.http.get(myGlobals.AllWorkOrdersDispature, this.jwt()).map((response: Response) => response.json());
  }

  GetWorkOrderByOrder(orderId): Observable<any> {
    return this.http.get(myGlobals.getWorkOrderbyOrder + orderId, this.jwt()).map((response: Response) => response.json());
  }

  GetAllWorkOrdersMovementController(pagingData): Observable<any> {
    return this.http.post(myGlobals.AllWorkOrdersMovementController, pagingData, this.jwt()).map((response: Response) => response.json());
  }

  GetCallendarWorkOrders(): Observable<any> {
    return this.http.get(myGlobals.GetCallendarWorkOrders, this.jwt()).map((response: Response) => response.json());
  }

  GetAvailableEquipmentInFactory(Id: any): Observable<any> {
    return this.http.get(myGlobals.AvailableEquipmentInFactory + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  UpdateAssignmentToWorkOrder(newEquip: any) {
    return this.http.post(myGlobals.UpdateAssignmentToWorkOrder, newEquip, this.jwt()).map((response: Response) => response.json());
  }

  UnassignEquipmentFromWorkOrder(newEquip: any) {
    return this.http.post(myGlobals.UnassignEquipmentFromWorkOrder, newEquip, this.jwt()
    ).map((response: Response) => response.json());
  }

  GetLatestCustomers() {
    return this.http.get(myGlobals.GetLatestCustomers, this.jwt()).map((response: Response) => response.json());
  }  

  GetAllCustomers() {
    return this.http.get(myGlobals.getAllCustomers, this.jwt()).map((response: Response) => response.json());
  }



  getAllCustomersByPaging(paginatedData) {
    return this.http.post(myGlobals.getAllCustomersByPaging, paginatedData, this.jwt()).map((response: Response) => response.json());
  }



  GetAllContractsByCustomerIdAsync(paginatedData) {
    return this.http.post(myGlobals.GetAllContractsByCustomerIdAsync, paginatedData, this.jwt()).map((response: Response) => response.json());
  }

  GetDetailedWorkOrdersByCustomerIdAsync(paginatedData) {
    return this.http.post(myGlobals.GetDetailedWorkOrdersByCustomerIdAsync, paginatedData, this.jwt()).map((response: Response) => response.json());
  }

  UpdateSubOrder(newOrder: any) {
    return this.http.post(myGlobals.UpdateSubOrder, newOrder, this.jwt()).map((response: Response) => response.json());
  }

  ReassinFactory(workorderId: any, factoryId: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.reassinFactory + `${workorderId}` + '&factoryId=' + `${factoryId}`, this.jwt()).map((response: Response) => response.json());
  }

  UpdateOrderAssignmentToFactory(workorderId: any, factoryId: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AssignmentToFactory + `${workorderId}` + '&factoryId=' + `${factoryId}`, this.jwt()).map((response: Response) => response.json());
  }

  GetJobDetails(Id: any): Observable<any> {
    return this.http.get(myGlobals.JobDetails + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  getAllComplains(Id: any): Observable<any> {
    return this.http.get(myGlobals.getAllComplains + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }



  GetReadyVehicle(factoryId, skippedCount): Observable<any> {
    return this.http.get(myGlobals.readyVehicle(factoryId, skippedCount), this.jwt()).map((response: Response) => response.json());
  }

  GetAvailableVehicleInFactory(Id: any): Observable<any> {
    return this.http.get(myGlobals.AvailableVehicleInFactory + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }


 
  AddJop(Job: any) {
    return this.http.post(myGlobals.AddJop, Job, this.jwt()).map((response: Response) => response.json());
  }

  CancelJop(Id: any, CancelReason): Observable<CustomerHistory> {
    return this.http.get(myGlobals.CancelJop + `${Id}`+ '&CancelReason=' + `${CancelReason}`, this.jwt()).map((response: Response) => response.json());
  }
  JobDone(Id: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.JobDone + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  ReassignJop(JobId: any, workorderId: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.ReassignJop + `${JobId}` + '&workorderId=' + `${workorderId}`, this.jwt()).map((response: Response) => response.json());
  }

  GetAllWorkOrder(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AllWorkOrder, this.jwt()).map((response: Response) => response.json());
  }

  checkCustomerNameExist(name: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.checkCustomerNameExist + `${name}`, this.jwt()).map((response: Response) => response.json());
  }

  checkCustomerphoneExist(phone: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.checkCustomerphoneExist + `${phone}`, this.jwt()).map((response: Response) => response.json());
  }
  
  chechCivilId(civilId: any) {
    return this.http.get(myGlobals.civilIdExists + `${civilId}`, this.jwt()).map((response: Response) => response.json());
  }

  chechContractNum(contractNum: any) {
    return this.http.get(myGlobals.contractExists + `${contractNum}`, this.jwt()).map((response: Response) => response.json());
  }

  GetAllNotificationsWorkOrder(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.NotificationOrders, this.jwt()).map((response: Response) => response.json());
  }

  GetAllRemindWorkOrder(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.RemindOrders, this.jwt()).map((response: Response) => response.json());
  }

  filterWorkOrdersReport(data): Observable<CustomerHistory> {
    return this.http.post(myGlobals.filterWorkorder, data, this.jwt()).map((response: Response) => response.json());
  }

  // private helper methods

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      let headers = new Headers({'Authorization': 'Bearer ' + currentUser.token});
      headers.append('Content-Type', 'application/json');
      return new RequestOptions({headers: headers});
    }
  }
}
