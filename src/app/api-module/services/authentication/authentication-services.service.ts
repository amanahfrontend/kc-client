import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import * as myGlobals from '../globalPath';


let headers = new Headers({ 'Content-Type': 'application/json' });
headers.append('Access-Control-Allow-Origin','*');

let options = new RequestOptions({ headers: headers });

@Injectable()
export class AuthenticationServicesService {

 constructor(private http: Http) { }
 isLoggedin: boolean = false;

    login(username: string, password: string) {

        return this.http.post(myGlobals.MainPath+'/api/auth/token', JSON.stringify({ username: username, password: password }),options)
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.token) {
                    console.log('---------- user login -------------- ');
                    console.log(user);
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                  localStorage.setItem('currentUser', JSON.stringify(user));
                  let date=new Date();
                  localStorage.setItem('currentDate', JSON.stringify(date) );

                    this.isLoggedin = true;

                }

                return user;
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.isLoggedin = false;

    }

    isLoggedIn() {

        if (localStorage.getItem("currentUser") == null) {
            this.isLoggedin = false;
            return this.isLoggedin;
        }
        else {
            return true;
        }
    }
    CurrentUser() {

        if (localStorage.getItem("currentUser") == null) {
            this.isLoggedin = false;
            return null;
        }
        else {
            return JSON.parse(localStorage.getItem('currentUser'));
        }
    }


}
