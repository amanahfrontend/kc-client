'use strict';

//export const MainPath = 'http://amanahadmin-001-site8.itempurl.com';
//export const ClientPath = 'http://amanahadmin-001-site10.itempurl.com/';

//export const MainPath = 'http://amanahadmin-001-site38.itempurl.com';
//export const ClientPath = 'http://amanahadmin-001-site39.itempurl.com';


//export const MainPath = 'http://localhost/KC-Server-Dev';
//export const ClientPath = 'http://localhost/KC-Server-Dev';


//export const MainPath = 'http://192.9.30.13/KC-Server';
//export const ClientPath = 'http://192.9.30.13/KC-Client/'

//export const MainPath = 'http://192.9.30.13/KCRM-Server';
//export const ClientPath = 'http://192.9.30.13/KC-Client/'

//export const MainPath = 'http://192.9.30.13/KCRM-Server';
//export const ClientPath = 'http://192.9.30.13/KC-Client/'



export const googlMapsAPiKey = 'AIzaSyAnh4qB0Ljx9umIc94sQTqQCYPO-vJT0k8';
//KCRMIdExist?kcrmId=1081 -------------------- Customers URLs ------------------------

export const SearchCustomer = MainPath + '/api/customer/SearchCustomer?searchToken=';
export const KCRMIdExist = MainPath + '/api/customer/KCRMIdExist?kcrmId=';
export const ItemCodeExists = MainPath + '/api/item/ItemCodeExists?code=';
export const getAllCustomersByPaging = MainPath + '/api/customer/GetAllByPaging';



export const getAllCustomers = MainPath + '/api/customer/getall';

export const CreatenewCustomer = MainPath + '/api/customer/CreateDetailedCustomer';
export const GetCustomerDetailsByID = MainPath + '/api/customer/CustomerHistory/';
export const GetCustomerDetailsByIdAsync = MainPath + '/api/customer/GetCustomerDetailsByIdAsync/';

export const EditCustomerInfo = MainPath + '/api/customer/Update';
export const GetLatestCustomers = MainPath + '/api/customer/GetLatestCustomers';


export const DeleteCustomer = MainPath + '/api/customer/DeleteCustomer?id=';

export const GetSelctedCustomer = MainPath + '/api/customer/GetSelctedCustomer?id=';


export const EditContract = MainPath + '/api/contract/Update';
// export const AddContract = MainPath + '/api/contract/add';
export const AddContract = MainPath + '/api/contract/CreateCustomContract';
export const AddNewWorkOrder = MainPath + '/api/WorkOrder/AddWorkOrder';
export const AddLocation = MainPath + '/api/location/add';
export const EditLocation = MainPath + '/api/location/update';
export const GetAllContractsByCustomerIdAsync = MainPath + '/api/contract/GetAllContractsByCustomerIdAsync';

export const GetLocationByContractIds = MainPath + '/api/location/GetLocationByContractIds';
export const getAllComplains = MainPath + '/api/complain/getAllComplains?id=';

export const EditComplain = MainPath + '/api/complain/Update';
export const AddComplain = MainPath + '/api/complain/add';
export const EditWorkOrder = MainPath + '/api/WorkOrder/UpdateWorkOrder';
export const GetDetailedWorkOrdersByCustomerIdAsync = MainPath + '/api/WorkOrder/GetDetailedWorkOrdersByCustomerIdAsync';

export const EditWorkOrderCancelled = MainPath + '/api/WorkOrder/UpdateWorkOrderStatus?workOrderId={0}&statusId={1}';
export const pendingApprovedOrders = MainPath + '/api/WorkOrder/GetPendingApprovedOrders';
export const getPendingApprovedOrdersAsync = MainPath + '/api/WorkOrder/getPendingApprovedOrdersAsync';



export const IsOrderHaveContractItem = MainPath + '/api/WorkOrder/IsOrderHaveContractItem?contractId={0}&itemId={1}';


export const PendingApprovedOrder = MainPath + '/api/OrderConfirmationHistory/GetByOrderId?orderId=';

export const locationsByContract = MainPath + '/api/location/GetLocationByContractId?contractId=';

//-------------------- Lookups URls -----------------------------
export const allCustomerTypes = MainPath + '/api/CustomerType/getall';
export const allCustomerCategories = MainPath + '/api/CustomerCategory/getall';
export const allContractType = MainPath + '/api/ContractType/getall';
export const allVehicleType = MainPath + '/api/vehicle/getall';
export const allWorkItems = MainPath + '/api/Item/GetAll';
export const contractMixTypes = MainPath + '/api/ContractItems/GetByContract?contractId=';
export const allLocation = MainPath + '/api/location/getall';
export const allFactories = MainPath + '/api/Factory/GetAll';
export const allWashing = MainPath + '/api/WashingArea/getall';
export const allGovernorates = MainPath + '/api/location/GetAllGovernorates';

export const allAreas = MainPath + '/api/location/GetAreas';
export const allBlocks = MainPath + '/api/location/GetBlocks';
export const allStreets = MainPath + '/api/location/GetStreets';
export const allGetLocationByPaci = MainPath + '/api/location/GetLocationByPaci?paciNumber=';
export const allStatus = MainPath + '/api/Status/GetAllByRole';
export const allStatuses = MainPath + '/api/Status/GetAll';
export const getAllVehiclesLocations = MainPath + '/api/vehicle/GetAllVehiclesLocations';
export const allDrivers = MainPath + '/api/driver/getall';

export const editStatus = MainPath + '/api/Status/Update';
export const AddStatus = MainPath + '/api/Status/Add';
export const DeleteStatus = MainPath + '/api/Status/Delete';

export const allRole = MainPath + '/api/Role/getall';
export const editRole = MainPath + '/api/Role/Update';
export const AddRole = MainPath + '/api/Role/Add';
export const DeleteRole = MainPath + '/api/Role/Delete';

export const allPeriorities = MainPath + '/api/Priority/getall';
export const editPeriorities = MainPath + '/api/Priority/Update';
export const AddPeriorities = MainPath + '/api/Priority/Add';
export const DeletePeriorities = MainPath + '/api/Priority/Delete';

export const editCustomerType = MainPath + '/api/CustomerType/Update';
export const AddCustomerType = MainPath + '/api/CustomerType/Add';
export const DeleteCustomerType = MainPath + '/api/CustomerType/Delete';

export const editCustomerCategory = MainPath + '/api/CustomerCategory/Update';
export const AddCustomerCategory = MainPath + '/api/CustomerCategory/Add';
export const DeleteCustomerCategory = MainPath + '/api/CustomerCategory/Delete';

export const editContractType = MainPath + '/api/ContractType/Update';
export const AddContractType = MainPath + '/api/ContractType/Add';
export const DeleteContractType = MainPath + '/api/ContractType/Delete';

export const editContractToDisableItems = MainPath + '/api/contract/editcontract';

export const editVehicleType = MainPath + '/api/vehicle/Update';
export const addVehicleType = MainPath + '/api/vehicle/Add';
export const deleteVehicleType = MainPath + '/api/vehicle/Delete?id=';

export const allEquipmentType = MainPath + '/api/EquipmentType/getall';

export const allElementsToBePoured = MainPath + '/api/ElementToBePoured/GetAll';
export const allAppConfiguration = MainPath + '/api/applicationConfiguration/GetAll';

export const addAppConfiguration = MainPath + '/api/applicationConfiguration/Add';
export const editAppConfiguration = MainPath + '/api/applicationConfiguration/Update';
export const deleteAppConfiguration = MainPath + '/api/applicationConfiguration/Delete';


export const addElementToBePoured = MainPath + '/api/ElementToBePoured/Add';
export const editElementToBePoured = MainPath + '/api/ElementToBePoured/Update';
export const deleteElementToBePoured = MainPath + '/api/ElementToBePoured/Delete';


export const getEquipmentByOrder = MainPath + '/api/Equipment/GetEquipmentAssignedToWorkOrder?workOrderId=';
export const editEquipmentType = MainPath + '/api/EquipmentType/Update';
export const AddEquipmentType = MainPath + '/api/EquipmentType/Add';
export const DeleteEquipmentType = MainPath + '/api/EquipmentType/Delete';

export const allEquipment = MainPath + '/api/Equipment/getall';
export const editEquipment = MainPath + '/api/Equipment/Update';
export const AddEquipment = MainPath + '/api/Equipment/Add';
export const DeleteEquipment = MainPath + '/api/Equipment/Delete';
export const GetContractById = MainPath + '/api/contract/get?id=';

export const editItem = MainPath + '/api/Item/Update';
export const AddItem = MainPath + '/api/Item/Add';
export const DeleteItem = MainPath + '/api/Item/Delete';

export const editFactory = MainPath + '/api/Factory/Update';
export const AddFactory = MainPath + '/api/Factory/Add';
export const DeleteFactory = MainPath + '/api/Factory/Delete';
export const AllFactory = MainPath + '/api/Factory/GetAll';

export const AddWashingArea = MainPath + '/api/WashingArea/Add';
export const GetWashingAreaById = MainPath + '/api/WashingArea/get?id=';

export const alluser = MainPath + '/api/User/getall';
export const Adduser = MainPath + '/api/User/Add';
export const UpdateUser = MainPath + '/api/User/update';
export const ResetPassword = MainPath + '/api/User/ResetPassword';
export const Deleteuser = MainPath + '/api/User/Delete';

export const DeleteComplain = MainPath + '/api/complain/delete?id=';

export const DeleteContract = MainPath + '/api/contract/delete?id=';
export const DeleteWorkOrder = MainPath + '/api/workorder/delete?id=';
export const GetAllApprovedOrders = MainPath + '/api/workorder/GetAllApprovedOrders';

export const UsernameExists = MainPath + '/api/User/UsernameExists?username=';

export const GetAllFactoryControllers = MainPath + '/api/user/GetAllFactoryControllers';
export const GetUsersByRoleName = MainPath + '/api/user/getbyrole?role=';

export const AssignDispatcherToFactory = MainPath + '/api/user/AssignControllerToFactory?userId=';
export const DeleteDispatcherFromFactory = MainPath + '/api/user/DeleteControllerFactoryAssignment/';
export const UpdateDispatcherFromFactory = MainPath + '/api/User/UpdateControllerFactoryAssignment?userId=';

export const EmailExists = MainPath + '/api/User/EmailExists?email=';

export const confirmationStatus = MainPath + '/api/LKP_ConfirmationStatus/getall';

export const confirmationStatusPost = MainPath + '/api/OrderConfirmationHistory/add';

// ---------------------------Work Order Management page -------------------------

export const GetCallendarWorkOrders = MainPath + '/api/WorkOrder/GetCalenderSuborders';
export const getOrderCountPerDate = MainPath + '/api/WorkOrder/GetCountForDate?startDate=';

export const AllWorkOrdersDispature = MainPath + '/api/WorkOrder/GetTodaySubordersForDispatcher';



export const AllWorkOrdersMovementController = MainPath + '/api/WorkOrder/GetTodaySuborders';
export const AvailableEquipmentInFactory = MainPath + '/api/Equipment/GetAvailableEquipmentInFactory?factoryId=';
export const UpdateAssignmentToWorkOrder = MainPath + '/api/Equipment/UpdateAssignmentToWorkOrder';
export const UnassignEquipmentFromWorkOrder = MainPath + '/api/Equipment/UnassignEquipmentFromWorkOrder';
export const UpdateSubOrder = MainPath + '/api/WorkOrder/UpdateSubOrder';
export const reassinFactory = MainPath + '/api/WorkOrder/CanAssignToFactory?workorderId=';
export const AssignmentToFactory = MainPath + '/api/WorkOrder/UpdateOrderAssignmentToFactory?orderId=';
export const JobDetails = MainPath + '/api/job/GetByWorkOrder?workorderId=';
export function readyVehicle(factoryId, skippedCount) {
  return MainPath + '/api/vehicle/GetAvailableVehicleByOrder?factoryId=' + factoryId + '&skippedNumber=' + skippedCount;
}

export const AvailableVehicleInFactory = MainPath + '/api/vehicle/GetAvailableVehicleInFactory?factoryId=';



export const GetVehicleJobs = MainPath + '/api/vehicle/GetVehicleJobs?vechileId=';

export const GetAvailableVehiclesByNumber = MainPath + '/api/vehicle/GetAvailableVehiclesByNumber?number=';

export const AddJop = MainPath + '/api/job/add';
export const CancelJop = MainPath + '/api/job/Cancel?jobId=';
export const JobDone = MainPath + '/api/job/JobDone?jobId=';



export const ReassignJop = MainPath + '/api/job/Reassign?jobId=';
export const AllWorkOrder = MainPath + '/api/WorkOrder/getall';
export const checkCustomerNameExist = MainPath + '/api/customer/IsCustomerNameExist?name=';
export const checkCustomerphoneExist = MainPath + '/api/customer/IsPhoneExist?phone=';
export const AllVehicles = MainPath + '/api/vehicle/getall';

export const NotificationOrders = MainPath + '/api/WorkOrder/getall';
export const RemindOrders = MainPath + '/api/WorkOrder/getall';
export const getWorkOrderbyOrder = MainPath + '/api/WorkOrder/get?id=';

export const hub = MainPath + '/dispatchingHub?authorization=';

export const lateJob = 'OnLateJobPublished';
export const updatedJob = 'OnUpdatesPublished';
export const upcomingJob = 'OnUpcomingWorkordersPublished';
export const liveLocation = 'OnLiveLoctionPublished';
export const orderConfirmation = 'OnOrderConfirmationPublished';

export const filterWorkorder = MainPath + '/api/workorder/filter';
export const filterSubOrder = MainPath + '/api/workorder/filtersuborders';

export const FilterSubordersForDispatcher = MainPath + '/api/workorder/FilterSubordersForDispatcher';


/*----------------------- Operations ---------------------*/

export const allContracts = MainPath + '/api/contract/getall';
export const getNonExpiredContracts = MainPath + '/api/contract/GetNonExpiredContracts?customerId=';
export const ordersByContractId = MainPath + '/api/WorkOrder/GetByContract?contractId=';
export const jobsByOrderId = MainPath + '/api/job/GetAllByWorkOrder?workorderId=';
export const updateJob = MainPath + '/api/job/UpdateDeliveryNote';

/*---------------------- Tracking ----------------------*/

export const allOrdersLive = MainPath + '/api/WorkOrder/GetTodaySuborders';






export const civilIdExists = MainPath + '/api/customer/CivilIdExist?civilId=';
export const contractExists = MainPath + '/api/contract/ContractNumberExists?number=';
