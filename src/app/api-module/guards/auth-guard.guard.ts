import {AlertServiceService} from './../services/alertservice/alert-service.service';
import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Location} from '@angular/common';

@Injectable()
export class AuthGuardGuard implements CanActivate {
  public roles: any;

  constructor(private router: Router, private _location: Location, private alertService: AlertServiceService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.roles = route.data["roles"] as Array<string>;
    if (localStorage.getItem('currentUser')) {
      let UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
      console.log(UserAllRoles.includes("CallCenter"));
      if (this.checkRoles(this.roles)) {
        return true;
      }
      else {
        if (state.url == '/' && (UserAllRoles.includes("Admin"))) {
          this.router.navigate(['/admin']);
          return false;
        }
        else if (state.url == '/' && (UserAllRoles.includes("Dispatcher") || UserAllRoles.includes("MovementController"))) {
          console.log('will route to board');
          this.router.navigate(['/workOrderBoard']);
          return false;
        }
        else if (state.url == '/' && (UserAllRoles.includes("CallCenter") || UserAllRoles.includes("SuperCallCenter")) ) {
          console.log('will route to confirmations');
          this.router.navigate(['search/orderConfirmation']);
          return false;
        }
        else {
          this.alertService.error('You don\'t have permission to Access');
          return false;

        }


      }
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }

  checkRoles(listRoles: Array<string>) {


    let UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
    console.log("------------------ UserAllRoles ----------- ");
    console.log(UserAllRoles);
    console.log("------------------ listRoles ----------- ");
    console.log(listRoles);

    for (var i = 0; i < this.roles.length; i++) {

      for (var j = 0; j < UserAllRoles.length; j++) {
        if (this.roles[i].toLowerCase() == UserAllRoles[j].toLowerCase()) {
          return true;
        }

      }
    }
    return false;
  }
}
