import { LookupService } from './../../api-module/services/lookup-services/lookup.service';
import { CustomerCrudService } from './../../api-module/services/customer-crud/customer-crud.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ExcelService } from '../excel-service.service';
import * as moment from 'moment';
import { UtilitiesService } from '../../operations/utilities.service';


@Component({
  selector: 'app-report-management',
  templateUrl: './report-management.component.html',
  styleUrls: ['./report-management.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class ReportManagementComponent implements OnInit {
  // initial center position for the map
  public lat: number = 29.378586;
  public lng: number = 47.990341;
  zoom: number = 8;
  factories: any[];
  status: any[];
  priorities: any[];
  orders;
  data: any[];
  currentPage: number = 0;
  lastPage: number = 0;
  startDate: any = new Date();
  endDate: any;

  public filterCritieria = {
    fK_Factory_Id: [""],
    fK_Status_Id: [""],
    fK_Priority_Id: [""],
    startDate: [null, null],
    amount: ["", ""],
    code: [""],
    pendingAmount: ["", ""]
  };
  public obj = {
    pageIndex: this.currentPage,
    pageSize: 10,
    data: []
  }

  public loadingImage: string = "assets/Images/loading.gif"
  loading = false;
    selectedLang: string;

  constructor(private customerService: CustomerCrudService, private utilities: UtilitiesService, private excelService: ExcelService, private _lookupService: LookupService, private _customerService: CustomerCrudService) {

    if (window.localStorage.getItem('lang')) {
      console.log(window.localStorage.getItem('lang'));
      this.selectedLang = window.localStorage.getItem('lang');
    } else {
      this.selectedLang = this.utilities.languages[1].symbol;
    }

  }

  ngOnInit() {
    this.setLookups();
  }

  pageChanged(pageNumber) {
    this.obj.pageIndex = pageNumber;
    this._customerService.filterWorkOrdersReport(this.obj).subscribe(
      data => {
        this.currentPage = data["pageIndex"];
        this.lastPage = Math.round(data['count'] / data['pageSize']);
        this.data = data['data'];
      }
    )
  }

  //for(var i = 0; i < length; i++) {


  exportAsXLSX(): void {
    let allOrders: any[] = [];
    let location =""
    for (let i = 0; i < this.data.length; i++) {
      let location = this.data[i].location;

      if (this.data[i].selectedLang == "en") {
        location = this.data[i].location;

        if (this.data[i].locationBlock != null) {
          location +=", Block:"+this.data[i].locationBlock;
        }
        if (this.data[i].locationStreet != null) {
          location += ", Street:" +this.data[i].locationStreet;

        }
        if (this.data[i].locationRemarks != null) {
          location += "," +this.data[i].locationRemarks;

        }
      }
      else {
        location = this.data[i].location;

        if (this.data[i].locationBlock != null) {
          location += ", القطعة:" + this.data[i].locationBlock;
        }
        if (this.data[i].locationStreet != null) {
          location += ", الشارع:" + this.data[i].locationStreet;

        }
        if (this.data[i].locationRemarks != null) {
          location += "," + this.data[i].locationRemarks;

        }

      }
      allOrders.push({
        "Ser": i + 1, "Factory": this.data[i].factoryName,
        "KCRM Customer Id": this.data[i].kcrM_Customer_Id, "Customer Name": this.data[i].customer_Name,
        "Customer Phone": this.getAllPhones(this.data[i].customer_Phone),

        "Location": location
        , "Poured Element": this.data[i].poured_Element, "Order Date": moment(this.data[i].startDate).format('LL'),
        "Order Time": moment(this.data[i].startDate).format('LTS'),
        "Ready Mix Type": this.data[i].readyMixtype,
        "Amount": this.data[i].amount, "Remarks": this.data[i].description
      });


      //allOrders[a].push(this.data[a].vechile_Name);
      //allOrders[a].push(this.data[a].poured_Element);
      //allOrders[a].push(this.data[a].startDate);
      //allOrders[a].push(this.data[a].readyMixtype);
      //allOrders[a].push(this.data[a].amount);
      ////allOrders[a].push(this.data[a].description);



    }

    this.excelService.exportAsExcelFile(allOrders, 'Schedule Report');
  }
  getAllPhones  (customer_Phone: any[]) {
    let allPhones: string="";
    if (customer_Phone.length > 0) {
      for (var i = 0; i < customer_Phone.length; i++) {
        allPhones = allPhones + customer_Phone[i].phone;
        if (customer_Phone.length >1) {
          allPhones += "/";
        }

      }

      return allPhones;

    }
     
    }

  applyFilter() {
    this.loading = true;
    this.data = [];
    this.obj.data.length = 0;
    if (moment(this.filterCritieria.startDate[0]).isValid()) {
      this.filterCritieria.startDate[0] = new Date(moment(this.filterCritieria.startDate[0]).format("YYYY-MM-DD HH:mm:ss"));
      this.filterCritieria.startDate[1] = new Date(moment(this.filterCritieria.startDate[1]).format("YYYY-MM-DD HH:mm:ss")) ;
 
    }

    Object.keys(this.filterCritieria).forEach((key) => {
      if (this.filterCritieria[key][0])
        this.obj.data.push({
          "Type": key,
          "Value": this.filterCritieria[key]
        });
      this.loading = false;
    });
    if (this.obj.data.length > 0)
      this._customerService.filterWorkOrdersReport(this.obj).subscribe(
        data => {

      



          this.data = data['data'];

          this.data.forEach(order => {
            if (new Date(order.startDate).getFullYear() != 1) {
              order.startDate = new Date(order.startDate);
              order.startDate.setHours(order.startDate.getHours() + this.customerService.getTimeOffsetTimeZone());
            }

            if (new Date(order.endDate).getFullYear() == 1 || new Date(order.endDate).getFullYear() == 1970) {
              order.endDate = null;
            }
            else {
              order.endDate = new Date(order.endDate);
              order.endDate.setHours(order.endDate.getHours() + this.customerService.getTimeOffsetTimeZone());
            }

          });
          this.data.map(x => {
            x.startDate = moment(x.startDate).format("YYYY-MM-DD HH:mm:ss");
            x.endDate = moment(x.endDate).format("YYYY-MM-DD HH:mm:ss");
            x.createdDate = moment(x.createdDate).format("YYYY-MM-DD HH:mm:ss");


          });
          if (moment(this.filterCritieria.startDate[0], 'YYYY-MM-DD').isValid())
            this.data = this.data.filter(x => moment(x.startDate).isBetween(this.filterCritieria.startDate[0], this.filterCritieria.startDate[1]));

          this.loading = false;
          //this.lastPage = Math.round(data['count'] / data['pageSize']);
          //this.currentPage = data['pageIndex'];
        }
      )
  }

  resetFilter() {
    this.obj.data.length = 0;
    this.data.length = 0;
    this.filterCritieria = {
      fK_Factory_Id: [""],
      fK_Status_Id: [""],
      fK_Priority_Id: [""],
      startDate: [null,null],
      amount: ["", ""],
      code: [""],
      pendingAmount: ["", ""]
    };
  }

  setLookups() {
    this.loading = true;

    this._lookupService.GetallFactories().subscribe(
      items =>
        this.factories = items
    )
    this._lookupService.GetallStatus().subscribe(
      items =>
        this.status = items
    )
    this._lookupService.GetallPeriorities().subscribe(
      items =>
        this.priorities = items
    )
    let UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
    //------------------- if Dispatcher ------------------------------
    //if ((UserAllRoles.indexOf("Dispatcher") > -1)) {
    //  this._customerService.GetAllWorkOrdersDispature().subscribe(WorkOrder => {
    //    this.orders = WorkOrder;
    //    this.loading = false;
    //  },
    //    () => {
    //      this.loading = false;
    //      // this.alertService.error('your session has timed out please login again !')
    //    });
    //}
    ////------------------- if MovementController ------------------------------
    //else if ((UserAllRoles.indexOf("MovementController") > -1)) {
    //  this._customerService.GetAllWorkOrdersMovementController().subscribe(WorkOrder => {
    //    this.orders = WorkOrder;
    //    this.loading = false;
    //  },
    //    () => {
    //      this.loading = false;
    //      // this.alertService.error('your session has timed out please login again !')
    //    });
    //}
    ////------------------- if Admin ------------------------------
    //else if ((UserAllRoles.indexOf("Admin") > -1)) {
    //  this._customerService.GetAllWorkOrdersMovementController().subscribe(WorkOrder => {
    //    this.orders = WorkOrder;
    //    this.loading = false;
    //  },
    //    () => {
    //      this.loading = false;
    //      // this.alertService.error('your session has timed out please login again !')
    //    });
    //}
  }

}
