import {AuthGuardGuard} from './../api-module/guards/auth-guard.guard';
import {RouterModule, Routes} from '@angular/router';
import {SharedModuleModule} from './../shared-module/shared-module.module';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReportManagementComponent} from './report-management/report-management.component';
import {AgmCoreModule} from '@agm/core';
import * as config from '../api-module/services/globalPath';


const routes: Routes = [
  {
    path: '',
    component: ReportManagementComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['MovementController', 'Dispatcher', 'Admin']}
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModuleModule,
    AgmCoreModule.forRoot(
      {
        apiKey: config.googlMapsAPiKey
      }
    ),
    RouterModule.forChild(routes)
  ],
  declarations: [ReportManagementComponent]
})
export class ReportModule {
}
